﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularioEstudiante1.aspx.cs" Inherits="CUCBecas.FormularioEstudiante1" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Formulario</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body class="bodyEs">
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
    <div class="containerEs">
        <form runat="server" class="formEs">
            <div class="divEs">
              
                    <label class="labelEs">
                        Nacionalidad<br>
                        <asp:TextBox ID="Nacionalidad" runat="server" placeholder="Nacionalidad" class="inputEs" ForeColor="Black" Text="Costarricense"></asp:TextBox>
                    </label>
              
                
                    <label class="labelEs">
                        Nivel (Cuatrimestre cursa)
                        <br>
                        <asp:DropDownList ID="Nivel" runat="server" AppendDataBoundItems="true" required="Falta tu Provincia" class="inputEs">
                            <asp:ListItem Value="">Seleccione su Nivel</asp:ListItem>
                            <asp:ListItem Value="Primer Ingreso">Primer Ingreso</asp:ListItem>
                            <asp:ListItem Value="Cuatrimestre 1">Cuatrimestre 1</asp:ListItem>
                            <asp:ListItem Value="Cuatrimestre 2">Cuatrimestre 2</asp:ListItem>
                            <asp:ListItem Value="Cuatrimestre 3">Cuatrimestre 3</asp:ListItem>
                            <asp:ListItem Value="Cuatrimestre 4">Cuatrimestre 4</asp:ListItem>
                            <asp:ListItem Value="Cuatrimestre 5">Cuatrimestre 5</asp:ListItem>
                            <asp:ListItem Value="Cuatrimestre 6">Cuatrimestre 6</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                
              
                    <label class="labelEs">
                        Teléfono Celular<br>
                        <asp:TextBox ID="Telcel" runat="server" placeholder="Teléfono Celular" class="inputEs" onkeypress="return numbersonly(event);" MinLength="8" MaxLength ="8"></asp:TextBox>
                    </label>
              
                    <label class="labelEs">
                        Teléfono Domiciliar<br>
                        <asp:TextBox ID="TelDom" runat="server" placeholder="Teléfono Domiciliar" class="inputEs" onkeypress="return numbersonly(event);" MinLength="8" MaxLength ="8"></asp:TextBox>
                    </label>
                
                    <label class="labelEs">
                        Estado Civil<br>
                        <asp:DropDownList ID="Ecivil" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                            <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                            <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                            <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                            <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                            <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                            <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                        </asp:DropDownList>
                    </label>
               
                    <label class="labelEs">
                        Provincia<br>
                       <asp:DropDownList ID="provincia" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ProviciaSelecioneda"  class="inputEs"></asp:DropDownList>
                    </label>
              
                    <label class="labelEs">
                        Cantón<br>
                        <asp:DropDownList ID="cantones" runat="server" AutoPostBack="True" OnSelectedIndexChanged="CantonSelecionedo"  class="inputEs"></asp:DropDownList> 
                    </label>
              
                    <label class="labelEs">
                        Distrito<br>
                        <asp:DropDownList ID="distritos" runat="server"  class="inputEs"></asp:DropDownList>
                    </label>
               
                    <label class="labelEs">
                        Dirección Exacta<br>
                        (Importante : anote N° casa , color , una descripción de algo cerca)<br>
                        <asp:TextBox ID="DirExa" runat="server" placeholder="Dirección Exacta" class="textareaEs" Rows="4" cols="50" TextMode="MultiLine" required="Falta tu descripcion" MaxLength="200" MinLength="25"></asp:TextBox>
                    </label>
               
                    <label class="labelEs">
                        Su condición como solicitante<br>
                        <asp:RadioButtonList ID="condicion" runat="server"  class="inputradioEs" AutoPostBack="True">
                            <asp:ListItem Value="Estudiante Regular, matriculado actualmente en el CUC que ha tenido beca anteriormente" class="inputradioEs"> Estudiante Regular, matriculado actualmente en el CUC que ha tenido beca anteriormente</asp:ListItem>
                            <asp:ListItem Value="Estudiante Regular, matriculado actualmente en el CUC que solicita beca por 1° vez" class="inputradioEs"> Estudiante Regular, matriculado actualmente en el CUC que solicita beca por 1° vez</asp:ListItem>
                            <asp:ListItem Value="Estudiante Primer Ingreso, no ha ingresado al CUC" class="inputradioEs"> Estudiante Primer Ingreso, no ha ingresado al CUC</asp:ListItem>
                        </asp:RadioButtonList>
                    </label>
               
                <asp:Label ID="Tb" runat="server"  class="labelEs">  Tipo de beca<br>
                        <asp:DropDownList ID="Tipobeca" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                            <asp:ListItem Value="">Seleccione su tipo de beca anteriormente</asp:ListItem>
                            <asp:ListItem Value="Beca 0">Beca 0</asp:ListItem>
                            <asp:ListItem Value="Beca 1">Beca 1</asp:ListItem>
                            <asp:ListItem Value="Beca 2">Beca 2</asp:ListItem>
                            <asp:ListItem Value="Beca 3">Beca 3</asp:ListItem>
                            <asp:ListItem Value="Beca 4">Beca 4</asp:ListItem>
                        </asp:DropDownList>
                   </asp:Label>
                <label class="labelEs">
                        Razon de perdida de beca<br>
                        <asp:TextBox ID="Razondeperdida" runat="server" placeholder="Perdida de Beca" class="inputEs" ></asp:TextBox>
                    </label>
                </div>
             <div class="centrado">
                    <asp:Button ID="Sig" runat="server" Text="Guardar y Seguir"  class="buttonEs" OnClick="Sig_Click"/>
               
             </div>
        </form>
    </div>
    <script type="text/javascript">
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode
        if (unicode != 8 && unicode != 44) {
            if (unicode < 48 || unicode > 57) //if not a number
            { return false } //disable key press    
        }
    }  
</SCRIPT>
</body>
</html>

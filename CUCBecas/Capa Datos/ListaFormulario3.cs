﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CUCBecas.Capa_Datos
{
    public class ListaFormulario3
    {
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string Cuantoslotes { get; set; }
        public string Usoslotes { get; set; }
        public string CuantosFincas { get; set; }
        public string UsosFincas { get; set; }
        public string CasaH { get; set; }
        public string CasaA { get; set; }
        public string Apartamentos { get; set; }
        public string Vehiculos { get; set; }
        public string Descripcion { get; set; }

    }
}
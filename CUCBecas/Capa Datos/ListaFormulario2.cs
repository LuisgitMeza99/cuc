﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CUCBecas.Capa_Datos
{
    public class ListaFormulario2
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Edad { get; set; }
        public string Parentesco { get; set; }     
        public string Estadocivil { get; set; }
        public string Estudia { get; set; }
        public string Beca { get; set; }
        public string Salud { get; set; }
        public string ProblemasSalud { get; set; }
        public string Ocupacion { get; set; }
        public string Instucion { get; set; }
        public string IngresoMensual { get; set; }

    }
}
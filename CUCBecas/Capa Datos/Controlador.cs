﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;



namespace CUCBecas.Capa_Datos
{

    public class Controlador
    {
        string conexion = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;

        public float RegistroEstu(string id_E, string nombre, string apellidos, string Correo, string sexo, string Carrera, string fe_Na, string password)
        {
            float Confirmacion = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("RegistroEstudiante", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@apellidos", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@fe_Na", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@sexo", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@carrera", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.Float));
                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id_E;
                cmd.Parameters["@nombre"].Value = nombre;
                cmd.Parameters["@apellidos"].Value = apellidos;
                cmd.Parameters["@email"].Value = Correo;
                cmd.Parameters["@fe_Na"].Value = fe_Na;
                cmd.Parameters["@sexo"].Value = sexo;
                cmd.Parameters["@carrera"].Value = Carrera;
                cmd.Parameters["@password"].Value = password;
                cmd.Parameters["@validar"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                Confirmacion = Convert.ToInt32(cmd.Parameters["@validar"].Value.ToString());
                cn.Close();
            }
            return Confirmacion;
        }
        public void EnviarCorreoRegistro(string nombre, string Correo)
        {
            string Descrip;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("CorreoRegistro", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@datos", SqlDbType.VarChar, 300));
                cmd.Parameters["@datos"].Direction = ParameterDirection.Output;
                cmd.Parameters["@datos"].Value = "";
                SqlDataReader dr = cmd.ExecuteReader();
                Descrip = cmd.Parameters["@datos"].Value.ToString();
                cn.Close();
            }
            string subject = "Datos importantes a tener en cuenta";

            string Body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/Email.htm")))
            {
                Body = reader.ReadToEnd();
            }
            Body = Body.Replace("{UserName}", nombre);
            Body = Body.Replace("{Description}", Descrip + " además le adjuntamos un pdf con informacion sobre este proceso ");
            string body = Body;
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("CucBecas@gmail.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(Correo));
                mailMessage.Attachments.Add(new Attachment(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/Formulario Bec-Soc-Eco.pdf")));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = Convert.ToBoolean("true");
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "CucBecas@gmail.com";
                NetworkCred.Password = "CUC3C2019";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse("587");
                smtp.Send(mailMessage);
            }
        }
        public float inicio(string id, string password)
        {
            float tipo;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("InicioSeccion", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@contra", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@tipo", SqlDbType.Float));
                cmd.Parameters["@tipo"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id;
                cmd.Parameters["@contra"].Value = password;
                cmd.Parameters["@tipo"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                tipo = Convert.ToInt32(cmd.Parameters["@tipo"].Value.ToString());
                cn.Close();
            }
            return tipo;
        }
        public float Registropersonal(string id, string nombre, string apellidos, string Correo, string departamento, string password)
        {
            float Confirmacion = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Registropersonal", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Apellidos", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Departamento", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.Float));
                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id;
                cmd.Parameters["@nombre"].Value = nombre;
                cmd.Parameters["@Apellidos"].Value = apellidos;
                cmd.Parameters["@email"].Value = Correo;
                cmd.Parameters["@Departamento"].Value = departamento;
                cmd.Parameters["@password"].Value = password;
                cmd.Parameters["@validar"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                Confirmacion = Convert.ToInt32(cmd.Parameters["@validar"].Value.ToString());
                cn.Close();
            }
            return Confirmacion;
        }
        public void EnviarCorreoPersonal(string nombre, string Correo)
        {

            string subject = "Registro";
            string Descrip = "Su registro se a realizado exitosamente por favor ingresar y cambiar su contraseña.";
            string Body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/Email.htm")))
            {
                Body = reader.ReadToEnd();
            }
            Body = Body.Replace("{UserName}", nombre);
            Body = Body.Replace("{Description}", Descrip);
            string body = Body;
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("CucBecas@gmail.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(Correo));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = Convert.ToBoolean("true");
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "CucBecas@gmail.com";
                NetworkCred.Password = "CUC3C2019";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse("587");
                smtp.Send(mailMessage);
            }
        }
        public string[] SeccionE(string id)
        {
            string[] datos = new string[4];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("variableEstu", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Nombre", SqlDbType.VarChar, 100));
                cmd.Parameters.Add(new SqlParameter("@Estado", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@Fecha", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));
                cmd.Parameters["@Nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@Estado"].Direction = ParameterDirection.Output;
                cmd.Parameters["@Fecha"].Direction = ParameterDirection.Output;
                cmd.Parameters["@email"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id;
                cmd.Parameters["@Nombre"].Value = 0;
                cmd.Parameters["@Estado"].Value = 0;
                cmd.Parameters["@Fecha"].Value = 0;
                cmd.Parameters["@email"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                datos[0] = cmd.Parameters["@Nombre"].Value.ToString();
                datos[1] = cmd.Parameters["@Estado"].Value.ToString();
                datos[2] = cmd.Parameters["@Fecha"].Value.ToString();
                datos[3] = cmd.Parameters["@email"].Value.ToString();
                cn.Close();
            }
            return datos;
        }
        public string varseccion(string id)
        {
            string dato = " ";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("variableSeccion", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Nombre", SqlDbType.VarChar, 100));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));
                cmd.Parameters["@Nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@email"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ID"].Value = id;
                cmd.Parameters["@Nombre"].Value = 0;
                cmd.Parameters["@email"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                dato = cmd.Parameters["@Nombre"].Value.ToString();
                HttpContext.Current.Session["email"] = cmd.Parameters["@email"].Value.ToString();

                cn.Close();
            }
            return dato;
        }
        public string insertFact()
        {
            string paso;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Insertarfactura", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@resultado", SqlDbType.VarChar, 100));
                cmd.Parameters["@resultado"].Direction = ParameterDirection.Output;
                cmd.Parameters["@resultado"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                paso = cmd.Parameters["@resultado"].Value.ToString();
                cn.Close();
            }
            return paso;
        }
        public string ValidarCambioContra(string id, string correo)
        {
            string valido;
            string token;
            string nombre;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("OlvidoContra", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@correo", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@paso", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar, 10));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 100));
                cmd.Parameters["@paso"].Direction = ParameterDirection.Output;
                cmd.Parameters["@token"].Direction = ParameterDirection.Output;
                cmd.Parameters["@nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id;
                cmd.Parameters["@correo"].Value = correo;
                cmd.Parameters["@paso"].Value = 0;
                cmd.Parameters["@token"].Value = 0;
                cmd.Parameters["@nombre"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = cmd.Parameters["@paso"].Value.ToString();
                token = cmd.Parameters["@token"].Value.ToString();
                nombre = cmd.Parameters["@nombre"].Value.ToString();
                cn.Close();
            }
            if (valido.Equals("1"))
            {
                string subject = "Cambio de Contraseña";
                string Descrip = "Este es su Codigo para cambiar su contraseña : " + token + " <br> Digitelo en la pantalla que le aparecio.";
                string Body = string.Empty;
                using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/CorreoNormail.htm")))
                {
                    Body = reader.ReadToEnd();
                }
                Body = Body.Replace("{UserName}", nombre);
                Body = Body.Replace("{Description}", Descrip);
                string body = Body;
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress("CucBecas@gmail.com");
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add(new MailAddress(correo));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = Convert.ToBoolean("true");
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = "CucBecas@gmail.com";
                    NetworkCred.Password = "CUC3C2019";
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse("587");
                    smtp.Send(mailMessage);
                }

            }
            return valido;
        }
        public Boolean Validartoken(string id, string token)
        {
            Boolean paso;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Validartok", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@valido", SqlDbType.Bit));
                cmd.Parameters["@valido"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id;
                cmd.Parameters["@token"].Value = token;
                cmd.Parameters["@valido"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                paso = Convert.ToBoolean(cmd.Parameters["@valido"].Value.ToString());
                cn.Close();
            }

            return paso;

        }
        public Boolean cambiarContra(string Contra)
        {
            Boolean paso;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Cambiar contraseña", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@contra", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@valido", SqlDbType.Bit));
                cmd.Parameters["@valido"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@token"].Value = (string)HttpContext.Current.Session["token"];
                cmd.Parameters["@contra"].Value = Contra;
                cmd.Parameters["@valido"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                paso = Convert.ToBoolean(cmd.Parameters["@valido"].Value.ToString());
                cn.Close();
            }
            return paso;
        }
        public string ValidacionFactura(string Factura)
        {
            string paso;
            string token;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("ValidarFactura", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Factura", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@paso", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar, 10));
                cmd.Parameters["@paso"].Direction = ParameterDirection.Output;
                cmd.Parameters["@token"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@Factura"].Value = Factura;
                cmd.Parameters["@paso"].Value = 0;
                cmd.Parameters["@token"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                paso = cmd.Parameters["@paso"].Value.ToString();
                token = cmd.Parameters["@token"].Value.ToString();
                cn.Close();
            }
            if (paso.Equals("1"))
            {
                string subject = "Ingresar al formulario";
                string Descrip = "Este es su Código para entrar al formulario : " + token + "<br><br> Este Código se inhabilitar cuando usted termine el formulario por completo y solo usted puede utilizarlo <br>Si deja el formulario sin completar lo puede volver a usar <br> Además le volvemos a ajuntar los datos de los anexos y que tenga un buen día";
                string Body = string.Empty;
                using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/CorreoNormail.htm")))
                {
                    Body = reader.ReadToEnd();
                }
                Body = Body.Replace("{UserName}", (string)HttpContext.Current.Session["Nombre"]);
                Body = Body.Replace("{Description}", Descrip);
                string body = Body;
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress("CucBecas@gmail.com");
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add(new MailAddress((string)HttpContext.Current.Session["email"]));
                    mailMessage.Attachments.Add(new Attachment(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/Formulario Bec-Soc-Eco.pdf")));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = Convert.ToBoolean("true");
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = "CucBecas@gmail.com";
                    NetworkCred.Password = "CUC3C2019";
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse("587");
                    smtp.Send(mailMessage);
                }

            }
            return paso;
        }
        public Boolean ValidacionFacturaCodigo(string token)
        {
            Boolean paso;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Validartok", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@valido", SqlDbType.Bit));
                cmd.Parameters["@valido"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@token"].Value = token;
                cmd.Parameters["@valido"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                paso = Convert.ToBoolean(cmd.Parameters["@valido"].Value.ToString());
                cn.Close();
            }
            return paso;
        }
        public void insertform1(string nacionalidad, string telcel, string teldom, string ecivil, string cuatri, string provincia, string canton, string distrito, string direxata, string condiciosoli)
        {
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Insert-1formulario", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nacionalidad", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@telcel", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@teldom", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@ecivil", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@cuatri", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@provincia", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@canton", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@distrito", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@direxata", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@condiciosoli", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar, 12));
                cmd.Parameters["@idform"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ID"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@nacionalidad"].Value = nacionalidad;
                cmd.Parameters["@telcel"].Value = telcel;
                cmd.Parameters["@teldom"].Value = teldom;
                cmd.Parameters["@ecivil"].Value = ecivil;
                cmd.Parameters["@cuatri"].Value = cuatri;
                cmd.Parameters["@provincia"].Value = provincia;
                cmd.Parameters["@canton"].Value = canton;
                cmd.Parameters["@distrito"].Value = distrito;
                cmd.Parameters["@direxata"].Value = direxata;
                cmd.Parameters["@condiciosoli"].Value = condiciosoli;
                cmd.Parameters["@idform"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                HttpContext.Current.Session["IDForm"] = cmd.Parameters["@idform"].Value.ToString();
                cn.Close();
            }

        }
        public void selectform1()
        {
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("select-1formulario", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar, 12));
                cmd.Parameters["@idform"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ID"].Value = (string)HttpContext.Current.Session["IDusuario"];

                cmd.Parameters["@idform"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();

                HttpContext.Current.Session["IDForm"] = cmd.Parameters["@idform"].Value.ToString();
                cn.Close();
            }

        }
        public string selectform2()
        {
            string valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("select-2formulario", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@valido", SqlDbType.VarChar, 1));
                cmd.Parameters["@valido"].Direction = ParameterDirection.Output;
                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@valido"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();

                valido = cmd.Parameters["@valido"].Value.ToString();
                cn.Close();
            }
            return valido;

        }

        public string[] datosolicitante()
        {
            string[] datos = new string[4];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("DatosPersonales", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Nombre", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@Apellidos", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@Edad", SqlDbType.VarChar, 20));
                cmd.Parameters.Add(new SqlParameter("@EstaCivil", SqlDbType.VarChar, 16));
                cmd.Parameters["@Nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@Apellidos"].Direction = ParameterDirection.Output;
                cmd.Parameters["@Edad"].Direction = ParameterDirection.Output;
                cmd.Parameters["@EstaCivil"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ID"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@Nombre"].Value = 0;
                cmd.Parameters["@Apellidos"].Value = 0;
                cmd.Parameters["@Edad"].Value = 0;
                cmd.Parameters["@EstaCivil"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                datos[0] = cmd.Parameters["@Nombre"].Value.ToString();
                datos[1] = cmd.Parameters["@Apellidos"].Value.ToString();
                datos[2] = cmd.Parameters["@Edad"].Value.ToString();
                datos[3] = cmd.Parameters["@EstaCivil"].Value.ToString();
                cn.Close();
            }
            return datos;
        }
        public void InsertarMiembro(string ced, string nom, string ape, string edad, string parent, string ecivil, string estu, string beca, string sal, string problesalud, string job, string company, string ingreMen)
        {
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Insert-2formulario", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@cedula", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@apellidos", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@edad", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@parentesco", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@ecivil", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@estudia", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@beca", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@salud", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@problematica", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@job", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@company", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@IngresoMensual", SqlDbType.Int));

                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@cedula"].Value = ced;
                cmd.Parameters["@nombre"].Value = nom;
                cmd.Parameters["@apellidos"].Value = ape;
                cmd.Parameters["@edad"].Value = edad;
                cmd.Parameters["@parentesco"].Value = parent;
                cmd.Parameters["@ecivil"].Value = ecivil;
                cmd.Parameters["@estudia"].Value = estu;
                cmd.Parameters["@beca"].Value = beca;
                cmd.Parameters["@salud"].Value = sal;
                cmd.Parameters["@problematica"].Value = problesalud;
                cmd.Parameters["@job"].Value = job;
                cmd.Parameters["@company"].Value = company;
                cmd.Parameters["@IngresoMensual"].Value = Convert.ToInt32(ingreMen);

                SqlDataReader dr = cmd.ExecuteReader();

                cn.Close();
            }
        }
        public void insertmonto(int ayud, int ingres, int otrayud, string tegresos, int egreso, int ingresofam, int cant)
        {
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Insert-2formularioD2", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@ayuda", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@Ingres", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@otrasayuda", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@tegresos", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@egresos", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@ingresosfamiliares", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@cantidad", SqlDbType.Int));
                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@ayuda"].Value = ayud;
                cmd.Parameters["@Ingres"].Value = ingres;
                cmd.Parameters["@otrasayuda"].Value = otrayud;
                cmd.Parameters["@tegresos"].Value = tegresos;
                cmd.Parameters["@egresos"].Value = egreso;
                cmd.Parameters["@ingresosfamiliares"].Value = ingresofam;
                cmd.Parameters["@cantidad"].Value = cant;
                SqlDataReader dr = cmd.ExecuteReader();

                cn.Close();
            }
        }
        public void Insertpropietario(string nom, string ced, string cl, string ul, string cf, string uf, string ch, string ca, string aparta, string vehiculo, string desve)
        {
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Insert-3formulario", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@cedula", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@cuantoslotes", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@usoslotes", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@cuantosficas", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@usosficas", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@casash", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@casasa", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@apartamentos", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@vehiculo", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@descripcionv", SqlDbType.VarChar));

                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@nombre"].Value = nom;
                cmd.Parameters["@cedula"].Value = ced;
                cmd.Parameters["@cuantoslotes"].Value = cl;
                cmd.Parameters["@usoslotes"].Value = ul;
                cmd.Parameters["@cuantosficas"].Value = cf;
                cmd.Parameters["@usosficas"].Value = uf;
                cmd.Parameters["@casash"].Value = ch;
                cmd.Parameters["@casasa"].Value = ca;
                cmd.Parameters["@apartamentos"].Value = aparta;
                cmd.Parameters["@vehiculo"].Value = vehiculo;
                cmd.Parameters["@descripcionv"].Value = desve;


                SqlDataReader dr = cmd.ExecuteReader();

                cn.Close();
            }
        }
        public void AceptoDB(string fech)
        {

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("AceptoBM", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Fecha", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Acepto", SqlDbType.VarChar));
                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@Fecha"].Value = fech;
                cmd.Parameters["@Acepto"].Value = "Si";
                SqlDataReader dr = cmd.ExecuteReader();
                cn.Close();
            }


        }
        public string selectform3()
        {
            string valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("select-3formulario", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@valido", SqlDbType.VarChar, 1));
                cmd.Parameters["@valido"].Direction = ParameterDirection.Output;
                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@valido"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();

                valido = cmd.Parameters["@valido"].Value.ToString();
                cn.Close();
            }
            return valido;

        }
        public string TraerCarrera()
        {
            string valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("TraerCarrera", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@carrera", SqlDbType.VarChar, 50));
                cmd.Parameters["@carrera"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@carrera"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = cmd.Parameters["@carrera"].Value.ToString();
                cn.Close();
            }
            return valido;

        }


        public void AceptoCompromiso(byte[] ima, string nombre, string tipo)
        {
            string dato;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("AceptoCompromiso", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@anexo", SqlDbType.VarBinary));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@tipo", SqlDbType.NVarChar));
                cmd.Parameters.Add(new SqlParameter("@dato", SqlDbType.VarChar, 200));
                cmd.Parameters["@dato"].Direction = ParameterDirection.Output;
                cmd.Parameters["@idform"].Value = (string)HttpContext.Current.Session["IDForm"];
                cmd.Parameters["@token"].Value = (string)HttpContext.Current.Session["token"];
                cmd.Parameters["@anexo"].Value = ima;
                cmd.Parameters["@nombre"].Value = nombre;
                cmd.Parameters["@tipo"].Value = tipo;
                cmd.Parameters["@dato"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                dato = cmd.Parameters["@dato"].Value.ToString();
                cn.Close();
            }
            DateTime fech = DateTime.Now;
            string fecha = fech.ToShortDateString();
            string subject = "Finalización del Formulario";

            string Body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/CorreoNormail.htm")))
            {
                Body = reader.ReadToEnd();
            }
            Body = Body.Replace("{UserName}", (string)HttpContext.Current.Session["Nombre"]);
            Body = Body.Replace("{Description}", "Se le informa que el dia " + fecha + dato);
            string body = Body;
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("CucBecas@gmail.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress((string)HttpContext.Current.Session["email"]));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = Convert.ToBoolean("true");
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "CucBecas@gmail.com";
                NetworkCred.Password = "CUC3C2019";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse("587");
                smtp.Send(mailMessage);
            }
        }

        public string[] TraerPerfil(string id)
        {
            string[] valido = new string[4];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("TraerdatosdelperfilEstudiante", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@apellidos", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@carrera", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@apellidos"].Direction = ParameterDirection.Output;
                cmd.Parameters["@email"].Direction = ParameterDirection.Output;
                cmd.Parameters["@carrera"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id;
                cmd.Parameters["@nombre"].Value = 0;
                cmd.Parameters["@apellidos"].Value = 0;
                cmd.Parameters["@email"].Value = 0;
                cmd.Parameters["@carrera"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@nombre"].Value.ToString();
                valido[1] = cmd.Parameters["@apellidos"].Value.ToString();
                valido[2] = cmd.Parameters["@email"].Value.ToString();
                valido[3] = cmd.Parameters["@carrera"].Value.ToString();
                cn.Close();
            }
            return valido;

        }

        public int actualizarPerfil(string nombre, string apellido, string email, string carrera)
        {
            int valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("ActualizarEstudiante", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@apellidos", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@carrera", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.VarChar));
                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@nombre"].Value = nombre;
                cmd.Parameters["@apellidos"].Value = apellido;
                cmd.Parameters["@email"].Value = email;
                cmd.Parameters["@carrera"].Value = carrera;
                cmd.Parameters["@validar"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = Convert.ToInt32(cmd.Parameters["@validar"].Value.ToString());
                cn.Close();
            }
            return valido;

        }

        public string actualizarPerfilContra(string contra)
        {
            string valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("ActualizarEstudianteContrasena", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.VarChar));
                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@password"].Value = contra;
                cmd.Parameters["@validar"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = cmd.Parameters["@validar"].Value.ToString();
                cn.Close();
            }
            return valido;

        }

        public string[] DatosEstudianteVisitaDom(string cedula)
        {
            string[] valido = new string[4];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SelectEstudianteVisitaD", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idest", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@valor", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 100));
                cmd.Parameters.Add(new SqlParameter("@carrera", SqlDbType.VarChar, 100));
                cmd.Parameters.Add(new SqlParameter("@fecha", SqlDbType.VarChar, 50));
                cmd.Parameters["@valor"].Direction = ParameterDirection.Output;
                cmd.Parameters["@nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@carrera"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fecha"].Direction = ParameterDirection.Output;
                cmd.Parameters["@idest"].Value = cedula;
                cmd.Parameters["@valor"].Value = 0;
                cmd.Parameters["@nombre"].Value = 0;
                cmd.Parameters["@carrera"].Value = 0;
                cmd.Parameters["@fecha"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@valor"].Value.ToString();
                if (valido[0] == "2")
                {
                    valido[1] = cmd.Parameters["@nombre"].Value.ToString();
                    valido[2] = cmd.Parameters["@carrera"].Value.ToString();
                    valido[3] = cmd.Parameters["@fecha"].Value.ToString();
                }

                cn.Close();
            }
            return valido;

        }

        public bool InsertVisitaDom(string cedula, string d1, string d2, string d3, string d4, string d5, string d6, string text, bool acc, bool d7, bool d8, bool d9, bool d10,
            bool d11, bool d12, bool d13, bool d14, bool d15, bool d16, bool d17, string d18, bool d19, string d20, string d21, bool d23, string d24,
            string d25, string d26, string d27, string d28, string d29, string ObsevacionPrS, string d30, string FaltaA, string d31, string d32, string d33, string d34, bool ducha, bool d35, bool d36, bool d37,
            bool d38, bool d39, bool d40, bool d41, bool d42, bool d43, string d44,string CategoBeca, string d45, string d46)
        {
            bool valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("InsertarVisitaDom", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@entre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@motivo", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Accesolugar", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@MeAcceso", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@infraestru", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@seguridad", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@AcesoCC", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Escuela", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Electricidad", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Internet", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Cable", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@transporte", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@RecoBasura", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@CentroSalud", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Agua", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Celular", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Alumbrado", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@otros", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@empleo", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Recreacion", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@N_Int", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@N_Trab", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Jmujeres", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@N_AM", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@N_DIS", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@N_Est", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Salud", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Desempleo", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@AbusoSustancia", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@observacionProblSustacia", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Violencia", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@faltApoy", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@tencia", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Adquisicion", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Estado", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Aposento", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@ducha", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Micro", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Compu", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Consola", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Radio", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Tv", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@CocinaE", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@Refri", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@lavadora", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@CocinaL", SqlDbType.Bit));
                cmd.Parameters.Add(new SqlParameter("@observa", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@CateBeca", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@cedula", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@val", SqlDbType.Bit));
                cmd.Parameters["@val"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = cedula;
                cmd.Parameters["@entre"].Value = d1;
                cmd.Parameters["@motivo"].Value = d2;
                cmd.Parameters["@Accesolugar"].Value = d3;
                cmd.Parameters["@MeAcceso"].Value = d4;
                cmd.Parameters["@infraestru"].Value = d5;
                cmd.Parameters["@seguridad"].Value = d6;
                cmd.Parameters["@AcesoCC"].Value = acc;
                cmd.Parameters["@Escuela"].Value = d7;
                cmd.Parameters["@Electricidad"].Value = d8;
                cmd.Parameters["@Internet"].Value = d9;
                cmd.Parameters["@Cable"].Value = d10;
                cmd.Parameters["@transporte"].Value = d11;
                cmd.Parameters["@RecoBasura"].Value = d12;
                cmd.Parameters["@CentroSalud"].Value = d13;
                cmd.Parameters["@Agua"].Value = d14;
                cmd.Parameters["@Celular"].Value = d15;
                cmd.Parameters["@Alumbrado"].Value = d16;
                cmd.Parameters["@otros"].Value = d17;
                cmd.Parameters["@empleo"].Value = d18;
                cmd.Parameters["@Recreacion"].Value = d19;
                cmd.Parameters["@N_Int"].Value = d20;
                cmd.Parameters["@N_Trab"].Value = d21;
                cmd.Parameters["@Jmujeres"].Value = d23;
                cmd.Parameters["@N_AM"].Value = d24;
                cmd.Parameters["@N_DIS"].Value = d25;
                cmd.Parameters["@N_Est"].Value = d26;
                cmd.Parameters["@Salud"].Value = d27;
                cmd.Parameters["@Desempleo"].Value = d28;
                cmd.Parameters["@AbusoSustancia"].Value = d29;
                cmd.Parameters["@observacionProblSustacia"].Value = ObsevacionPrS;
                cmd.Parameters["@Violencia"].Value = d30;
                cmd.Parameters["@faltApoy"].Value = FaltaA;
                cmd.Parameters["@tencia"].Value = d31;
                cmd.Parameters["@Adquisicion"].Value = d32;
                cmd.Parameters["@Estado"].Value = d33;
                cmd.Parameters["@Aposento"].Value = d34;
                cmd.Parameters["@ducha"].Value = ducha;
                cmd.Parameters["@Micro"].Value = d35;
                cmd.Parameters["@Compu"].Value = d36;
                cmd.Parameters["@Consola"].Value = d37;
                cmd.Parameters["@Radio"].Value = d38;
                cmd.Parameters["@Tv"].Value = d39;
                cmd.Parameters["@CocinaE"].Value = d40;
                cmd.Parameters["@Refri"].Value = d41;
                cmd.Parameters["@lavadora"].Value = d42;
                cmd.Parameters["@CocinaL"].Value = d43;
                cmd.Parameters["@observa"].Value = d44;
                cmd.Parameters["@CateBeca"].Value = CategoBeca;
                cmd.Parameters["@Nombre"].Value = d45;
                cmd.Parameters["@cedula"].Value = d46;
                cmd.Parameters["@val"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = bool.Parse(cmd.Parameters["@val"].Value.ToString());


                cn.Close();
            }
            return valido;

        }

        public string InsertarBeca(string id, string beca, string observa)
        {
            string valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("IngresarBeca", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idform", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@beca", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@observacion", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@valido", SqlDbType.VarChar));
                cmd.Parameters["@valido"].Direction = ParameterDirection.Output;
                cmd.Parameters["@idform"].Value = id;
                cmd.Parameters["@beca"].Value = beca;
                cmd.Parameters["@observacion"].Value = observa;
                cmd.Parameters["@valido"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = cmd.Parameters["@valido"].Value.ToString();
                cn.Close();
            }
            return valido;

        }
        public string[] TraerPerfilPersonal()
        {
            string[] valido = new string[4];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("TraerdatosdelperfilPersonal", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@apellidos", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@depa", SqlDbType.VarChar, 50));
                cmd.Parameters["@nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@apellidos"].Direction = ParameterDirection.Output;
                cmd.Parameters["@email"].Direction = ParameterDirection.Output;
                cmd.Parameters["@depa"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@nombre"].Value = 0;
                cmd.Parameters["@apellidos"].Value = 0;
                cmd.Parameters["@email"].Value = 0;
                cmd.Parameters["@depa"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@nombre"].Value.ToString();
                valido[1] = cmd.Parameters["@apellidos"].Value.ToString();
                valido[2] = cmd.Parameters["@email"].Value.ToString();
                valido[3] = cmd.Parameters["@depa"].Value.ToString();
                cn.Close();
            }
            return valido;

        }
        public int actualizarPersonal(string nombre, string apellido, string email, string carrera)
        {
            int valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("ActualizarPersonal", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@apellidos", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Departamento", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.VarChar));
                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@nombre"].Value = nombre;
                cmd.Parameters["@apellidos"].Value = apellido;
                cmd.Parameters["@email"].Value = email;
                cmd.Parameters["@Departamento"].Value = carrera;
                cmd.Parameters["@validar"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = Convert.ToInt32(cmd.Parameters["@validar"].Value.ToString());
                cn.Close();
            }
            return valido;

        }
        public void actualizarFechas(string cuatri, string fp, string ff, string fr, string fa)
        {

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("ActualizarFechas", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@cuantri", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@fechaP", SqlDbType.Date));
                cmd.Parameters.Add(new SqlParameter("@fechaf", SqlDbType.Date));
                cmd.Parameters.Add(new SqlParameter("@fechaR", SqlDbType.Date));
                cmd.Parameters.Add(new SqlParameter("@fechaA", SqlDbType.Date));
                cmd.Parameters["@cuantri"].Value = cuatri;
                cmd.Parameters["@fechaP"].Value = fp;
                cmd.Parameters["@fechaf"].Value = ff;
                cmd.Parameters["@fechaR"].Value = fr;
                cmd.Parameters["@fechaA"].Value = fa;

                SqlDataReader dr = cmd.ExecuteReader();

                cn.Close();
            }


        }
        public string actualizarPersonalContra(string contra,string id)
        {
            string valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("ActualizarPersonalContrasena", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.VarChar));
                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = id;
                cmd.Parameters["@password"].Value = contra;
                cmd.Parameters["@validar"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = cmd.Parameters["@validar"].Value.ToString();
                cn.Close();
            }
            return valido;

        }

        public string Entrarformulario()
        {
            string valido;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("TerminaForm", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@val", SqlDbType.VarChar));
                cmd.Parameters["@val"].Direction = ParameterDirection.Output;
                cmd.Parameters["@val"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido = cmd.Parameters["@val"].Value.ToString();
                cn.Close();
            }
            return valido;

        }
        public string[] FechasIncio()
        {
            string[] valido = new string[6];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Fechasinicio", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@fechapago", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@fechaform", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@fechaR", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@fechaini", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@fechafin", SqlDbType.VarChar, 50));
                cmd.Parameters.Add(new SqlParameter("@coste", SqlDbType.VarChar, 50));
                cmd.Parameters["@fechapago"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechaform"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechaR"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechaini"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechafin"].Direction = ParameterDirection.Output;
                cmd.Parameters["@coste"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechapago"].Value = 0;
                cmd.Parameters["@fechaform"].Value = 0;
                cmd.Parameters["@fechaR"].Value = 0;
                cmd.Parameters["@fechaini"].Value = 0;
                cmd.Parameters["@fechafin"].Value = 0;
                cmd.Parameters["@coste"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@coste"].Value.ToString();
                valido[1] = cmd.Parameters["@fechapago"].Value.ToString();
                valido[2] = cmd.Parameters["@fechaform"].Value.ToString();
                valido[3] = cmd.Parameters["@fechaR"].Value.ToString();
                valido[4] = cmd.Parameters["@fechaini"].Value.ToString();
                valido[5] = cmd.Parameters["@fechafin"].Value.ToString();
                cn.Close();
            }
            return valido;

        }
        public void CorreoMasivo()
        {
            string cedu;
            string Nombre;
            string Correo;
            string Resultado;
            string FechaA;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("select e.IDEstudiante, E.Nombre +' '+E.Apellidos as 'Nombre',E.Correo,F.Beca,CONVERT(varchar(20),fe.InicioFechaApelacion)+' al '+CONVERT(varchar(20),fe.FinFechaApelacion) as 'Apelacion'  from  Formularios F inner join Estudiantes E on F.IDEstudiante = E.IDEstudiante inner join Fechas fe on F.FechaRevision = fe.FechaResultado where  CONVERT(VARCHAR(10), getdate(), 111) >= f.FechaRevision and f.Revisado ='Si' and f.Notificado ='0' ", cn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        cedu = Convert.ToString(reader[0]);
                        Nombre = Convert.ToString(reader[1]);
                        Correo = Convert.ToString(reader[2]);
                        Resultado = Convert.ToString(reader[3]);
                        FechaA = Convert.ToString(reader[4]);
                        this.CorreoResultado(Nombre, Correo, Resultado, FechaA);
                        this.UpdateM(cedu);
                    }
                }

                cn.Close();
            }


        }
        public void UpdateM(string ced)
        {
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("UPDATE Formularios SET Notificado ='1' WHERE IDEstudiante = "+ced, cn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                }

                cn.Close();
            }


        }




        public void CorreoResultado(string nombre, string Correo, string resultado, string fecha)
        {


            string subject = "Resultado de Beca";

            string Body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/CorreoNormail.htm")))
            {
                Body = reader.ReadToEnd();
            }
            Body = Body.Replace("{UserName}", nombre);
            Body = Body.Replace("{Description}", "Su resultado de la beca fue : " + resultado + "<br> Cualquier consulta (incluyendo si no ve reflejado su resultado en la página) o apelación sobre el otorgamiento de beca se puede apersonar a la Unidad de Trabajo Social en el siguiente horario: 2:00pm a 3:30pm del "+ fecha + " <br><br>NOTA: Estos días son correspondientes a los TRES ÚNICOS días estipulados para apelaciones, por lo que después de estas fechas no se recibirá apelación alguna. Es decir, despues de estas fechas no se hará ningún cambio en el resultado de becas.");
            string body = Body;
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("CucBecas@gmail.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(Correo));

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = Convert.ToBoolean("true");
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "CucBecas@gmail.com";
                NetworkCred.Password = "CUC3C2019";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse("587");
                smtp.Send(mailMessage);
            }
        }
        public void CorreoFactura(string nombre, string Correo, string factura)
        {


            string subject = "Gracias por su compra";

            string Body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/Correos/CorreoNormail.htm")))
            {
                Body = reader.ReadToEnd();
            }
            Body = Body.Replace("{UserName}", nombre);
            Body = Body.Replace("{Description}", "Usted a pagado el derecho de realizar el formulario con un coste de " + (string)HttpContext.Current.Session["Coste"] + " y el serial de su factura es : " + factura);
            string body = Body;
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress("CucBecas@gmail.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(Correo));

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = Convert.ToBoolean("true");
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "CucBecas@gmail.com";
                NetworkCred.Password = "CUC3C2019";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse("587");
                smtp.Send(mailMessage);
            }
        }
        public string[] selectpFin ()
        {
            string []valido = new string[2];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SelectPFinaciero", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@d1", SqlDbType.VarChar, 20));
                cmd.Parameters.Add(new SqlParameter("@d2", SqlDbType.VarChar, 200));
                cmd.Parameters["@d1"].Direction = ParameterDirection.Output;
                cmd.Parameters["@d2"].Direction = ParameterDirection.Output;
                cmd.Parameters["@d1"].Value = 0;
                cmd.Parameters["@d2"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@d1"].Value.ToString();
                valido[1] = cmd.Parameters["@d2"].Value.ToString();
                cn.Close();
            }
            return valido;

        }
        public void EditarPfin(string d1,string d2)
        {
       
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("EditarPFinaciero", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@d1", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@d2", SqlDbType.VarChar));
                cmd.Parameters["@d1"].Value = d1;
                cmd.Parameters["@d2"].Value = d2;
                SqlDataReader dr = cmd.ExecuteReader();
                
                cn.Close();
            }
            

        }
        public string[] DatosAnterioresdebeca()
        {
            string[] valido = new string[2];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("DatosBecaAnterios", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@idf", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@Beca", SqlDbType.VarChar, 10));
                cmd.Parameters.Add(new SqlParameter("@Observacion", SqlDbType.VarChar, 100));
                cmd.Parameters["@Beca"].Direction = ParameterDirection.Output;
                cmd.Parameters["@Observacion"].Direction = ParameterDirection.Output;
                cmd.Parameters["@idf"].Value = (string)HttpContext.Current.Session["NFormulario"];
                cmd.Parameters["@Beca"].Value = 0;
                cmd.Parameters["@Observacion"].Value = 0;
                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@Beca"].Value.ToString();
                valido[1] = cmd.Parameters["@Observacion"].Value.ToString();
                cn.Close();
            }
            return valido;

        }


        public string[] selectFechas(string cuatri)
        {
            string[] valido = new string[4];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SelectFechas", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@cuantri", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@fechaP", SqlDbType.VarChar,10));
                cmd.Parameters.Add(new SqlParameter("@fechaf", SqlDbType.VarChar, 10));
                cmd.Parameters.Add(new SqlParameter("@fechaR", SqlDbType.VarChar, 10));
                cmd.Parameters.Add(new SqlParameter("@fechaA", SqlDbType.VarChar, 10));
                cmd.Parameters["@fechaP"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechaf"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechaR"].Direction = ParameterDirection.Output;
                cmd.Parameters["@fechaA"].Direction = ParameterDirection.Output;
                cmd.Parameters["@cuantri"].Value = cuatri;
                cmd.Parameters["@fechaP"].Value = 0;
                cmd.Parameters["@fechaf"].Value = 0;
                cmd.Parameters["@fechaR"].Value = 0;
                cmd.Parameters["@fechaA"].Value = 0;

                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@fechaP"].Value.ToString();
                valido[1] = cmd.Parameters["@fechaf"].Value.ToString();
                valido[2] = cmd.Parameters["@fechaR"].Value.ToString();
                valido[3] = cmd.Parameters["@fechaA"].Value.ToString();
                cn.Close();
            }
            return valido;

        }


        public string[] perfiladmin()
        {
            string[] valido = new string[3];
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Traeradmin", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ua", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar, 3));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar, 70));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50));

                cmd.Parameters["@id"].Direction = ParameterDirection.Output;
                cmd.Parameters["@nombre"].Direction = ParameterDirection.Output;
                cmd.Parameters["@email"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ua"].Value = (string)HttpContext.Current.Session["IDusuario"];
                cmd.Parameters["@id"].Value = 0;
                cmd.Parameters["@nombre"].Value = 0;
                cmd.Parameters["@email"].Value = 0;
             

                SqlDataReader dr = cmd.ExecuteReader();
                valido[0] = cmd.Parameters["@id"].Value.ToString();
                valido[1] = cmd.Parameters["@nombre"].Value.ToString();
                valido[2] = cmd.Parameters["@email"].Value.ToString();
                cn.Close();
            }
            return valido;

        }


        public string Updateperfiladmin(string nom,string email)
        {
            string valido = "";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("ActualizarAd", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.VarChar,2));

                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["id"];
                cmd.Parameters["@nombre"].Value = nom;
                cmd.Parameters["@email"].Value = email;


                SqlDataReader dr = cmd.ExecuteReader();
                valido = cmd.Parameters["@validar"].Value.ToString();
              
                cn.Close();
            }
            return valido;

        }
        public string Nadmin(string user, string name ,string email,string contra)
        {
            string valido ;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("Newadmin", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@ua", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@nombre", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@contra", SqlDbType.VarChar));
                cmd.Parameters.Add(new SqlParameter("@validar", SqlDbType.VarChar));
                cmd.Parameters["@validar"].Direction = ParameterDirection.Output;
                cmd.Parameters["@id"].Value = (string)HttpContext.Current.Session["id"];
                cmd.Parameters["@ua"].Value = user;              
                cmd.Parameters["@nombre"].Value = name;
                cmd.Parameters["@email"].Value = email;
                cmd.Parameters["@contra"].Value = contra;
                cmd.Parameters["@validar"].Value = 0;

               SqlDataReader dr = cmd.ExecuteReader();
                valido =  cmd.Parameters["@validar"].Value.ToString();
               
                cn.Close();
            }
            return valido;

        }
    }
}


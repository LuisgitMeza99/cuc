﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class FormularioEstudiante2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
           string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            string cod = (string)Session["cod"];

            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1") || !cod.Equals("true"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
            Controlador control = new Controlador();
            string[] datos = control.datosolicitante();
            Cédula.Text = id;
            Nombre.Text = datos[0];
            Apellidos.Text = datos[1];
            Edad.Text = datos[2];
            Escivil.Text = datos[3];
            if (IsPostBack || !IsPostBack)
            {
             string miembros = (string)Session["miembros"];
            if (miembros.Equals("1"))
            {
                    titulo2.Visible = false;
                    c2.Visible = false;
                    n2.Visible = false;
                    a2.Visible = false;
                    e2.Visible = false;
                    p2.Visible = false;
                    ec2.Visible = false;
                    est2.Visible = false;
                    b2.Visible = false;
                    s2.Visible = false;
                    Psalud2.Visible = false;
                    o2.Visible = false;
                    i2.Visible = false;
                    ingreso2.Visible = false;

                    titulo3.Visible = false;
                    c3.Visible = false;
                    n3.Visible = false;
                    a3.Visible = false;
                    e3.Visible = false;
                    p3.Visible = false;
                    ec3.Visible = false;
                    est3.Visible = false;
                    b3.Visible = false;
                    s3.Visible = false;
                    Psalud3.Visible = false;
                    o3.Visible = false;
                    i3.Visible = false;
                    ingreso3.Visible = false;

                    titulo4.Visible = false;
                    c4.Visible = false;
                    n4.Visible = false;
                    a4.Visible = false;
                    e4.Visible = false;
                    p4.Visible = false;
                    ec4.Visible = false;
                    est4.Visible = false;
                    b4.Visible = false;
                    s4.Visible = false;
                    Psalud4.Visible = false;
                    o4.Visible = false;
                    i4.Visible = false;
                    ingreso4.Visible = false;

                    titulo5.Visible = false;
                    c5.Visible = false;
                    n5.Visible = false;
                    a5.Visible = false;
                    e5.Visible = false;
                    p5.Visible = false;
                    ec5.Visible = false;
                    est5.Visible = false;
                    b5.Visible = false;
                    s5.Visible = false;
                    Psalud5.Visible = false;
                    o5.Visible = false;
                    i5.Visible = false;
                    ingreso5.Visible = false;

                    titulo6.Visible = false;
                    c6.Visible = false;
                    n6.Visible = false;
                    a6.Visible = false;
                    e6.Visible = false;
                    p6.Visible = false;
                    ec6.Visible = false;
                    est6.Visible = false;
                    b6.Visible = false;
                    s6.Visible = false;
                    Psalud6.Visible = false;
                    o6.Visible = false;
                    i6.Visible = false;
                    ingreso6.Visible = false;

                    titulo7.Visible = false;
                    c7.Visible = false;
                    n7.Visible = false;
                    a7.Visible = false;
                    e7.Visible = false;
                    p7.Visible = false;
                    ec7.Visible = false;
                    est7.Visible = false;
                    b7.Visible = false;
                    s7.Visible = false;
                    Psalud7.Visible = false;
                    o7.Visible = false;
                    i7.Visible = false;
                    ingreso7.Visible = false;

                    titulo8.Visible = false;
                    c8.Visible = false;
                    n8.Visible = false;
                    a8.Visible = false;
                    e8.Visible = false;
                    p8.Visible = false;
                    ec8.Visible = false;
                    est8.Visible = false;
                    b8.Visible = false;
                    s8.Visible = false;
                    Psalud8.Visible = false;
                    o8.Visible = false;
                    i8.Visible = false;
                    ingreso8.Visible = false;

                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
            if (miembros.Equals("2"))
            {
                    titulo3.Visible = false;
                    c3.Visible = false;
                    n3.Visible = false;
                    a3.Visible = false;
                    e3.Visible = false;
                    p3.Visible = false;
                    ec3.Visible = false;
                    est3.Visible = false;
                    b3.Visible = false;
                    s3.Visible = false;
                    Psalud3.Visible = false;
                    o3.Visible = false;
                    i3.Visible = false;
                    ingreso3.Visible = false;

                    titulo4.Visible = false;
                    c4.Visible = false;
                    n4.Visible = false;
                    a4.Visible = false;
                    e4.Visible = false;
                    p4.Visible = false;
                    ec4.Visible = false;
                    est4.Visible = false;
                    b4.Visible = false;
                    s4.Visible = false;
                    Psalud4.Visible = false;
                    o4.Visible = false;
                    i4.Visible = false;
                    ingreso4.Visible = false;

                    titulo5.Visible = false;
                    c5.Visible = false;
                    n5.Visible = false;
                    a5.Visible = false;
                    e5.Visible = false;
                    p5.Visible = false;
                    ec5.Visible = false;
                    est5.Visible = false;
                    b5.Visible = false;
                    s5.Visible = false;
                    Psalud5.Visible = false;
                    o5.Visible = false;
                    i5.Visible = false;
                    ingreso5.Visible = false;

                    titulo6.Visible = false;
                    c6.Visible = false;
                    n6.Visible = false;
                    a6.Visible = false;
                    e6.Visible = false;
                    p6.Visible = false;
                    ec6.Visible = false;
                    est6.Visible = false;
                    b6.Visible = false;
                    s6.Visible = false;
                    Psalud6.Visible = false;
                    o6.Visible = false;
                    i6.Visible = false;
                    ingreso6.Visible = false;

                    titulo7.Visible = false;
                    c7.Visible = false;
                    n7.Visible = false;
                    a7.Visible = false;
                    e7.Visible = false;
                    p7.Visible = false;
                    ec7.Visible = false;
                    est7.Visible = false;
                    b7.Visible = false;
                    s7.Visible = false;
                    Psalud7.Visible = false;
                    o7.Visible = false;
                    i7.Visible = false;
                    ingreso7.Visible = false;

                    titulo8.Visible = false;
                    c8.Visible = false;
                    n8.Visible = false;
                    a8.Visible = false;
                    e8.Visible = false;
                    p8.Visible = false;
                    ec8.Visible = false;
                    est8.Visible = false;
                    b8.Visible = false;
                    s8.Visible = false;
                    Psalud8.Visible = false;
                    o8.Visible = false;
                    i8.Visible = false;
                    ingreso8.Visible = false;

                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
            if (miembros.Equals("3"))
            {

                    titulo4.Visible = false;
                    c4.Visible = false;
                    n4.Visible = false;
                    a4.Visible = false;
                    e4.Visible = false;
                    p4.Visible = false;
                    ec4.Visible = false;
                    est4.Visible = false;
                    b4.Visible = false;
                    s4.Visible = false;
                    Psalud4.Visible = false;
                    o4.Visible = false;
                    i4.Visible = false;
                    ingreso4.Visible = false;

                    titulo5.Visible = false;
                    c5.Visible = false;
                    n5.Visible = false;
                    a5.Visible = false;
                    e5.Visible = false;
                    p5.Visible = false;
                    ec5.Visible = false;
                    est5.Visible = false;
                    b5.Visible = false;
                    s5.Visible = false;
                    Psalud5.Visible = false;
                    o5.Visible = false;
                    i5.Visible = false;
                    ingreso5.Visible = false;

                    titulo6.Visible = false;
                    c6.Visible = false;
                    n6.Visible = false;
                    a6.Visible = false;
                    e6.Visible = false;
                    p6.Visible = false;
                    ec6.Visible = false;
                    est6.Visible = false;
                    b6.Visible = false;
                    s6.Visible = false;
                    Psalud6.Visible = false;
                    o6.Visible = false;
                    i6.Visible = false;
                    ingreso6.Visible = false;

                    titulo7.Visible = false;
                    c7.Visible = false;
                    n7.Visible = false;
                    a7.Visible = false;
                    e7.Visible = false;
                    p7.Visible = false;
                    ec7.Visible = false;
                    est7.Visible = false;
                    b7.Visible = false;
                    s7.Visible = false;
                    Psalud7.Visible = false;
                    o7.Visible = false;
                    i7.Visible = false;
                    ingreso7.Visible = false;

                    titulo8.Visible = false;
                    c8.Visible = false;
                    n8.Visible = false;
                    a8.Visible = false;
                    e8.Visible = false;
                    p8.Visible = false;
                    ec8.Visible = false;
                    est8.Visible = false;
                    b8.Visible = false;
                    s8.Visible = false;
                    Psalud8.Visible = false;
                    o8.Visible = false;
                    i8.Visible = false;
                    ingreso8.Visible = false;

                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
            if (miembros.Equals("4"))
            {
                    titulo5.Visible = false;
                    c5.Visible = false;
                    n5.Visible = false;
                    a5.Visible = false;
                    e5.Visible = false;
                    p5.Visible = false;
                    ec5.Visible = false;
                    est5.Visible = false;
                    b5.Visible = false;
                    s5.Visible = false;
                    Psalud5.Visible = false;
                    o5.Visible = false;
                    i5.Visible = false;
                    ingreso5.Visible = false;

                    titulo6.Visible = false;
                    c6.Visible = false;
                    n6.Visible = false;
                    a6.Visible = false;
                    e6.Visible = false;
                    p6.Visible = false;
                    ec6.Visible = false;
                    est6.Visible = false;
                    b6.Visible = false;
                    s6.Visible = false;
                    Psalud6.Visible = false;
                    o6.Visible = false;
                    i6.Visible = false;
                    ingreso6.Visible = false;

                    titulo7.Visible = false;
                    c7.Visible = false;
                    n7.Visible = false;
                    a7.Visible = false;
                    e7.Visible = false;
                    p7.Visible = false;
                    ec7.Visible = false;
                    est7.Visible = false;
                    b7.Visible = false;
                    s7.Visible = false;
                    Psalud7.Visible = false;
                    o7.Visible = false;
                    i7.Visible = false;
                    ingreso7.Visible = false;

                    titulo8.Visible = false;
                    c8.Visible = false;
                    n8.Visible = false;
                    a8.Visible = false;
                    e8.Visible = false;
                    p8.Visible = false;
                    ec8.Visible = false;
                    est8.Visible = false;
                    b8.Visible = false;
                    s8.Visible = false;
                    Psalud8.Visible = false;
                    o8.Visible = false;
                    i8.Visible = false;
                    ingreso8.Visible = false;

                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
            if (miembros.Equals("5"))
            {


                    titulo6.Visible = false;
                    c6.Visible = false;
                    n6.Visible = false;
                    a6.Visible = false;
                    e6.Visible = false;
                    p6.Visible = false;
                    ec6.Visible = false;
                    est6.Visible = false;
                    b6.Visible = false;
                    s6.Visible = false;
                    Psalud6.Visible = false;
                    o6.Visible = false;
                    i6.Visible = false;
                    ingreso6.Visible = false;

                    titulo7.Visible = false;
                    c7.Visible = false;
                    n7.Visible = false;
                    a7.Visible = false;
                    e7.Visible = false;
                    p7.Visible = false;
                    ec7.Visible = false;
                    est7.Visible = false;
                    b7.Visible = false;
                    s7.Visible = false;
                    Psalud7.Visible = false;
                    o7.Visible = false;
                    i7.Visible = false;
                    ingreso7.Visible = false;

                    titulo8.Visible = false;
                    c8.Visible = false;
                    n8.Visible = false;
                    a8.Visible = false;
                    e8.Visible = false;
                    p8.Visible = false;
                    ec8.Visible = false;
                    est8.Visible = false;
                    b8.Visible = false;
                    s8.Visible = false;
                    Psalud8.Visible = false;
                    o8.Visible = false;
                    i8.Visible = false;
                    ingreso8.Visible = false;

                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
            if (miembros.Equals("6"))
            {
                    titulo7.Visible = false;
                    c7.Visible = false;
                    n7.Visible = false;
                    a7.Visible = false;
                    e7.Visible = false;
                    p7.Visible = false;
                    ec7.Visible = false;
                    est7.Visible = false;
                    b7.Visible = false;
                    s7.Visible = false;
                    Psalud7.Visible = false;
                    o7.Visible = false;
                    i7.Visible = false;
                    ingreso7.Visible = false;

                    titulo8.Visible = false;
                    c8.Visible = false;
                    n8.Visible = false;
                    a8.Visible = false;
                    e8.Visible = false;
                    p8.Visible = false;
                    ec8.Visible = false;
                    est8.Visible = false;
                    b8.Visible = false;
                    s8.Visible = false;
                    Psalud8.Visible = false;
                    o8.Visible = false;
                    i8.Visible = false;
                    ingreso8.Visible = false;

                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
            if (miembros.Equals("7"))
            {
                    titulo8.Visible = false;
                    c8.Visible = false;
                    n8.Visible = false;
                    a8.Visible = false;
                    e8.Visible = false;
                    p8.Visible = false;
                    ec8.Visible = false;
                    est8.Visible = false;
                    b8.Visible = false;
                    s8.Visible = false;
                    Psalud8.Visible = false;
                    o8.Visible = false;
                    i8.Visible = false;
                    ingreso8.Visible = false;

                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
            if (miembros.Equals("8"))
            {
                    titulo9.Visible = false;
                    c9.Visible = false;
                    n9.Visible = false;
                    a9.Visible = false;
                    e9.Visible = false;
                    p9.Visible = false;
                    ec9.Visible = false;
                    est9.Visible = false;
                    b9.Visible = false;
                    s9.Visible = false;
                    Psalud9.Visible = false;
                    o9.Visible = false;
                    i9.Visible = false;
                    ingreso9.Visible = false;

                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }
                if (miembros.Equals("9"))
                {


                    titulo10.Visible = false;
                    c10.Visible = false;
                    n10.Visible = false;
                    a10.Visible = false;
                    e10.Visible = false;
                    p10.Visible = false;
                    ec10.Visible = false;
                    est10.Visible = false;
                    b10.Visible = false;
                    s10.Visible = false;
                    Psalud10.Visible = false;
                    o10.Visible = false;
                    i10.Visible = false;
                    ingreso10.Visible = false;
                }

            }
          
        }

        protected void Sig_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            List<ListaFormulario2> Miembros = new List<ListaFormulario2>();
            string cant = (string)Session["miembros"];
            if (cant.Equals("1"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
            }
            if (cant.Equals("2"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
            }
            if (cant.Equals("3"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
            }
            if (cant.Equals("4"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula4.Text,
                    Nombre = Nombre4.Text,
                    Apellidos = Apellidos4.Text,
                    Edad = Edad4.Text,
                    Parentesco = Parentesco4.Text,
                    Estadocivil = Ecivil4.Text,
                    Estudia = Estudia4.Text,
                    Beca = Beca4.Text,
                    Salud = Salud4.Text,
                    ProblemasSalud = ProblematicaSalud4.Text,
                    Ocupacion = Ocupación4.Text,
                    Instucion = Institución4.Text,
                    IngresoMensual = IngresoMensual4.Text
                });
            }
            if (cant.Equals("5"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula4.Text,
                    Nombre = Nombre4.Text,
                    Apellidos = Apellidos4.Text,
                    Edad = Edad4.Text,
                    Parentesco = Parentesco4.Text,
                    Estadocivil = Ecivil4.Text,
                    Estudia = Estudia4.Text,
                    Beca = Beca4.Text,
                    Salud = Salud4.Text,
                    ProblemasSalud = ProblematicaSalud4.Text,
                    Ocupacion = Ocupación4.Text,
                    Instucion = Institución4.Text,
                    IngresoMensual = IngresoMensual4.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula5.Text,
                    Nombre = Nombre5.Text,
                    Apellidos = Apellidos5.Text,
                    Edad = Edad5.Text,
                    Parentesco = Parentesco5.Text,
                    Estadocivil = Ecivil5.Text,
                    Estudia = Estudia5.Text,
                    Beca = Beca5.Text,
                    Salud = Salud5.Text,
                    ProblemasSalud = ProblematicaSalud5.Text,
                    Ocupacion = Ocupación5.Text,
                    Instucion = Institución5.Text,
                    IngresoMensual = IngresoMensual5.Text
                });
            }
            if (cant.Equals("6"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula4.Text,
                    Nombre = Nombre4.Text,
                    Apellidos = Apellidos4.Text,
                    Edad = Edad4.Text,
                    Parentesco = Parentesco4.Text,
                    Estadocivil = Ecivil4.Text,
                    Estudia = Estudia4.Text,
                    Beca = Beca4.Text,
                    Salud = Salud4.Text,
                    ProblemasSalud = ProblematicaSalud4.Text,
                    Ocupacion = Ocupación4.Text,
                    Instucion = Institución4.Text,
                    IngresoMensual = IngresoMensual4.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula5.Text,
                    Nombre = Nombre5.Text,
                    Apellidos = Apellidos5.Text,
                    Edad = Edad5.Text,
                    Parentesco = Parentesco5.Text,
                    Estadocivil = Ecivil5.Text,
                    Estudia = Estudia5.Text,
                    Beca = Beca5.Text,
                    Salud = Salud5.Text,
                    ProblemasSalud = ProblematicaSalud5.Text,
                    Ocupacion = Ocupación5.Text,
                    Instucion = Institución5.Text,
                    IngresoMensual = IngresoMensual5.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula6.Text,
                    Nombre = Nombre6.Text,
                    Apellidos = Apellidos6.Text,
                    Edad = Edad6.Text,
                    Parentesco = Parentesco6.Text,
                    Estadocivil = Ecivil6.Text,
                    Estudia = Estudia6.Text,
                    Beca = Beca6.Text,
                    Salud = Salud6.Text,
                    ProblemasSalud = ProblematicaSalud6.Text,
                    Ocupacion = Ocupación6.Text,
                    Instucion = Institución6.Text,
                    IngresoMensual = IngresoMensual6.Text
                });
            }
            if (cant.Equals("7"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula4.Text,
                    Nombre = Nombre4.Text,
                    Apellidos = Apellidos4.Text,
                    Edad = Edad4.Text,
                    Parentesco = Parentesco4.Text,
                    Estadocivil = Ecivil4.Text,
                    Estudia = Estudia4.Text,
                    Beca = Beca4.Text,
                    Salud = Salud4.Text,
                    ProblemasSalud = ProblematicaSalud4.Text,
                    Ocupacion = Ocupación4.Text,
                    Instucion = Institución4.Text,
                    IngresoMensual = IngresoMensual4.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula5.Text,
                    Nombre = Nombre5.Text,
                    Apellidos = Apellidos5.Text,
                    Edad = Edad5.Text,
                    Parentesco = Parentesco5.Text,
                    Estadocivil = Ecivil5.Text,
                    Estudia = Estudia5.Text,
                    Beca = Beca5.Text,
                    Salud = Salud5.Text,
                    ProblemasSalud = ProblematicaSalud5.Text,
                    Ocupacion = Ocupación5.Text,
                    Instucion = Institución5.Text,
                    IngresoMensual = IngresoMensual5.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula6.Text,
                    Nombre = Nombre6.Text,
                    Apellidos = Apellidos6.Text,
                    Edad = Edad6.Text,
                    Parentesco = Parentesco6.Text,
                    Estadocivil = Ecivil6.Text,
                    Estudia = Estudia6.Text,
                    Beca = Beca6.Text,
                    Salud = Salud6.Text,
                    ProblemasSalud = ProblematicaSalud6.Text,
                    Ocupacion = Ocupación6.Text,
                    Instucion = Institución6.Text,
                    IngresoMensual = IngresoMensual6.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula7.Text,
                    Nombre = Nombre7.Text,
                    Apellidos = Apellidos7.Text,
                    Edad = Edad7.Text,
                    Parentesco = Parentesco7.Text,
                    Estadocivil = Ecivil7.Text,
                    Estudia = Estudia7.Text,
                    Beca = Beca7.Text,
                    Salud = Salud7.Text,
                    ProblemasSalud = ProblematicaSalud7.Text,
                    Ocupacion = Ocupación7.Text,
                    Instucion = Institución7.Text,
                    IngresoMensual = IngresoMensual7.Text
                });
            }
            if (cant.Equals("8"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula4.Text,
                    Nombre = Nombre4.Text,
                    Apellidos = Apellidos4.Text,
                    Edad = Edad4.Text,
                    Parentesco = Parentesco4.Text,
                    Estadocivil = Ecivil4.Text,
                    Estudia = Estudia4.Text,
                    Beca = Beca4.Text,
                    Salud = Salud4.Text,
                    ProblemasSalud = ProblematicaSalud4.Text,
                    Ocupacion = Ocupación4.Text,
                    Instucion = Institución4.Text,
                    IngresoMensual = IngresoMensual4.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula5.Text,
                    Nombre = Nombre5.Text,
                    Apellidos = Apellidos5.Text,
                    Edad = Edad5.Text,
                    Parentesco = Parentesco5.Text,
                    Estadocivil = Ecivil5.Text,
                    Estudia = Estudia5.Text,
                    Beca = Beca5.Text,
                    Salud = Salud5.Text,
                    ProblemasSalud = ProblematicaSalud5.Text,
                    Ocupacion = Ocupación5.Text,
                    Instucion = Institución5.Text,
                    IngresoMensual = IngresoMensual5.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula6.Text,
                    Nombre = Nombre6.Text,
                    Apellidos = Apellidos6.Text,
                    Edad = Edad6.Text,
                    Parentesco = Parentesco6.Text,
                    Estadocivil = Ecivil6.Text,
                    Estudia = Estudia6.Text,
                    Beca = Beca6.Text,
                    Salud = Salud6.Text,
                    ProblemasSalud = ProblematicaSalud6.Text,
                    Ocupacion = Ocupación6.Text,
                    Instucion = Institución6.Text,
                    IngresoMensual = IngresoMensual6.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula7.Text,
                    Nombre = Nombre7.Text,
                    Apellidos = Apellidos7.Text,
                    Edad = Edad7.Text,
                    Parentesco = Parentesco7.Text,
                    Estadocivil = Ecivil7.Text,
                    Estudia = Estudia7.Text,
                    Beca = Beca7.Text,
                    Salud = Salud7.Text,
                    ProblemasSalud = ProblematicaSalud7.Text,
                    Ocupacion = Ocupación7.Text,
                    Instucion = Institución7.Text,
                    IngresoMensual = IngresoMensual7.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula8.Text,
                    Nombre = Nombre8.Text,
                    Apellidos = Apellidos8.Text,
                    Edad = Edad8.Text,
                    Parentesco = Parentesco8.Text,
                    Estadocivil = Ecivil8.Text,
                    Estudia = Estudia8.Text,
                    Beca = Beca8.Text,
                    Salud = Salud8.Text,
                    ProblemasSalud = ProblematicaSalud8.Text,
                    Ocupacion = Ocupación8.Text,
                    Instucion = Institución8.Text,
                    IngresoMensual = IngresoMensual8.Text
                });
            }
            if (cant.Equals("9"))
            {
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula4.Text,
                    Nombre = Nombre4.Text,
                    Apellidos = Apellidos4.Text,
                    Edad = Edad4.Text,
                    Parentesco = Parentesco4.Text,
                    Estadocivil = Ecivil4.Text,
                    Estudia = Estudia4.Text,
                    Beca = Beca4.Text,
                    Salud = Salud4.Text,
                    ProblemasSalud = ProblematicaSalud4.Text,
                    Ocupacion = Ocupación4.Text,
                    Instucion = Institución4.Text,
                    IngresoMensual = IngresoMensual4.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula5.Text,
                    Nombre = Nombre5.Text,
                    Apellidos = Apellidos5.Text,
                    Edad = Edad5.Text,
                    Parentesco = Parentesco5.Text,
                    Estadocivil = Ecivil5.Text,
                    Estudia = Estudia5.Text,
                    Beca = Beca5.Text,
                    Salud = Salud5.Text,
                    ProblemasSalud = ProblematicaSalud5.Text,
                    Ocupacion = Ocupación5.Text,
                    Instucion = Institución5.Text,
                    IngresoMensual = IngresoMensual5.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula6.Text,
                    Nombre = Nombre6.Text,
                    Apellidos = Apellidos6.Text,
                    Edad = Edad6.Text,
                    Parentesco = Parentesco6.Text,
                    Estadocivil = Ecivil6.Text,
                    Estudia = Estudia6.Text,
                    Beca = Beca6.Text,
                    Salud = Salud6.Text,
                    ProblemasSalud = ProblematicaSalud6.Text,
                    Ocupacion = Ocupación6.Text,
                    Instucion = Institución6.Text,
                    IngresoMensual = IngresoMensual6.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula7.Text,
                    Nombre = Nombre7.Text,
                    Apellidos = Apellidos7.Text,
                    Edad = Edad7.Text,
                    Parentesco = Parentesco7.Text,
                    Estadocivil = Ecivil7.Text,
                    Estudia = Estudia7.Text,
                    Beca = Beca7.Text,
                    Salud = Salud7.Text,
                    ProblemasSalud = ProblematicaSalud7.Text,
                    Ocupacion = Ocupación7.Text,
                    Instucion = Institución7.Text,
                    IngresoMensual = IngresoMensual7.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula8.Text,
                    Nombre = Nombre8.Text,
                    Apellidos = Apellidos8.Text,
                    Edad = Edad8.Text,
                    Parentesco = Parentesco8.Text,
                    Estadocivil = Ecivil8.Text,
                    Estudia = Estudia8.Text,
                    Beca = Beca8.Text,
                    Salud = Salud8.Text,
                    ProblemasSalud = ProblematicaSalud8.Text,
                    Ocupacion = Ocupación8.Text,
                    Instucion = Institución8.Text,
                    IngresoMensual = IngresoMensual8.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula9.Text,
                    Nombre = Nombre9.Text,
                    Apellidos = Apellidos9.Text,
                    Edad = Edad9.Text,
                    Parentesco = Parentesco9.Text,
                    Estadocivil = Ecivil9.Text,
                    Estudia = Estudia9.Text,
                    Beca = Beca9.Text,
                    Salud = Salud9.Text,
                    ProblemasSalud = ProblematicaSalud9.Text,
                    Ocupacion = Ocupación9.Text,
                    Instucion = Institución9.Text,
                    IngresoMensual = IngresoMensual9.Text
                });
            }
            if (cant.Equals("10"))
            {

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula.Text,
                    Nombre = Nombre.Text,
                    Apellidos = Apellidos.Text,
                    Edad = Edad.Text,
                    Parentesco = Parentesco.Text,
                    Estadocivil = Escivil.Text,
                    Estudia = Estudia.Text,
                    Beca = Beca.Text,
                    Salud = Salud.Text,
                    ProblemasSalud = ProblematicaSalud.Text,
                    Ocupacion = Ocupación.Text,
                    Instucion = Institución.Text,
                    IngresoMensual = IngresoMensual.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula2.Text,
                    Nombre = Nombre2.Text,
                    Apellidos = Apellidos2.Text,
                    Edad = Edad2.Text,
                    Parentesco = Parentesco2.Text,
                    Estadocivil = Ecivil2.Text,
                    Estudia = Estudia2.Text,
                    Beca = Beca2.Text,
                    Salud = Salud2.Text,
                    ProblemasSalud = ProblematicaSalud2.Text,
                    Ocupacion = Ocupación2.Text,
                    Instucion = Institución2.Text,
                    IngresoMensual = IngresoMensual2.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula3.Text,
                    Nombre = Nombre3.Text,
                    Apellidos = Apellidos3.Text,
                    Edad = Edad3.Text,
                    Parentesco = Parentesco3.Text,
                    Estadocivil = Ecivil3.Text,
                    Estudia = Estudia3.Text,
                    Beca = Beca3.Text,
                    Salud = Salud3.Text,
                    ProblemasSalud = ProblematicaSalud3.Text,
                    Ocupacion = Ocupación3.Text,
                    Instucion = Institución3.Text,
                    IngresoMensual = IngresoMensual3.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula4.Text,
                    Nombre = Nombre4.Text,
                    Apellidos = Apellidos4.Text,
                    Edad = Edad4.Text,
                    Parentesco = Parentesco4.Text,
                    Estadocivil = Ecivil4.Text,
                    Estudia = Estudia4.Text,
                    Beca = Beca4.Text,
                    Salud = Salud4.Text,
                    ProblemasSalud = ProblematicaSalud4.Text,
                    Ocupacion = Ocupación4.Text,
                    Instucion = Institución4.Text,
                    IngresoMensual = IngresoMensual4.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula5.Text,
                    Nombre = Nombre5.Text,
                    Apellidos = Apellidos5.Text,
                    Edad = Edad5.Text,
                    Parentesco = Parentesco5.Text,
                    Estadocivil = Ecivil5.Text,
                    Estudia = Estudia5.Text,
                    Beca = Beca5.Text,
                    Salud = Salud5.Text,
                    ProblemasSalud = ProblematicaSalud5.Text,
                    Ocupacion = Ocupación5.Text,
                    Instucion = Institución5.Text,
                    IngresoMensual = IngresoMensual5.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula6.Text,
                    Nombre = Nombre6.Text,
                    Apellidos = Apellidos6.Text,
                    Edad = Edad6.Text,
                    Parentesco = Parentesco6.Text,
                    Estadocivil = Ecivil6.Text,
                    Estudia = Estudia6.Text,
                    Beca = Beca6.Text,
                    Salud = Salud6.Text,
                    ProblemasSalud = ProblematicaSalud6.Text,
                    Ocupacion = Ocupación6.Text,
                    Instucion = Institución6.Text,
                    IngresoMensual = IngresoMensual6.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula7.Text,
                    Nombre = Nombre7.Text,
                    Apellidos = Apellidos7.Text,
                    Edad = Edad7.Text,
                    Parentesco = Parentesco7.Text,
                    Estadocivil = Ecivil7.Text,
                    Estudia = Estudia7.Text,
                    Beca = Beca7.Text,
                    Salud = Salud7.Text,
                    ProblemasSalud = ProblematicaSalud7.Text,
                    Ocupacion = Ocupación7.Text,
                    Instucion = Institución7.Text,
                    IngresoMensual = IngresoMensual7.Text
                });

                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula8.Text,
                    Nombre = Nombre8.Text,
                    Apellidos = Apellidos8.Text,
                    Edad = Edad8.Text,
                    Parentesco = Parentesco8.Text,
                    Estadocivil = Ecivil8.Text,
                    Estudia = Estudia8.Text,
                    Beca = Beca8.Text,
                    Salud = Salud8.Text,
                    ProblemasSalud = ProblematicaSalud8.Text,
                    Ocupacion = Ocupación8.Text,
                    Instucion = Institución8.Text,
                    IngresoMensual = IngresoMensual8.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula9.Text,
                    Nombre = Nombre9.Text,
                    Apellidos = Apellidos9.Text,
                    Edad = Edad9.Text,
                    Parentesco = Parentesco9.Text,
                    Estadocivil = Ecivil9.Text,
                    Estudia = Estudia9.Text,
                    Beca = Beca9.Text,
                    Salud = Salud9.Text,
                    ProblemasSalud = ProblematicaSalud9.Text,
                    Ocupacion = Ocupación9.Text,
                    Instucion = Institución9.Text,
                    IngresoMensual = IngresoMensual9.Text
                });
                Miembros.Add(new ListaFormulario2
                {
                    Cedula = Cédula10.Text,
                    Nombre = Nombre10.Text,
                    Apellidos = Apellidos10.Text,
                    Edad = Edad10.Text,
                    Parentesco = Parentesco10.Text,
                    Estadocivil = Ecivil10.Text,
                    Estudia = Estudia10.Text,
                    Beca = Beca10.Text,
                    Salud = Salud10.Text,
                    ProblemasSalud = ProblematicaSalud10.Text,
                    Ocupacion = Ocupación10.Text,
                    Instucion = Institución10.Text,
                    IngresoMensual = IngresoMensual10.Text
                });
            }
            int Im = Convert.ToInt32(IngresoMensual.Text);
            int Im2 = Convert.ToInt32(IngresoMensual2.Text);
            int Im3 = Convert.ToInt32(IngresoMensual3.Text);
            int Im4 = Convert.ToInt32(IngresoMensual4.Text);
            int Im5 = Convert.ToInt32(IngresoMensual5.Text);
            int Im6 = Convert.ToInt32(IngresoMensual6.Text);
            int Im7 = Convert.ToInt32(IngresoMensual7.Text);
            int Im8 = Convert.ToInt32(IngresoMensual8.Text);
            int Im9 = Convert.ToInt32(IngresoMensual9.Text);
            int Im10 = Convert.ToInt32(IngresoMensual10.Text);
            int ae = Convert.ToInt32(ayudaEco.Text);
            int ia = Convert.ToInt32(IngresosA.Text);
            int oa = Convert.ToInt32(otrasayudas.Text);
            int egre = Convert.ToInt32(Egresosm.Text);
            int ingresofamilar = Im + Im2 + Im3 + Im4 + Im5 + Im6 + Im7 + Im8 + Im9 + Im10  + ae + ia + oa;
            foreach (ListaFormulario2 miembro in Miembros)
            {
                control.InsertarMiembro(miembro.Cedula,miembro.Nombre,miembro.Apellidos,miembro.Edad,miembro.Parentesco,miembro.Estadocivil,miembro.Estudia,miembro.Beca,miembro.Salud,miembro.ProblemasSalud,miembro.Ocupacion,miembro.Instucion,miembro.IngresoMensual);
            }
            control.insertmonto(ae, ia, oa,tegreso.Text,egre, ingresofamilar, Convert.ToInt32(cant));
            Session["miembros"] = 0;
           Response.Redirect("instruciones3");

        }

       
    }
}
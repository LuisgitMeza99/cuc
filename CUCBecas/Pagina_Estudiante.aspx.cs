﻿using System;
using CUCBecas.Capa_Datos;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class Pagina_Estudiante : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            string fecha = (string)Session["Fecha"];
            Fecha.Visible = false;
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1"))
                {
                    Response.Redirect("IniciarSesion");
                }
               
            }
            Controlador control = new Controlador();
            string[] val = control.FechasIncio();
            Nom.Text = "Bienvenido " + (string)Session["Nombre"];
        
                Beca.Text = "Su estado de Beca es : " + (string)Session["Estado"];
            if (!fecha.Equals("1800-01-01")) {
                Fecha.Visible = true;
                Fecha.Text = "Y aplica hasta : " + (string)Session["Fecha"];
            }
        
           
        }
    }
}
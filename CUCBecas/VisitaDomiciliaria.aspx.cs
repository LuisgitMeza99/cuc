﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class VisitaDomiciliaria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("2"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
            if (IsPostBack)
            {
                string cedula = Cédula.Text;
                Controlador control = new Controlador();
                string[] datos = control.DatosEstudianteVisitaDom(cedula);
                if (datos[0] == "0")
                {
                    Response.Write("<script>alert('Cedula o Dimex no registrada en el sistema')</script>");
                }
                if (datos[0] == "1")
                {
                    Response.Write("<script>alert('El estudiante no tiene un formulario realizado')</script>");
                }
                if (datos[0] == "2")
                {
                    Nombre.Text = datos[1];
                    Carrera.Text = datos[2];
                    Fecha.Text = datos[3];
                }
            }
        }

        protected void Sig_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            bool val = control.InsertVisitaDom(Cédula.Text, PersonasEntravistadas.Text, Motivo.Text, CondicionA.Text, MediosAceso.Text, MediosAceso.Text, Infraestructura.Text, VulnerabilidadSocial.Text, AccesoCentro.Checked, Escuela.Checked, Electricidad.Checked, Internet.Checked, Cable.Checked, Transporte.Checked, RecoleccionBasura.Checked, CentroSalud.Checked, AguaPotable.Checked, Teléfono.Checked, Alumbrado.Checked, Otros.Checked, FuentesEmpleo.SelectedValue, Recreacion.Checked, NumeroIntegrantes.Text, NumeroTrabajadores.Text, Convert.ToBoolean(Convert.ToInt32(Jmujeres.SelectedValue)), NumeroAdultasMayores.Text, NumeroDiscapacidados.Text, NumeroEstudiantes.Text, SituacioneSalud.Text, SituacionDesempleo.Text, Adicciones.SelectedValue, observaciondeProblemas.Text, Violencia.SelectedValue, FaltaApoyo.Text, Tenecia.Text, Adquision.Text, Estado.Text, Aposentos.Text, ducha.Checked, microondas.Checked, Computadora.Checked, Consola.Checked, Radio.Checked, Televisor.Checked, CocinaE.Checked, Refrigeradora.Checked, Lavadora.Checked, CocinaL.Checked, Observaciones.Text, Beca.SelectedValue, NomEntre.Text, CedEntre.Text);
            if (val == true)
            {
                Response.Redirect("PrincipalTrabajoSocial");
            }
        }
    }
}
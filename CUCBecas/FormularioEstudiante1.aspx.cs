﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class FormularioEstudiante1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["IDForm"] = "";
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            string cod = (string)Session["cod"];
           
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1") || !cod.Equals("true"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
            if (!IsPostBack)
            {
                Inciollenar();
            }
            Razondeperdida.Enabled = false;
            Tipobeca.Enabled = false;
            if (condicion.Items[0].Selected)
            {
               Tipobeca.Enabled = true;
                Razondeperdida.Enabled = true;
            }
        
        }

        protected void Sig_Click(object sender, EventArgs e)
        {
            string nacionaliadad =Nacionalidad.Text;
            string tcel = Telcel.Text;
            string tdom = TelDom.Text;
            string level = Nivel.Text;
            string eci = Ecivil.Text;
            string pro = provincia.Text;
            string can = cantones.Text;
            string dis = distritos.Text;
            string direccion = DirExa.Text;
            string cont = "";

            if (Tipobeca.Enabled == true)
            {
                cont = condicion.Text + ", Beca anterior :" + Tipobeca.Text+ ", Razon de perdida de la Beca : "+Razondeperdida.Text;
            }
            else
            {
                cont = condicion.Text;
            }
            Controlador control = new Controlador();
            control.insertform1(nacionaliadad, tcel, tdom, eci,level,pro,can,dis,direccion,cont) ;
            Response.Redirect("instruciones2");
        }
        private void Inciollenar()
        {
            provincia.DataSource = Consultar("Select * from Provincias");
            provincia.DataTextField = "NombreProvincia";
            provincia.DataValueField = "idProvincia";
            provincia.DataBind();
            provincia.Items.Insert(0, new ListItem("Selecione su provincia", "0"));
            cantones.Items.Insert(0, new ListItem("Selecione", "0"));
            distritos.Items.Insert(0, new ListItem("Selecione", "0"));
        }
        public DataSet Consultar(string strsql)
        {
            string conexion = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;
            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand cmd = new SqlCommand(strsql, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            return ds;
        }

        protected void ProviciaSelecioneda(object sender, EventArgs e)
        {
            int idprovincia = Convert.ToInt32(provincia.SelectedValue);
            cantones.DataSource = Consultar("Select * from Cantones where idProvincia =" + idprovincia);
            cantones.DataTextField = "NombreCanton";
            cantones.DataValueField = "idCanton";
            cantones.DataBind();
            cantones.Items.Insert(0, new ListItem("Selecione su Canton", "0"));
        }

        protected void CantonSelecionedo(object sender, EventArgs e)
        {
            int idCanton = Convert.ToInt32(cantones.SelectedValue);
            distritos.DataSource = Consultar("Select * from Distrito where idCanton =" + idCanton);
            distritos.DataTextField = "NombreDistrito";
            distritos.DataValueField = "idDistrito";
            distritos.DataBind();
            distritos.Items.Insert(0, new ListItem("Selecione su Distrito", "0"));
        }
    }
}
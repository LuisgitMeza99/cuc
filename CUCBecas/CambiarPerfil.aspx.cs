﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class CambiarPerfil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1"))
                {
                    Response.Redirect("IniciarSesion");
                }
                Controlador control = new Controlador();
                string[] datos = control.TraerPerfil((string)Session["IDusuario"]);
                Nombre.Text = datos[0];
                Apellidos.Text = datos[1];
                Correo.Text = datos[2];
                Carrera.Text = datos[3];
            }
          
        }

        protected void Entrar2_Click(object sender, EventArgs e)
        {
            Response.Redirect("CambiarPerfilContra");
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            string nombre = Nombre.Text;
            string ape = Apellidos.Text;
            string correo = Correo.Text;
            string carrera = Carrera.Text;
            int valido = control.actualizarPerfil(nombre, ape, correo, carrera);
            if (valido == 1)
                {
                Response.Redirect("Pagina_Estudiante");
            }
            else
            {
                Response.Write("<script>alert('Hubo un error por favor informar')</script>");
            }
        }
    }
}
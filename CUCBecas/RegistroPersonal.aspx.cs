﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class RegistroPersonal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("4"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            string contra = password.Text;
            if (contra.Equals(password2.Text))
            {
                if (contra.Length >= 8)
                {
                    Controlador control = new Controlador();
                    string NombreFull = Nombre.Text + " " + Apellidos.Text;
                    string cedu = Cedula.Text;
                    string nom = Nombre.Text;
                    string apellidos = Apellidos.Text;
                    string correo = Correo.Text;
                    string departamento = Departamento.Text;
                  

                    float Registro = control.Registropersonal(cedu, nom, apellidos, correo, departamento, contra);
                    if (Registro == 1)
                    {
                        control.EnviarCorreoPersonal(NombreFull, correo);
                        Response.Redirect("PrincipalAdmin");
                    }
                    else
                    {
                        Response.Write("<script>alert('Cedula  ya registrados en el sistema')</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Las Contraseña tiene que tener mas de 8 caracteres')</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Las Contraseña no coincide')</script>");
            }

        }
    }
}
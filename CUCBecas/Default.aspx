﻿<%@ Page Title="Home Page" Language="C#"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CUCBecas._Default" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
    <body>
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"   class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="IniciarSesion.aspx">Iniciar Sesión</a>
          <a href="Contact.aspx">Contactenos</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
        <div class="espacio">
 </div>
        
    
 <form runat="server" >
   
  <div class="rowb">
      <div class="colb un">
           <span><h2>Pasos el proceso de Beca Socioeconómica :</h2><br></span>
      <span>1) Registrarse en nuestra página.</span>         
    </div>
    </div>
         

    <div class="rowb">
    <div class="colb dos">
      <span>2) Pagar el derecho del formulario con un costo de <asp:Label ID="Label1" runat="server" ></asp:Label> tiene tiempo hasta el  Día : <asp:Label ID="Label2" runat="server" ></asp:Label> para pagar .</span>
    </div>
    </div>


     <div class="rowb">
    <div class="colb tre">
      <span>3) En la opción formulario poner el Serial de su factura y le llegara un email con un código que tendrá que ingresar , si no ve el correo busque en no deseado.
</span>
    </div>
    </div>

    <div class="rowb">
     <div class="colb for">
      <span>4) Leer el documento adjunto al email anterior donde tendra la información a cerca de los anexos.</span>
    </div>
  </div>
        <div class="rowb">
     <div class="colb five">
      <span>5) Llenar el formulario con los datos solicitados , los anexos tienen que estar en un solo pdf y en el orden indicado , <br>tiene tiempo hasta el  día : <asp:Label ID="Label3" runat="server" ></asp:Label> para llenarlo. </span>
    </div>
  </div>

     <div class="rowb">
     <div class="colb un">
      <span>6) El  día : <asp:Label ID="Label4" runat="server" ></asp:Label> se le avisará de su resultado de beca </span>
    </div>
  </div>
      <div class="rowb">
     <div  class="colb for">
      <span>7) Cualquier consulta (incluyendo si no ve reflejado su resultado en la página) o apelación sobre el otorgamiento de beca se puede apersonar a la Unidad de Trabajo Social en el siguiente horario: 2:00pm a 3:30pm del  : <asp:Label ID="Label5" runat="server" ></asp:Label> al : <asp:Label ID="Label6" runat="server" ></asp:Label>  </span><br><br> <span>NOTA: Estos días son correspondientes a los TRES ÚNICOS días estipulados para apelaciones, por lo que después de estas fechas no se recibirá apelación alguna. Es decir, despues de estas fechas no se hará ningún cambio en el resultado de becas. </span>
    </div>
  </div>
    </form>
 <div class="espacio">
 </div>

    

        <!-- COMIENZA EL FOOTER -->  
        
        <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>


                        

                    </div>

                </div>
          </footer>
</body>
</html>

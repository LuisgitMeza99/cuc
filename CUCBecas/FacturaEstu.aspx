﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FacturaEstu.aspx.cs" Inherits="CUCBecas.FacturaEstu" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Comprobar</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body class="bodyEs">
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio"> 
         <div class="espacio">
    </div>
  <div class="rowb">
        <div class="colb un">
            <span>
                <h2>Digite el serial de su factura </h2>
            </span>
            <br>
             <form class="form" runat="server">   
         <span> <asp:Label ID="Label" runat="server"  Text="El serial de su factura solo es si no tienes un código valido"></asp:Label><br></span><br>
        <asp:TextBox ID="Factura" runat="server" placeholder="Serial de su factura"  class="log"></asp:TextBox>
              <asp:Button ID="Entrar" runat="server" Text="Ingresar" OnClick="Entrar_Click"   /> 
             <asp:TextBox ID="Codigo" runat="server" placeholder="Código"   class="log"></asp:TextBox>
   
                   <asp:Button ID="Entrar2" runat="server" Text="Ir al formulario" OnClick="Entrar2_Click"   /> 
                  
     </form>
                 </div>
    </div>

    <div class="espacio">
    </div>


   
      
     
<!-- COMIENZA EL FOOTER -->

    <div class="clear"></div>
    <footer>
        <div id="content_footer" class="container">

            <ul id="nav_footer">
                <a href="" class="logo_footer">
                    <img src="img/logo_cuc.png" /></a>
            </ul>

            <div class="clear"></div>

            <div class="content_info_footer ">

                <span>Colegio Universitario de Cartago</span>
                <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                <a href="">Teléfono:2550-6100</a>
                <script>
                    (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date(); a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-54698007-1', 'auto');
                    ga('send', 'pageview');

                </script>



            </div>

        </div>
    </footer>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditarPf.aspx.cs" Inherits="CUCBecas.EditarPf" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Editar parametros</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    
   

</head>
    <body class="overlay">
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"  class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="IniciarSesion.aspx">Iniciar Sesión</a>
          <a href="Contact.aspx">Contactenos</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
  
          
   <form id="form1" runat="server"  class="registro">
      
      <asp:TextBox ID="Coste" runat="server" placeholder="Coste"  required="Falta tu cédula o dimex" class="select" onkeypress="return numbersonly(event);"  ></asp:TextBox>
       <asp:TextBox ID="Horario" runat="server" placeholder="Horario"  required="Falta tu Nombre" class="select" TextMode="MultiLine"></asp:TextBox>
       
     <asp:Button ID="Entrar" runat="server" Text="Actualizar" OnClick="Entrar_Click"  />
</form>
       
      <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>


                        

                    </div>

                </div>
          </footer>
     <script type="text/javascript">
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode
        if (unicode != 8 && unicode != 44) {
            if (unicode < 48 || unicode > 57) //if not a number
            { return false } //disable key press    
        }
    }  
</SCRIPT>      
</body>
</html>
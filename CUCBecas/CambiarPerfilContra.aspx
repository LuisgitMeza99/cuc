﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CambiarPerfilContra.aspx.cs" Inherits="CUCBecas.CambiarPerfilContra" %>

 <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Cambiar Contraseña</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    
   

</head>
    <body class="overlay">
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"  class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="Pagina_Estudiante.aspx">Salir</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
  
          
   <form id="form1" runat="server"  class="registro">
      
    <asp:TextBox ID="password" runat="server" placeholder="Contraseña" TextMode="Password" required="Falta tu  Contraseña" class="select"  ></asp:TextBox>
        <asp:TextBox ID="password2" runat="server" placeholder="Repita la Contraseña" TextMode="Password" required="Falta tu Contraseña" class="select" ></asp:TextBox> 
     <asp:Button ID="Entrar" runat="server" Text="Cambiar contraseña" OnClick="Entrar_Click" />
</form>
       
      <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>


                        

                    </div>

                </div>
          </footer>
    
</body>
</html>
﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class FacturaEstu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1"))
                {
                    Response.Redirect("IniciarSesion");
                }
               
                Controlador control = new Controlador();
                string val = control.Entrarformulario();

                if (val.Equals("1"))
                {
                    Response.Write("<script>alert('Ya paso el tiempo de llenar el formulario')</script>");
                    Response.Write("<script>setTimeout(\"location.href = 'Pagina_Estudiante.aspx';\",5);</script>");
                }
            }
            
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            string fac = Factura.Text;
           string pas = control.ValidacionFactura(fac);

            if (pas.Equals("1"))
            {
                Response.Write("<script>alert('Se envio un correo con el codigo')</script>");
            }
            else
            {
                Response.Write("<script>alert('Esta factura ya fue usada')</script>");
            }
          
        }

        protected void Entrar2_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            Session["token"] = Codigo.Text;
            string token= Codigo.Text;
            Boolean val = control.ValidacionFacturaCodigo(token);
            if (val == true)
            {
                Session["cod"] = "true";
                Response.Redirect("instruciones1");
            }
            else
            {
                Response.Write("<script>alert('Esta Codigo ya no es valido')</script>");
            }
        }
    }
}
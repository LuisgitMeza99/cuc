﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class CambiarPerfilPersonal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("2") && !seccion.Equals("3") )
                {
                    Response.Redirect("IniciarSesion");
                }
                Controlador control = new Controlador();
                string[] datos = control.TraerPerfilPersonal();
                Nombre.Text = datos[0];
                Apellidos.Text = datos[1];
                Correo.Text = datos[2];
                Departamento.Text = datos[3];
            }
        }

        protected void Entrar2_Click(object sender, EventArgs e)
        {
            Response.Redirect("CambiarPerfilPersonalContra");
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            string nombre = Nombre.Text;
            string ape = Apellidos.Text;
            string correo = Correo.Text;
            string Departament = Departamento.Text;
            int valido = control.actualizarPersonal(nombre, ape, correo, Departament);
            if (valido == 1)
            {
                string seccion = (string)Session["sesion"];
                if (seccion.Equals("2"))
                {
                    Response.Redirect("PrincipalTrabajoSocial");
                }
                if (seccion.Equals("3"))
                {
                    Response.Redirect("PrincipalFinanciero");
                }
             
                }
            else
            {
                Response.Write("<script>alert('Hubo un error por favor informar')</script>");
            }
        }
    }
}
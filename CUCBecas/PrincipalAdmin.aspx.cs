﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class PrincipalAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack) {
     
                if (string.IsNullOrEmpty(id)  || !seccion.Equals("4"))
            {
                Response.Redirect("IniciarSesion");
            }
                Controlador control = new Controlador();
                string [] val = control.perfiladmin();
                Session["id"] = val[0];
                Session["Nombre"] = val[1];
                Session["Email"] = val[2];
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CambiarPerfilPersonal.aspx.cs" Inherits="CUCBecas.CambiarPerfilPersonal" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Perfil</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    
   

</head>
    <body class="overlay">
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"  class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
       <a href="PrincipalTrabajoSocial.aspx">Salir</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
  
          
   <form id="form1" runat="server"  class="registro">
      
       <asp:TextBox ID="Nombre" runat="server" placeholder="Nombre"  required="Falta tu Nombre" class="select"></asp:TextBox>
         <asp:TextBox ID="Apellidos" runat="server" placeholder="Apellidos"  required="Faltan tus Apellidos" class="select"></asp:TextBox>
        <asp:TextBox ID="Correo" runat="server" placeholder="Correo" TextMode="Email" required="Falta tu Correo" class="select"></asp:TextBox>
    <asp:DropDownList ID="Departamento" runat="server" AppendDataBoundItems="true" required="Falta tu sexo" class="select">
       <asp:ListItem  Value="">Seleccione su Departamento</asp:ListItem>
       <asp:ListItem Value="Unidad de trabajo Social">Unidad de trabajo Social</asp:ListItem>
       <asp:ListItem Value="Financiero">Financiero</asp:ListItem>
       </asp:DropDownList>
  <asp:Button ID="Entrar2" runat="server" Text="Cambiar Contraseña" OnClick="Entrar2_Click" />
     <asp:Button ID="Entrar" runat="server" Text="Actualizar Informacion" OnClick="Entrar_Click" />
</form>
       
      <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>


                        

                    </div>

                </div>
          </footer>
     <script type="text/javascript">
         function numbersonly(e) {
             var unicode = e.charCode ? e.charCode : e.keyCode
             if (unicode != 8 && unicode != 44) {
                 if (unicode < 48 || unicode > 57) //if not a number
                 { return false } //disable key press    
             }
         }  
</SCRIPT>      
</body>
</html>
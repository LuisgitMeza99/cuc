﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularioEstudiante2.aspx.cs" Inherits="CUCBecas.FormularioEstudiante2" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Formulario</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body class="bodyEs">
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
    <div class="containerEs">
        <form runat="server" class="formEs">
            <div class="divEs">
                <h1 class="titulos">➤ Primer miembro del grupo familiar </h1>


                <label class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" ReadOnly="True"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre" runat="server" placeholder="Nombre" class="inputEs" ReadOnly="True"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos" runat="server" placeholder="Apellidos" class="inputEs" ReadOnly="True"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                   Fecha de Nacimiento<br>

                    <asp:TextBox ID="Edad" runat="server" placeholder="Edad" class="inputEs" ReadOnly="True" ></asp:TextBox>
                </label>

                <label class="labelEs">
                    Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco" runat="server" placeholder="Parentesco" class="inputEs" Text="Solicitante" ReadOnly="True"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Estado Civil<br>
                    <asp:TextBox ID="Escivil" runat="server" placeholder="Ecivil" class="inputEs" ReadOnly="True"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Si" class="inputradioEs" Selected="True"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </label>

                <label class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs" Selected="True"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </label>

                <label class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud" runat="server" AppendDataBoundItems="true" required="" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
                    Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </label>




                <!--Segunda persona -->


                <asp:Label ID="titulo2" runat="server" class="titulos"><h1 >➤ Segundo miembro del grupo familiar</h1></asp:Label>


                <asp:Label ID="c2" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula2" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="n2" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre2" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a2" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos2" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e2" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad2" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="p2" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco2" runat="server" placeholder="Parentesco" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec2" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil2" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est2" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia2" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b2" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca2" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s2" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud2" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud2" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud2" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o2" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación2" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i2" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución2" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso2" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual2" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>


                <!--Tercera persona persona -->


                <asp:Label ID="titulo3" runat="server" class="titulos">   <h1 >➤ Tercer miembro del grupo familiar</h1></asp:Label>

                <asp:Label ID="c3" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula3" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n3" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre3" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a3" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos3" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e3" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad3" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>
                </asp:Label>


                <asp:Label ID="p3" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco3" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec3" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil3" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est3" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia3" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b3" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca3" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s3" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud3" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud3" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud3" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o3" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación3" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i3" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución3" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso3" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual3" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>


                <!--Cuarta persona persona -->


                <asp:Label ID="titulo4" runat="server" class="titulos">   <h1> ➤ Cuarto miembro del grupo familiar</h1></asp:Label>


                <asp:Label ID="c4" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula4" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n4" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre4" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a4" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos4" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e4" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad4" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>

                </asp:Label>


                <asp:Label ID="p4" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco4" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec4" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil4" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est4" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia4" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b4" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca4" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s4" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud4" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud4" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud4" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o4" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación4" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i4" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución4" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso4" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual4" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>

                <!--Quinta persona persona -->


                <asp:Label ID="titulo5" runat="server" class="titulos">   <h1 >➤ Quinto miembro del grupo familiar</h1></asp:Label>

                <asp:Label ID="c5" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula5" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n5" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre5" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a5" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos5" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e5" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad5" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="p5" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco5" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec5" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil5" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est5" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia5" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b5" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca5" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s5" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud5" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud5" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud5" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o5" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación5" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i5" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución5" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso5" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual5" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>

                <!--sexta persona -->


                <asp:Label ID="titulo6" runat="server" class="titulos">   <h1 >➤ Sexto miembro del grupo familiar</h1></asp:Label>


                <asp:Label ID="c6" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula6" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n6" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre6" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a6" runat="server" class="labelEs" required="cedula">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos6" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e6" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad6" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>

                </asp:Label>

                <asp:Label ID="p6" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco6" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec6" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil6" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est6" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia6" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b6" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca6" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s6" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud6" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="Psalud6" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud6" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o6" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación6" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i6" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución6" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso6" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual6" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>

                <!--setima persona -->


                <asp:Label ID="titulo7" runat="server" class="titulos">   <h1 >➤ Séptimo miembro del grupo familiar</h1></asp:Label>


                <asp:Label ID="c7" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula7" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n7" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre7" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a7" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos7" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e7" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad7" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="p7" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco7" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec7" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil7" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est7" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia7" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b7" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca7" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s7" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud7" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud7" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud7" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o7" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación7" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i7" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución7" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso7" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual7" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>

                <!--octava persona -->


                <asp:Label ID="titulo8" runat="server" class="titulos">   <h1 >➤ Octavo miembro del grupo familiar</h1></asp:Label>




                <asp:Label ID="c8" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula8" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n8" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre8" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a8" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos8" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e8" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad8" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="p8" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco8" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec8" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil8" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est8" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia8" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b8" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca8" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s8" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud8" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud8" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud8" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o8" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación8" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i8" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución8" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso8" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual8" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>

                <!--novena persona -->


                <asp:Label ID="titulo9" runat="server" class="titulos">   <h1 >➤ Noveno miembro del grupo familiar</h1></asp:Label>




                <asp:Label ID="c9" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula9" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n9" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre9" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a9" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos9" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e9" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad9" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="p9" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco9" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec9" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil9" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est9" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia9" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b9" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca9" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s9" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud9" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud9" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud9" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o9" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación9" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i9" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución9" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ingreso9" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    ₡
                    <asp:TextBox ID="IngresoMensual9" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>

                <!--decima persona -->


                <asp:Label ID="titulo10" runat="server" class="titulos">   <h1 >➤ Decimo miembro del grupo familiar</h1></asp:Label>



                <asp:Label ID="c10" runat="server" class="labelEs">
                    <br>
                    Cédula<br>
                    <asp:TextBox ID="Cédula10" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula"  MaxLength="12"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="n10" runat="server" class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre10" runat="server" placeholder="Nombre" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="a10" runat="server" class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos10" runat="server" placeholder="Apellidos" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="e10" runat="server" class="labelEs">
                    <br>
                    Fecha de Nacimiento<br>
                    <asp:TextBox ID="Edad10" runat="server" placeholder="Edad" class="inputEs" TextMode="Date" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="p10" runat="server" class="labelEs">Parentesco con el
                    <br>
                    / la estudiante que solicita la beca<br>
                    <asp:TextBox ID="Parentesco10" runat="server" placeholder="Parentesco" class="inputEs" required="cedula"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="ec10" runat="server" class="labelEs">
                    <br>
                    Estado Civil<br>

                    <asp:DropDownList ID="Ecivil10" runat="server" AppendDataBoundItems="true" required="Falta tu Estado Civil" class="inputEs" >
                        <asp:ListItem Value="">Seleccione su Estado Civil</asp:ListItem>
                        <asp:ListItem Value="Soltero(a)">Soltero(a)</asp:ListItem>
                        <asp:ListItem Value="Union Libre">Union Libre</asp:ListItem>
                        <asp:ListItem Value="Casado(a)">Casado(a)</asp:ListItem>
                        <asp:ListItem Value="Divorciado(a)">Divorciado(a)</asp:ListItem>
                        <asp:ListItem Value="Viudo(a)">Viudo(a)</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>

                <asp:Label ID="est10" runat="server" class="labelEs">
                    <br>
                    Estudia<br>

                    <asp:RadioButtonList ID="Estudia10" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="b10" runat="server" class="labelEs">
                    <br>
                    Beca<br>

                    <asp:RadioButtonList ID="Beca10" runat="server" RepeatDirection="Horizontal" required="cedula">
                        <asp:ListItem Value="Si" class="inputradioEs"> Si</asp:ListItem>
                        <asp:ListItem Value="No" class="inputradioEs"> No</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Label>

                <asp:Label ID="s10" runat="server" class="labelEs">
                    <br>
                    Salud<br>
                    <asp:DropDownList ID="Salud10" runat="server" AppendDataBoundItems="true" required="Falta su tipo de beca anterior" class="inputEs">
                        <asp:ListItem Value="">Seleccione su Salud</asp:ListItem>
                        <asp:ListItem Value="Muy Buena">Muy Buena</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Normal">Normal</asp:ListItem>
                        <asp:ListItem Value="Mala">Mala</asp:ListItem>
                        <asp:ListItem Value="Muy Mala">Muy Mala</asp:ListItem>
                    </asp:DropDownList>
                </asp:Label>
                <asp:Label ID="Psalud10" runat="server" class="labelEs">Problematica de Salud
                    <br>
                    (Si selecionó mala o muy mala)<br>
                    <asp:TextBox ID="ProblematicaSalud10" runat="server" placeholder=" Problematica de Salud " class="inputEs" TextMode="MultiLine" Rows="2" cols="100" MaxLength="200"></asp:TextBox>
                </asp:Label>
                <asp:Label ID="o10" runat="server" class="labelEs">Ocupación
                    <br>
                    (Tipo de trabajo)<br>
                    <asp:TextBox ID="Ocupación10" runat="server" placeholder="Ocupación" class="inputEs"></asp:TextBox>
                </asp:Label>

                <asp:Label ID="i10" runat="server" class="labelEs">Institución donde labora
                    <br>
                    o que otorga la pensión / Beca<br>
                    <asp:TextBox ID="Institución10" runat="server" placeholder="Institución" class="inputEs"></asp:TextBox>
                </asp:Label>


                <asp:Label ID="ingreso10" runat="server" class="labelEs">Ingreso Mensual Bruto
                    <br>
                    (Por salario / Pensión / Beca)<br>
                    <asp:TextBox ID="IngresoMensual10" runat="server" placeholder="" class="inputEs" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </asp:Label>

                <label class="titulos">
                    <label class="labelEs">
                        Ayuda económica de familiares (que no vivan en la casa), de particulares u otras ayudas<br>
                        (Fundaciones, grupos religiosos, organizaciones, comunales entre otros)
                        <br>
                        ₡
                        <asp:TextBox ID="ayudaEco" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                    </label>
                </label>
                <label class="titulos">
                    <label class="labelEs">
                        Ingresos (lo que en el hogar se recibe) por concepto de alquileres de
                        <br>
                        locales, casas, cocheras, lotes, vehículos, entre otros.<br>
                        ₡
                        <asp:TextBox ID="IngresosA" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                    </label>
                </label>
                <label class="titulos">
                    <label class="labelEs">
                        Otras ayudas que ingresan mensual al hogar y
                        <br>
                        que se reciben en especie, indique un monto aproximado<br>
                        ₡
                        <asp:TextBox ID="otrasayudas" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                    </label>
                </label>

                <label class="labelEs">
                    <br>
                    Tipo de Egreso<br>
                    <asp:DropDownList ID="tegreso" runat="server" AppendDataBoundItems="true" class="inputEs">
                        <asp:ListItem Value="">Seleccione su tipo de egreso</asp:ListItem>
                        <asp:ListItem Value="Alquiler">Alquiler de departamento</asp:ListItem>
                        <asp:ListItem Value="Hipoteca">Hipoteca de hogar</asp:ListItem>
                        <asp:ListItem Value="Prestamo">Prestamo por hogar</asp:ListItem>
                        <asp:ListItem Value="Alquiler y Prestamo">Alquiler de departamento y prestamo por hogar</asp:ListItem>
                        <asp:ListItem Value="Alquiler e Hipoteca">Alquiler de departamento e Hipoteca de hogar</asp:ListItem>
                        <asp:ListItem Value="Hipoteca">Hipoteca de hogar e prestamo por hogar</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
                    <br>
                    Indique el monto mensual del egreso<br>
                    ₡
                    <asp:TextBox ID="Egresosm" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" Text="0" min="0"></asp:TextBox>
                </label>

            </div>
            <div class="centrado">
            <a href="instruciones2.aspx" class="buttonEs">Ver Instrucciónes</a>

            <asp:Button ID="Sig" runat="server" Text="Guardar y Seguir" class="buttonEs" OnClick="Sig_Click" />
 </div>
        </form>
    </div>
    <script type="text/javascript">
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 44) {
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }
    </script>
</body>
</html>

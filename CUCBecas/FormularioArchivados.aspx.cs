﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class FormularioArchivados : System.Web.UI.Page
    {
        string conexion = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("2"))
                {
                    Response.Redirect("IniciarSesion");
                }
                Inciollenar();
            }
            if (IsPostBack)
            {
                using (SqlConnection cn = new SqlConnection(conexion))
                {
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("TablaRegistro", cn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@cuatri", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters.Add("@ano", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters.Add("@cedu", SqlDbType.VarChar, 12);
                    da.SelectCommand.Parameters["@cuatri"].Value = Cuatri.Text;
                    da.SelectCommand.Parameters["@ano"].Value = Ano.Text;
                    da.SelectCommand.Parameters["@cedu"].Value = Cedula.Text;
                   DataTable dt = new DataTable();
                    da.Fill(dt);
                    this.Formulario.DataSource = dt;
                    Formulario.DataBind();
                    cn.Close();
                }
            }

        }
        public DataSet Consultar(string strsql)
        {

            SqlConnection con = new SqlConnection(conexion);
            con.Open();
            SqlCommand cmd = new SqlCommand(strsql, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            return ds;
        }
        private void Inciollenar()
        {
            Ano.DataSource = Consultar("select Año from Formularios group by Año");
            Ano.DataTextField = "Año";
            Ano.DataValueField = "Año";
            Ano.DataBind();
            Ano.Items.Insert(0, new ListItem("Selecione el año", "0"));

        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            string id = ((sender as LinkButton).CommandArgument).ToString();
            byte[] bytes;
            string fileName, contentType;
            string constr = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select NombreAnexo, Anexos, TipodeContenido from Formularios where IDFormulario=@Id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        sdr.Read();
                        bytes = (byte[])sdr["Anexos"];
                        contentType = sdr["TipodeContenido"].ToString();
                        fileName = sdr["NombreAnexo"].ToString();
                    }
                    con.Close();
                }
            }
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
        protected void Formulario_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                //
                // Se obtiene indice de la row seleccionada
                //
                int index = Convert.ToInt32(e.CommandArgument);

                //
                // Obtengo el id de la entidad que se esta editando
                // en este caso de la entidad Person
                //
                Session["NFormulario"] = Formulario.DataKeys[index].Value;
                Response.Write("<script>location.href='RevisiondelEstudiante.aspx';</script>");

            }
        }

        protected void Formulario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ingresoper = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Ingreso Per Cápita"));
                string beca = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Categoria de Beca"));
                if (ingresoper > 3500)
                {
                    if (beca.Equals("Beca 0"))
                    {
                        e.Row.BackColor = System.Drawing.Color.ForestGreen;
                    }
                    else if(beca.Equals("Beca 1"))
                    {
                        e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                    }
                    else if (beca.Equals("Beca 2"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Goldenrod;
                    }
                    else if (beca.Equals("Beca 3"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                    else if (beca.Equals("Beca 4"))
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }
                    else if (beca.Equals("No Aplica"))
                    {
                        e.Row.BackColor = System.Drawing.Color.SlateGray;
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }

                }

                if (ingresoper <= 3500 && ingresoper >= 3101)
                {
                    if (beca.Equals("Beca 0"))
                    {
                        e.Row.BackColor = System.Drawing.Color.ForestGreen;
                    }
                    else if (beca.Equals("Beca 1"))
                    {
                        e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                    }
                    else if (beca.Equals("Beca 2"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Goldenrod;
                    }
                    else if (beca.Equals("Beca 3"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                    else if (beca.Equals("Beca 4"))
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }
                    else if (beca.Equals("No Aplica"))
                    {
                        e.Row.BackColor = System.Drawing.Color.SlateGray;
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }
                }

                if (ingresoper <= 3100 && ingresoper >= 2601)
                {
                    if (beca.Equals("Beca 0"))
                    {
                        e.Row.BackColor = System.Drawing.Color.ForestGreen;
                    }
                    else if (beca.Equals("Beca 1"))
                    {
                        e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                    }
                    else if (beca.Equals("Beca 2"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Goldenrod;
                    }
                    else if (beca.Equals("Beca 3"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                    else if (beca.Equals("Beca 4"))
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }
                    else if (beca.Equals("No Aplica"))
                    {
                        e.Row.BackColor = System.Drawing.Color.SlateGray;
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                }

                if (ingresoper <= 2600 && ingresoper >= 2101)
                {
                    if (beca.Equals("Beca 0"))
                    {
                        e.Row.BackColor = System.Drawing.Color.ForestGreen;
                    }
                    else if (beca.Equals("Beca 1"))
                    {
                        e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                    }
                    else if (beca.Equals("Beca 2"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Goldenrod;
                    }
                    else if (beca.Equals("Beca 3"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                    else if (beca.Equals("Beca 4"))
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }
                    else if (beca.Equals("No Aplica"))
                    {
                        e.Row.BackColor = System.Drawing.Color.SlateGray;
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.Goldenrod;
                    }
                }
                if (ingresoper <= 2100 && ingresoper >= 1801)
                {
                    if (beca.Equals("Beca 0"))
                    {
                        e.Row.BackColor = System.Drawing.Color.ForestGreen;
                    }
                    else if (beca.Equals("Beca 1"))
                    {
                        e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                    }
                    else if (beca.Equals("Beca 2"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Goldenrod;
                    }
                    else if (beca.Equals("Beca 3"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                    else if (beca.Equals("Beca 4"))
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }
                    else if (beca.Equals("No Aplica"))
                    {
                        e.Row.BackColor = System.Drawing.Color.SlateGray;
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                    }
                }

                if (ingresoper <= 1800)
                {
                    if (beca.Equals("Beca 0"))
                    {
                        e.Row.BackColor = System.Drawing.Color.ForestGreen;
                    }
                    else if (beca.Equals("Beca 1"))
                    {
                        e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                    }
                    else if (beca.Equals("Beca 2"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Goldenrod;
                    }
                    else if (beca.Equals("Beca 3"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                    }
                    else if (beca.Equals("Beca 4"))
                    {
                        e.Row.BackColor = System.Drawing.Color.OrangeRed;
                    }
                    else if (beca.Equals("No Aplica"))
                    {
                        e.Row.BackColor = System.Drawing.Color.SlateGray;
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.ForestGreen;
                    }
                }


            }
        }

        
    }
}
﻿
using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{

    public partial class RevisiondelEstudiante : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("2"))
                {
                    Response.Redirect("IniciarSesion");
                }
                Controlador control = new Controlador();
               string [] val = control.DatosAnterioresdebeca();
                Beca.Text = val[0];
                Observaciones.Text = val[1];
                string conexion = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(conexion))
                {
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("DatosRevisonFormulario", cn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@idfor", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters["@idfor"].Value = Session["NFormulario"];
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    this.Estudiante.DataSource = dt;
                    Estudiante.DataBind();
                    cn.Close();
                }
                using (SqlConnection cn = new SqlConnection(conexion))
                {
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("DatosRevisionDGF", cn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@idfor", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters["@idfor"].Value = Session["NFormulario"];
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    this.DatosGF.DataSource = dt;
                    DatosGF.DataBind();
                    cn.Close();
                }
                using (SqlConnection cn = new SqlConnection(conexion))
                {
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("DatosRevisonDinero", cn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@idfor", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters["@idfor"].Value = Session["NFormulario"];
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    this.Dineros.DataSource = dt;
                    Dineros.DataBind();
                    cn.Close();
                }
                using (SqlConnection cn = new SqlConnection(conexion))
                {
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("DatosRevisionDBM", cn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@idfor", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters["@idfor"].Value = Session["NFormulario"];
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    this.DatosBm.DataSource = dt;
                    DatosBm.DataBind();
                    cn.Close();
                }
                using (SqlConnection cn = new SqlConnection(conexion))
                {
                    cn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("DatosRevisioVisitaDom", cn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@idfor", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters["@idfor"].Value = Session["NFormulario"];
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    this.DatosVisita.DataSource = dt;
                    DatosVisita.DataBind();
                    cn.Close();
                }
            }
        }

      
        protected void Sig_Click1(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
           string val = control.InsertarBeca(Convert.ToString(Session["NFormulario"]),Beca.Text,Observaciones.Text);
            if (val.Equals("1"))
            {
                if (Convert.ToString(Session["Devolver"]).Equals("1"))
                {
                    Session["Devolver"] = "";
                    Response.Redirect("RevisionBecas");
                }
                else
                {
                    Response.Redirect("FormularioArchivados");
                }
            }
            else
            {
                Response.Redirect("RevisiondelEstudiante");
            }
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            string id = ((sender as LinkButton).CommandArgument).ToString();
            byte[] bytes;
            string fileName, contentType;
            string constr = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select NombreAnexo, Anexos, TipodeContenido from Formularios where IDFormulario=@Id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        sdr.Read();
                        bytes = (byte[])sdr["Anexos"];
                        contentType = sdr["TipodeContenido"].ToString();
                        fileName = sdr["NombreAnexo"].ToString();
                    }
                    con.Close();
                }
            }
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
   
}
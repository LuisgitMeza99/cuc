﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RevisionBecas.aspx.cs" Inherits="CUCBecas.RevisionBecas" %>

<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,800,700,900,300,100" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
</head>
<body >
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="PrincipalTrabajoSocial.aspx">Salir</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
    <form class="form" runat="server">
          
          
              
         <img src="img/Beca 0.png" style="width:200px;"/>
         <img src="img/Beca 1.png" style="width:200px;"/>
         <img src="img/Beca 2.png" style="width:200px;"/>
         <img src="img/Beca 3.png" style="width:200px;"/>
         <img src="img/Beca 4.png" style="width:200px;"/>
        <img src="img/NoAplica.png" style="width:200px;"/>
        <div style="overflow-x: auto; ">
            <asp:GridView ID="Formulario" runat="server" UseAccessibleHeader="True" DataKeyNames="N° Formulario" BackColor="White" BorderColor="White" BorderWidth="1px" CellPadding="10" CellSpacing="5" ForeColor="White" GridLines="Horizontal" OnRowCommand="Formulario_RowCommand" OnRowDataBound="Formulario_RowDataBound" AutoGenerateColumns="False" CssClass="gridview">
                <Columns>
                    <asp:CommandField ButtonType="Button" HeaderText="Revisar"  ShowSelectButton="True"   >
                        <ControlStyle CssClass="hooola" />
                    </asp:CommandField>
                      <asp:BoundField DataField="N° Formulario" HeaderText="N° Formulario" />
                                <asp:BoundField DataField="Cédula" HeaderText="Cédula" />
                                <asp:BoundField DataField="Nombre Completo" HeaderText="Nombre Completo" />
                                <asp:BoundField DataField="Carrera" HeaderText="Carrera" />
                                <asp:BoundField DataField="Nivel" HeaderText="Nivel" />
                    <asp:BoundField DataField="Provincia" HeaderText="Provincia" />
                    <asp:BoundField DataField="Cantón" HeaderText="Cantón" />
                    <asp:BoundField DataField="Distrito" HeaderText="Distrito" />
                    <asp:BoundField DataField="Ingreso Familiar" HeaderText="Ingreso Familiar" />
                    <asp:BoundField DataField="Egreso Familiar" HeaderText="Egreso Familiar" />
                    <asp:BoundField DataField="Cantidad de miembros" HeaderText="Cantidad de miembros" />
                    <asp:BoundField DataField="Ingreso Per Cápita" HeaderText="Ingreso Per Cápita" />
                    <asp:BoundField DataField="Visita Domiciliaria" HeaderText="Visita Domiciliaria" />
                     <asp:BoundField DataField="Muebles e Inmuebles" HeaderText="Muebles e Inmuebles" />
                    <asp:BoundField DataField="Categoria de Beca" HeaderText="Categoria de Beca" />
                    <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" />
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Anexos">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDownload" runat="server" Text="Descargar Anexo" OnClick="DownloadFile"
                                CommandArgument='<%# Eval("N° Formulario") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

              

            </asp:GridView>

        </div>
    </form>

</body>
</html>


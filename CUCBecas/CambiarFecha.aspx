﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CambiarFecha.aspx.cs" Inherits="CUCBecas.CambiarFecha" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Cambiar Fecha</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body class="bodyEs">
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="PrincipalTrabajoSocial.aspx">Salir</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
    <div class="containerEs">
        <form runat="server" class="formEs">
            <div class="divEs">
                <h1 class="titulos">➤ Primer Cuatrimestre</h1>


                <label class="labelEs">
                    <br>
                   Fecha de Pago<br>
                     <asp:TextBox ID="FechaPago" runat="server" placeholder="Fecha de Pago"  required="Falta tu Nombre"  TextMode="Date" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Fecha de entrega de formulario<br>
                     <asp:TextBox ID="FechaEntrega" runat="server" placeholder="Fecha de entrega de formulario"  TextMode="Date" required="Faltan tus Apellidos" class="inputEs"></asp:TextBox>
        
                 
                </label>

                <label class="labelEs">
                    <br>
                    Fecha de resultado de becas<br>
                    <asp:TextBox ID="FechaResultado" runat="server" placeholder="Fecha de resultado de formulario"  TextMode="Date" required="Falta tu Correo" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Inicio de Apelacion de resultado<br>

                   <asp:TextBox ID="FechaApelacion" runat="server" placeholder="Fecha de Apelacion de resultado" TextMode="Date" required="Falta tu Correo" class="inputEs"></asp:TextBox>
                </label>
                <h1 class="titulos">➤ Segundo Cuatrimestre</h1>


                <label class="labelEs">
                    <br>
                   Fecha de Pago<br>
                     <asp:TextBox ID="FechaPago2" runat="server" placeholder="Fecha de Pago"  required="Falta tu Nombre"  TextMode="Date" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Fecha de entrega de formulario<br>
                     <asp:TextBox ID="FechaEntrega2" runat="server" placeholder="Fecha de entrega de formulario"  TextMode="Date" required="Faltan tus Apellidos" class="inputEs"></asp:TextBox>
        
                 
                </label>

                <label class="labelEs">
                    <br>
                    Fecha de resultado de becas<br>
                    <asp:TextBox ID="FechaResultado2" runat="server" placeholder="Fecha de resultado de formulario"  TextMode="Date" required="Falta tu Correo" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Inicio de Apelacion de resultado<br>

                   <asp:TextBox ID="FechaApelacion2" runat="server" placeholder="Fecha de Apelacion de resultado" TextMode="Date" required="Falta tu Correo" class="inputEs"></asp:TextBox>
                </label>
                   <h1 class="titulos">➤ Tercer Cuatrimestre</h1>


                <label class="labelEs">
                    <br>
                   Fecha de Pago<br>
                     <asp:TextBox ID="FechaPago3" runat="server" placeholder="Fecha de Pago"  required="Falta tu Nombre"  TextMode="Date" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Fecha de entrega de formulario<br>
                     <asp:TextBox ID="FechaEntrega3" runat="server" placeholder="Fecha de entrega de formulario"  TextMode="Date" required="Faltan tus Apellidos" class="inputEs"></asp:TextBox>
        
                 
                </label>

                <label class="labelEs">
                    <br>
                    Fecha de resultado de becas<br>
                    <asp:TextBox ID="FechaResultado3" runat="server" placeholder="Fecha de resultado de formulario"  TextMode="Date" required="Falta tu Correo" class="inputEs"></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Inicio de Apelacion de resultado<br>

                   <asp:TextBox ID="FechaApelacion3" runat="server" placeholder="Fecha de Apelacion de resultado" TextMode="Date" required="Falta tu Correo" class="inputEs"></asp:TextBox>
                </label>
                </div>
       
       <div class="centrado">
           
    <asp:Button ID="Entra" runat="server" Text="Guardar" OnClick="Entra_Click" class="buttonEs"/>
</div>
    
</form>
        </div>
    <div class="espacio">
 </div>
 
</body>
</html>
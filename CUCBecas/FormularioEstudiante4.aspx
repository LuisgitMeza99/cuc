﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularioEstudiante4.aspx.cs" Inherits="CUCBecas.FormularioEstudiante4" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body>
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>


    

    <div class="rowb">
        <div class="colb un">
            <span>
                <h2>BIENESTAR ESTUDIANTIL Y CALIDAD DE VIDA PROGRAMA DE BECAS COMPROMISO DE ESTUDIANTES BECADOS </h2>
            </span>
            <br>
             <form class="form" runat="server">
            <span>Yo  <asp:Label ID="Nom" runat="server"  ReadOnly="True"></asp:Label>  Carnet y/o N° de Cédula  <asp:Label ID="ced" runat="server"  ReadOnly="True"></asp:Label> , 
                 solicitante de beca socioeconómica para ser estudiante de la Carrera 
                  <asp:Label ID="Carrera" runat="server"  ReadOnly="True"></asp:Label> me comprometo el día 
                 <asp:Label ID="fecha" runat="server"  ReadOnly="True"></asp:Label>  a cumplir los deberes  que me indica el Reglamento de 
                Becas del Colegio Universitario de Cartago (CUC), siendo de mi obligatoriedad conocer 
                dicho reglamento, el cual se encuentra disponible en la página web www.cuc.ac.cr, 
                específicamente en el apartado llamado Normativa. 
             </span>
                   <br>
               
                        <asp:CheckBox ID="Acepto" runat="server" class="log" required=" ss" server="" Text="Acepto terminos y condiciones" />
                 <asp:FileUpload ID="Anexos" runat="server" accept=".pdf" runat="server" class="buttonEs" required=" ss" server="" /><br>
                        <asp:Button ID="Finalizar" runat="server" Text="Finalizar" class="buttonEs" OnClick="Finalizar_Click"   />
                 
                    </form>
        </div>
    </div>

    <div class="espacio">
    </div>



    <!-- COMIENZA EL FOOTER -->

    <div class="clear"></div>
    <footer>
        <div id="content_footer" class="container">

            <ul id="nav_footer">
                <a href="" class="logo_footer">
                    <img src="img/logo_cuc.png" /></a>
            </ul>

            <div class="clear"></div>

            <div class="content_info_footer ">

                <span>Colegio Universitario de Cartago</span>
                <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                <a href="">Teléfono:2550-6100</a>
                <script>
                    (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date(); a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-54698007-1', 'auto');
                    ga('send', 'pageview');

                </script>



            </div>

        </div>
    </footer>
</body>
</html>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="instruciones3.aspx.cs" Inherits="CUCBecas.instruciones3" %>


<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body>
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>


    

    <div class="rowb">
        <div class="colb un">
            <span>
                <h2>III. DATOS BIENES MUEBLES E INMUEBLES DEL GRUPO FAMILIAR</h2>
            </span>
            <br>
            <span>Debidamente apercibido de las implicaciones legales que implica el delito de perjuicio establecido en 
               el código penal, declaro bajo gravedad de juramento, que los datos e informes que brindo son fidedignos.<br>
               A su vez me comprometo con las disposiciones establecidas en el Reglamento de Becas de la Institución.Cualquier dato falso u omisión de información, anula la presente solicitud, perdiendo el 
               estudiante, la oportunidad de gozar de este beneficio, toda solicitud presentada fuera de 
               la fecha indicada o sin la documentación requerida y actualizada, es nula y por tanto se 
               desechará de oficio.<br>
                Toda situación que afecte los requisitos que el Reglamento de Becas establece deberá informar de forma escrita en el Departamento de Bienestar Estudiantil y Calidad de Vida.<span>
                    <form class="form" runat="server">
                        <span> Cedula</span>
                        <asp:Label ID="ced" runat="server" class="log" ReadOnly="True"></asp:Label> 
                         <span> Fecha</span>
                        <asp:Label ID="fecha" runat="server" class="log" ReadOnly="True"></asp:Label> 
                        
                        <asp:CheckBox ID="Acepto" runat="server"  required="Tienes que aceptar" class="log"  Text=" Acepto terminos y condiciones"/>

                         <br>
                         Numero de Propietarios de muebles e inmuebles de la Familia (Si no tiene muebles e inmuebles digite 0)
                        <br>
                        <br>
                        <asp:TextBox ID="num" runat="server" placeholder="Numero de propiedades" required="Falta la cantidad de familiares" class="log" TextMode="Number" min="0" max="5" step="1" Text="1"></asp:TextBox>
                        <asp:Button ID="Sigu" runat="server" Text="Seguir" class="buttonEs" OnClick="Sigu_Click"   />
                    </form>
        </div>
    </div>

    <div class="espacio">
    </div>



    <!-- COMIENZA EL FOOTER -->

    <div class="clear"></div>
    <footer>
        <div id="content_footer" class="container">

            <ul id="nav_footer">
                <a href="" class="logo_footer">
                    <img src="img/logo_cuc.png" /></a>
            </ul>

            <div class="clear"></div>

            <div class="content_info_footer ">

                <span>Colegio Universitario de Cartago</span>
                <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                <a href="">Teléfono:2550-6100</a>
                <script>
                    (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date(); a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-54698007-1', 'auto');
                    ga('send', 'pageview');

                </script>



            </div>

        </div>
    </footer>
</body>
</html>

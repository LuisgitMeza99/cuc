﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class RevisionBecas : System.Web.UI.Page
    {
        string conexion = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("2"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter("TablaCuatrimestre", cn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                da.Fill(dt);
                this.Formulario.DataSource = dt;
                Formulario.DataBind();
                cn.Close();
            }
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            string id = ((sender as LinkButton).CommandArgument).ToString();
            byte[] bytes;
            string fileName, contentType;
            string constr = ConfigurationManager.ConnectionStrings["conectar"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select NombreAnexo, Anexos, TipodeContenido from Formularios where IDFormulario=@Id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        sdr.Read();
                        bytes = (byte[])sdr["Anexos"];
                        contentType = sdr["TipodeContenido"].ToString();
                        fileName = sdr["NombreAnexo"].ToString();
                    }
                    con.Close();
                }
            }
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }

        protected void Formulario_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                //
                // Se obtiene indice de la row seleccionada
                //
                int index = Convert.ToInt32(e.CommandArgument);

                //
                // Obtengo el id de la entidad que se esta editando
                // en este caso de la entidad Person
                //
                Session["NFormulario"] = Formulario.DataKeys[index].Value;
                Session["Devolver"] = 1;
                Response.Write("<script>location.href='RevisiondelEstudiante.aspx';</script>");

            }
        }

        protected void Formulario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int ingresoper = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Ingreso Per Cápita"));

                if (ingresoper > 3500)
                {
                    e.Row.BackColor = System.Drawing.Color.SlateGray;
                }

                if (ingresoper <= 3500 && ingresoper >= 3101)
                {
                    e.Row.BackColor = System.Drawing.Color.OrangeRed;
                }

                if (ingresoper <= 3100 && ingresoper >= 2601)
                {
                    e.Row.BackColor = System.Drawing.Color.Orange;
                }

                if (ingresoper <= 2600 && ingresoper >= 2101)
                {
                    e.Row.BackColor = System.Drawing.Color.Goldenrod;
                }
                if (ingresoper <= 2100 && ingresoper >= 1801)
                {
                    e.Row.BackColor = System.Drawing.Color.MediumSeaGreen;
                }

                if (ingresoper <= 1800)
                {
                    e.Row.BackColor = System.Drawing.Color.ForestGreen;
                }


            }
        }

        
    }
}
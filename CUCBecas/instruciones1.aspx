﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="instruciones1.aspx.cs" Inherits="CUCBecas.instruciones1" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title></title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
    <body>
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"   class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="Contact.aspx">Contactenos</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
        <div class="espacio">
 </div>
        
    

   
  <div class="rowb">
      <div class="colb un">
      <span><h2>CONSIDERACIONES IMPORTANTES ACERCA DEL TRÁMITE DE SOLICITUD DE BECA :</h2></span><br>
         <span>  El fin de este formulario de solicitud de beca es para obtener información requerida por parte del Colegio 
Universitario de Cartago (CUC) con una visión, lo más completa posible, sobre la situación socioeconómica(condiciones personales, académicas, económicas, familiares) del estudiante interesado en obtener el beneficio 
de beca de nuestra Institución. <br>
El estudiante está en la obligación de contestar todas las preguntas en forma clara, completa y veraz, deberá 
aportar toda la documentación que se le solicita, además debe indicar cualquier otro dato que considere 
necesario para valorar su situación.<br>La información suministrada es confidencial y por lo tanto es de uso exclusivo de la Oficina de Trabajo Social 
para los propósitos que la Institución se ha propuesto. Toda información facilitada en el formulario podrá ser 
verificada por parte del CUC, a través de visitas domiciliarias, entrevistas, coordinación interinstitucional e 
información de la comunidad o cualquier otro medio pertinente.Es deber de todo solicitante conocer el reglamento el Reglamento de Becas, el cual se encuentra disponible en 
la página web www.cuc.ac.cr.</span>  
         
                <form class="form" runat="server">
                    <asp:Button ID="Sig" runat="server" Text="Seguir"  class="buttonEs" OnClick="Sig_Click" />
                    </form>
               
    </div>
    </div>
          

         <div class="espacio">
 </div>

    

        <!-- COMIENZA EL FOOTER -->  
        
        <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54698007-1', 'auto');
  ga('send', 'pageview');

</script>

                        

                    </div>

                </div>
          </footer>
</body>
</html>
         

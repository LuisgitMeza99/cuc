﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class CambiarPerfilPersonalContra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("2") && !seccion.Equals("3") && !seccion.Equals("4"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            string contra = password.Text;
            if (contra.Equals(password2.Text))
            {
                if (contra.Length >= 5)
                {
                    string seccion = (string)Session["sesion"];
                   
                    Controlador control = new Controlador();
                    string valido = "";
                    if (seccion.Equals("4")) {
                        string id = (string)Session["id"];
                        valido = control.actualizarPersonalContra(password.Text, (string)Session["id"]);
                    }
                    else
                    {
                        string id = (string)Session["IDusuario"];
                        valido = control.actualizarPersonalContra(password.Text, id);
                    }
                   
                    if (valido.Equals("1"))
                    {
                       
                        if (seccion.Equals("2"))
                        {
                            Response.Redirect("PrincipalTrabajoSocial");
                        }
                        if (seccion.Equals("3"))
                        {
                            Response.Redirect("PrincipalFinanciero");
                        }
                        if (seccion.Equals("4"))
                        {
                            Response.Redirect("PrincipalAdmin");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Hubo un error por favor informar')</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Las Contraseña tiene que tener mas de 5 caracteres')</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Las Contraseña no coincide')</script>");
            }
        }
    }
}
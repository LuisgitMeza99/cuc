﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class FormularioEstudiante4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            string cod = (string)Session["cod"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1") || !cod.Equals("true"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
            Controlador control = new Controlador();
            string carr = control.TraerCarrera();
            Nom.Text = (string)Session["Nombre"];
            ced.Text = id;
            Carrera.Text = carr;
            DateTime fech = DateTime.Now;
            fecha.Text = fech.ToShortDateString();

        }

        protected void Finalizar_Click(object sender, EventArgs e)
        {
            if (Acepto.Checked && (Anexos.PostedFile != null) && (Anexos.PostedFile.ContentLength > 0))
            {
                string filename = (string)Session["IDForm"] +" "+(string)Session["Nombre"];
                string contentType = Anexos.PostedFile.ContentType;
                using (Stream fs = Anexos.PostedFile.InputStream)
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        byte[] bytes = br.ReadBytes((Int32)fs.Length); Controlador control = new Controlador();
                        control.AceptoCompromiso(bytes, filename, contentType);
                        Response.Redirect("Pagina_Estudiante");
                    }
                }
            }
        }
    }
}
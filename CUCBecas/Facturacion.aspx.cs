﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class Facturacion : System.Web.UI.Page
    {
     
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("3") )
                {
                    Response.Redirect("IniciarSesion");
                }
                
            }
            costo.Text = (string)Session["Coste"];
            

        }

       

        protected void Sig_Click(object sender, EventArgs e)
        {
            
            string fact = Nombre.Text + " " + Apellidos.Text;
            string correo = Correo.Text;
            Controlador control = new Controlador();
            string paso = control.insertFact();
            control.CorreoFactura(fact, correo, paso);
           
            Response.Redirect("Facturacion");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string cedula = Cédula.Text; Controlador control = new Controlador(); 
            string[] datos = control.TraerPerfil(cedula);
            Nombre.Text = datos[0]; Apellidos.Text = datos[1];
            Correo.Text = datos[2];
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Perfiladmin.aspx.cs" Inherits="CUCBecas.Perfiladmin" %>



<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Perfil</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    
   

</head>
    <body class="overlay">
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"  class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
       <a href="PrincipalAdmin.aspx">Salir</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
  
          
   <form id="form1" runat="server"  class="registro">
      
    
         <asp:TextBox ID="Nombre" runat="server" placeholder="Nombre"  required="Faltan tus Apellidos" class="select"></asp:TextBox>
        <asp:TextBox ID="Correo" runat="server" placeholder="Correo" TextMode="Email" required="Falta tu Correo" class="select"></asp:TextBox>
   
  <asp:Button ID="Entrar2" runat="server" Text="Cambiar Contraseña" OnClick="Entrar2_Click" />
     <asp:Button ID="Entrar" runat="server" Text="Actualizar Informacion" OnClick="Entrar_Click" />
</form>
       
     
    
     <script type="text/javascript">
         function numbersonly(e) {
             var unicode = e.charCode ? e.charCode : e.keyCode
             if (unicode != 8 && unicode != 44) {
                 if (unicode < 48 || unicode > 57) //if not a number
                 { return false } //disable key press    
             }
         }  
</SCRIPT>      
</body>
</html>
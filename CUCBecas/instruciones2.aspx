﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="instruciones2.aspx.cs" Inherits="CUCBecas.Intruciones2" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
    <body>
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"   class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="Contact.aspx">Contactenos</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
        <div class="espacio">
 </div>
        
    
<div class="rowb">
    <div class="colb for">
      <span>Es muy importante conocer si el solicitante ha tenido o tiene en su contexto familiar situaciones: Orfandad, Separación padres, Divorcio, Maltrato Físico/Verbal, ideación o suicidios, Falta de apoyo, Alcoholismo, Drogadicción, Malas Relaciones u Otro (s) puede exponerlo en una carta o/y de forma presencial en Trabajo Social.</span>
    </div>
    </div>
   
  <div class="rowb">
      <div class="colb un">
      <span><h2>II DATOS DEL GRUPO FAMILIAR - SITUACIÓN ECONÓMICA - SALUD DE EL/LA ESTUDIANTE SOLICITANTE</h2></span><br>
         <span style="text-align:left"> Llene el siguiente formulario con los datos de cada uno de los miembros del grupo familiar (número total de miembros que viven con el/la estudiante solicitante en su hogar). Debe iniciar con los 
                datos de el/la estudiante solicitante. DEBE ANOTAR A TODOS.<br> <br> Se aclara lo siguiente :<br> 
             <br> Parentesco : se refiere al grado de consanguineidad o afinidad con el/la estudiante solicitante a la beca. Ej.: solicitante, madre, padre, hermano(a), primo(a), tío(a), sobrino(a), entre otros.
             <br> Salud : Es importante saber si es un diagnóstico de salud, mental, física, Enfermedad Crónica, Invalidez Física, Alcoholismo, Uso de Drogas, Mala Nutrición u otro(s).
             <br> Ingresos mensuales : monto total que recibe cada miembro de la familia por concepto de ingresos por cuenta propia, salarios, beca, pensiones, alquileres, entre otros. Salario sin deducciones bruto</span> 
                 <form class="form" runat="server">
                     Número de miembros del grupo familiar (Incluyendose máximo 10) <br>
                     <br>
                      <asp:TextBox ID="Fam" runat="server" placeholder="Numero de miembros" required="Falta la cantidad de familiares" class="log" TextMode="Number"  min="1" max="10" step="1" Text="1"></asp:TextBox>
                    <asp:Button ID="Sig" runat="server" Text="Seguir"  class="buttonEs" OnClick="Sig_Click"/>
                    </form>
    </div>
    </div>
 
         <div class="espacio">
 </div>

    

        <!-- COMIENZA EL FOOTER -->  
        
        <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54698007-1', 'auto');
  ga('send', 'pageview');

</script>

                        

                    </div>

                </div>
          </footer>
</body>
</html>
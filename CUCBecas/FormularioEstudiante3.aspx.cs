﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class FormularioEstudiante3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            string cod = (string)Session["cod"];

            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1") || !cod.Equals("true"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
            if (IsPostBack || !IsPostBack)
            {
                string miembros = (string)Session["miembros"];
                if (miembros.Equals("0"))
                {
                    Response.Redirect("FormularioEstudiante4");
                }
                    if (miembros.Equals("1"))
                {
                    titulo2.Visible = false;
                    n2.Visible = false;
                    c2.Visible = false;
                    tl2.Visible = false;
                    cl2.Visible = false;
                    usf2.Visible = false;
                    t2.Visible = false;
                    cf2.Visible = false;
                    usl2.Visible = false;
                    ch2.Visible = false;
                    ca2.Visible = false;
                    aparta2.Visible = false;
                    v2.Visible = false;
                    dv2.Visible = false;

                    titulo3.Visible = false;
                    n3.Visible = false;
                    c3.Visible = false;
                    tl3.Visible = false;
                    cl3.Visible = false;
                    usl3.Visible = false;
                    usf3.Visible = false;
                    t3.Visible = false;
                    cf3.Visible = false;
                    usf3.Visible = false;
                    ch3.Visible = false;
                    ca3.Visible = false;
                    aparta3.Visible = false;
                    v3.Visible = false;
                    dv3.Visible = false;

                    titulo4.Visible = false;
                    n4.Visible = false;
                    c4.Visible = false;
                    tl4.Visible = false;
                    cl4.Visible = false;
                    usl4.Visible = false;
                    usf4.Visible = false;
                    t4.Visible = false;
                    cf4.Visible = false; 
                    ch4.Visible = false;
                    ca4.Visible = false;
                    aparta4.Visible = false;
                    v4.Visible = false;
                    dv4.Visible = false;

                    titulo5.Visible = false;
                    n5.Visible = false;
                    c5.Visible = false;
                    tl5.Visible = false;
                    cl5.Visible = false;
                    usl5.Visible = false;
                    t5.Visible = false;
                    cf5.Visible = false;
                    usf5.Visible = false;
                    ch5.Visible = false;
                    ca5.Visible = false;
                    aparta5.Visible = false;
                    v5.Visible = false;
                    dv5.Visible = false;
                }
                if (miembros.Equals("2"))
                {

                    titulo3.Visible = false;
                    n3.Visible = false;
                    c3.Visible = false;
                    tl3.Visible = false;
                    cl3.Visible = false;
                    usl3.Visible = false;
                    usf3.Visible = false;
                    t3.Visible = false;
                    cf3.Visible = false;
                    usf3.Visible = false;
                    ch3.Visible = false;
                    ca3.Visible = false;
                    aparta3.Visible = false;
                    v3.Visible = false;
                    dv3.Visible = false;

                    titulo4.Visible = false;
                    n4.Visible = false;
                    c4.Visible = false;
                    tl4.Visible = false;
                    cl4.Visible = false;
                    usl4.Visible = false;
                    usf4.Visible = false;
                    t4.Visible = false;
                    cf4.Visible = false;
                    ch4.Visible = false;
                    ca4.Visible = false;
                    aparta4.Visible = false;
                    v4.Visible = false;
                    dv4.Visible = false;

                    titulo5.Visible = false;
                    n5.Visible = false;
                    c5.Visible = false;
                    tl5.Visible = false;
                    cl5.Visible = false;
                    t5.Visible = false;
                    cf5.Visible = false;
                    usl5.Visible = false;
                    usf5.Visible = false;
                    ch5.Visible = false;
                    ca5.Visible = false;
                    aparta5.Visible = false;
                    v5.Visible = false;
                    dv5.Visible = false;
                }
                if (miembros.Equals("3"))
                {
                    titulo4.Visible = false;
                    n4.Visible = false;
                    c4.Visible = false;
                    tl4.Visible = false;
                    cl4.Visible = false;
                    usl4.Visible = false;
                    usf4.Visible = false;
                    t4.Visible = false;
                    cf4.Visible = false;
                    ch4.Visible = false;
                    ca4.Visible = false;
                    aparta4.Visible = false;
                    v4.Visible = false;
                    dv4.Visible = false;

                    titulo5.Visible = false;
                    n5.Visible = false;
                    c5.Visible = false;
                    tl5.Visible = false;
                    cl5.Visible = false;
                    t5.Visible = false;
                    usl5.Visible = false;
                    cf5.Visible = false;
                    usf5.Visible = false;
                    ch5.Visible = false;
                    ca5.Visible = false;
                    aparta5.Visible = false;
                    v5.Visible = false;
                    dv5.Visible = false;
                }
                if (miembros.Equals("4"))
                {

                    titulo5.Visible = false;
                    n5.Visible = false;
                    c5.Visible = false;
                    tl5.Visible = false;
                    cl5.Visible = false;
                    usl5.Visible = false;
                    t5.Visible = false;
                    cf5.Visible = false;
                    usf5.Visible = false;
                    ch5.Visible = false;
                    ca5.Visible = false;
                    aparta5.Visible = false;
                    v5.Visible = false;
                    dv5.Visible = false;
                }
            }

        }

        protected void Sig_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            List<ListaFormulario3> Propietarios = new List<ListaFormulario3>();
            string prop = (string)Session["miembros"];
            if (prop.Equals("1"))
            {
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep.Text,
                    Cedula = Cedula.Text,
                    Cuantoslotes = CuantosL.Text,
                    Usoslotes = UsosL.Text,
                    CuantosFincas =CuantosP.Text,
                    UsosFincas =UsosP.Text,
                    CasaH = CuantosCH.Text,
                    CasaA = CuantosCA.Text,
                    Apartamentos = CuantosAP.Text,
                    Vehiculos = Vehiculo.Text,
                    Descripcion = Descripcion.Text
                }) ;
            }
            if (prop.Equals("2"))
            {
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep.Text,
                    Cedula = Cedula.Text,
                    Cuantoslotes = CuantosL.Text,
                    Usoslotes = UsosL.Text,
                    CuantosFincas = CuantosP.Text,
                    UsosFincas = UsosP.Text,
                    CasaH = CuantosCH.Text,
                    CasaA = CuantosCA.Text,
                    Apartamentos = CuantosAP.Text,
                    Vehiculos = Vehiculo.Text,
                    Descripcion = Descripcion.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep2.Text,
                    Cedula = Cedula2.Text,
                    Cuantoslotes = CuantosL2.Text,
                    Usoslotes = UsosL2.Text,
                    CuantosFincas = CuantosP2.Text,
                    UsosFincas = UsosP2.Text,
                    CasaH = CuantosCH2.Text,
                    CasaA = CuantosCA2.Text,
                    Apartamentos = CuantosAP2.Text,
                    Vehiculos = Vehiculo2.Text,
                    Descripcion = Descripcion2.Text

                });
            }
            if (prop.Equals("3"))
            {
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep.Text,
                    Cedula = Cedula.Text,
                    Cuantoslotes = CuantosL.Text,
                    Usoslotes = UsosL.Text,
                    CuantosFincas = CuantosP.Text,
                    UsosFincas = UsosP.Text,
                    CasaH = CuantosCH.Text,
                    CasaA = CuantosCA.Text,
                    Apartamentos = CuantosAP.Text,
                    Vehiculos = Vehiculo.Text,
                    Descripcion = Descripcion.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep2.Text,
                    Cedula = Cedula2.Text,
                    Cuantoslotes = CuantosL2.Text,
                    Usoslotes = UsosL2.Text,
                    CuantosFincas = CuantosP2.Text,
                    UsosFincas = UsosP2.Text,
                    CasaH = CuantosCH2.Text,
                    CasaA = CuantosCA2.Text,
                    Apartamentos = CuantosAP2.Text,
                    Vehiculos = Vehiculo2.Text,
                    Descripcion = Descripcion2.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep3.Text,
                    Cedula = Cedula3.Text,
                    Cuantoslotes = CuantosL3.Text,
                    Usoslotes = UsosL3.Text,
                    CuantosFincas = CuantosP3.Text,
                    UsosFincas = UsosP3.Text,
                    CasaH = CuantosCH3.Text,
                    CasaA = CuantosCA3.Text,
                    Apartamentos = CuantosAP3.Text,
                    Vehiculos = Vehiculo3.Text,
                    Descripcion = Descripcion3.Text

                });
            }
            if (prop.Equals("4"))
            {
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep.Text,
                    Cedula = Cedula.Text,
                    Cuantoslotes = CuantosL.Text,
                    Usoslotes = UsosL.Text,
                    CuantosFincas = CuantosP.Text,
                    UsosFincas = UsosP.Text,
                    CasaH = CuantosCH.Text,
                    CasaA = CuantosCA.Text,
                    Apartamentos = CuantosAP.Text,
                    Vehiculos = Vehiculo.Text,
                    Descripcion = Descripcion.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep2.Text,
                    Cedula = Cedula2.Text,
                    Cuantoslotes = CuantosL2.Text,
                    Usoslotes = UsosL2.Text,
                    CuantosFincas = CuantosP2.Text,
                    UsosFincas = UsosP2.Text,
                    CasaH = CuantosCH2.Text,
                    CasaA = CuantosCA2.Text,
                    Apartamentos = CuantosAP2.Text,
                    Vehiculos = Vehiculo2.Text,
                    Descripcion = Descripcion2.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep3.Text,
                    Cedula = Cedula3.Text,
                    Cuantoslotes = CuantosL3.Text,
                    Usoslotes = UsosL3.Text,
                    CuantosFincas = CuantosP3.Text,
                    UsosFincas = UsosP3.Text,
                    CasaH = CuantosCH3.Text,
                    CasaA = CuantosCA3.Text,
                    Apartamentos = CuantosAP3.Text,
                    Vehiculos = Vehiculo3.Text,
                    Descripcion = Descripcion3.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep4.Text,
                    Cedula = Cedula4.Text,
                    Cuantoslotes = CuantosL4.Text,
                    Usoslotes = UsosL4.Text,
                    CuantosFincas = CuantosP4.Text,
                    UsosFincas = UsosP4.Text,
                    CasaH = CuantosCH4.Text,
                    CasaA = CuantosCA4.Text,
                    Apartamentos = CuantosAP4.Text,
                    Vehiculos = Vehiculo4.Text,
                    Descripcion = Descripcion4.Text

                });
            }
            if (prop.Equals("5"))
            {
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep.Text,
                    Cedula = Cedula.Text,
                    Cuantoslotes = CuantosL.Text,
                    Usoslotes = UsosL.Text,
                    CuantosFincas = CuantosP.Text,
                    UsosFincas = UsosP.Text,
                    CasaH = CuantosCH.Text,
                    CasaA = CuantosCA.Text,
                    Apartamentos = CuantosAP.Text,
                    Vehiculos = Vehiculo.Text,
                    Descripcion = Descripcion.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep2.Text,
                    Cedula = Cedula2.Text,
                    Cuantoslotes = CuantosL2.Text,
                    Usoslotes = UsosL2.Text,
                    CuantosFincas = CuantosP2.Text,
                    UsosFincas = UsosP2.Text,
                    CasaH = CuantosCH2.Text,
                    CasaA = CuantosCA2.Text,
                    Apartamentos = CuantosAP2.Text,
                    Vehiculos = Vehiculo2.Text,
                    Descripcion = Descripcion2.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep3.Text,
                    Cedula = Cedula3.Text,
                    Cuantoslotes = CuantosL3.Text,
                    Usoslotes = UsosL3.Text,
                    CuantosFincas = CuantosP3.Text,
                    UsosFincas = UsosP3.Text,
                    CasaH = CuantosCH3.Text,
                    CasaA = CuantosCA3.Text,
                    Apartamentos = CuantosAP3.Text,
                    Vehiculos = Vehiculo3.Text,
                    Descripcion = Descripcion3.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep4.Text,
                    Cedula = Cedula4.Text,
                    Cuantoslotes = CuantosL4.Text,
                    Usoslotes = UsosL4.Text,
                    CuantosFincas = CuantosP4.Text,
                    UsosFincas = UsosP4.Text,
                    CasaH = CuantosCH4.Text,
                    CasaA = CuantosCA4.Text,
                    Apartamentos = CuantosAP4.Text,
                    Vehiculos = Vehiculo4.Text,
                    Descripcion = Descripcion4.Text

                });
                Propietarios.Add(new ListaFormulario3
                {
                    Nombre = Nombrep5.Text,
                    Cedula = Cedula5.Text,
                    Cuantoslotes = CuantosL5.Text,
                    Usoslotes = UsosL5.Text,
                    CuantosFincas = CuantosP5.Text,
                    UsosFincas = UsosP5.Text,
                    CasaH = CuantosCH5.Text,
                    CasaA = CuantosCA5.Text,
                    Apartamentos = CuantosAP5.Text,
                    Vehiculos = Vehiculo5.Text,
                    Descripcion = Descripcion5.Text

                });
            }
            foreach (ListaFormulario3 propietario in Propietarios)
            {
                control.Insertpropietario(propietario.Nombre, propietario.Cedula, propietario.Cuantoslotes, propietario.Usoslotes, propietario.CuantosFincas, propietario.UsosFincas, propietario.CasaH, propietario.CasaA, propietario.Apartamentos, propietario.Vehiculos, propietario.Descripcion);
            }
            Response.Redirect("FormularioEstudiante4");

        }
    }
}
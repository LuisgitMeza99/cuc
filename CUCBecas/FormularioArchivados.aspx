﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularioArchivados.aspx.cs" Inherits="CUCBecas.FormularioArchivados" %>

<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,800,700,900,300,100" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
</head>
<body >
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="PrincipalTrabajoSocial.aspx">Salir</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
    <div class="containerEs">
    <form class="formEs" runat="server" >
         
        
              
        <div class="divEs">
            <label class="labelEs">
                        Cuatrimestre<br>
                       <asp:DropDownList ID="Cuatri" runat="server"   class="inputEs"  AutoPostBack="True">
               <asp:ListItem Value="">Seleccione Cuatrimestre</asp:ListItem>
                            <asp:ListItem Value="1">Cuatrimestre 1</asp:ListItem>
                            <asp:ListItem Value="2">Cuatrimestre 2</asp:ListItem>
                            <asp:ListItem Value="3">Cuatrimestre 3</asp:ListItem>
                           </asp:DropDownList>
                    </label>
              
                    <label class="labelEs">
                        Año<br>
                        <asp:DropDownList ID="Ano" runat="server"  class="inputEs"  AutoPostBack="True" ></asp:DropDownList> 
                         </label>
            <label class="labelEs">
                        Cédula<br>
                        <asp:TextBox ID="Cedula" runat="server" placeholder="Cédula" class="inputEs" onkeypress="return numbersonly(event);" AutoPostBack="true"></asp:TextBox>
                         </label>
            </div>
         
        <img src="img/Beca 0.png" style="width:200px;"/>
         <img src="img/Beca 1.png" style="width:200px;"/>
         <img src="img/Beca 2.png" style="width:200px;"/>
         <img src="img/Beca 3.png" style="width:200px;"/>
         <img src="img/Beca 4.png" style="width:200px;"/>
        <img src="img/NoAplica.png" style="width:200px;"/>
        <div style="overflow-x: auto; ">
            <asp:GridView ID="Formulario" runat="server" UseAccessibleHeader="True" DataKeyNames="N° Formulario" BackColor="White" BorderColor="White" BorderWidth="1px" CellPadding="10" CellSpacing="5" ForeColor="White" GridLines="Horizontal" OnRowCommand="Formulario_RowCommand" OnRowDataBound="Formulario_RowDataBound" AutoGenerateColumns="False" CssClass="gridview">
                <Columns>
                    <asp:CommandField ButtonType="Button" HeaderText="Revisar"  ShowSelectButton="True"   >
                        <ControlStyle CssClass="hooola" />
                    </asp:CommandField>
                      <asp:BoundField DataField="N° Formulario" HeaderText="N° Formulario" />
                                <asp:BoundField DataField="Cédula" HeaderText="Cédula" />
                                <asp:BoundField DataField="Nombre Completo" HeaderText="Nombre Completo" />
                                <asp:BoundField DataField="Carrera" HeaderText="Carrera" />
                                <asp:BoundField DataField="Nivel" HeaderText="Nivel" />
                    <asp:BoundField DataField="Provincia" HeaderText="Provincia" />
                    <asp:BoundField DataField="Cantón" HeaderText="Cantón" />
                    <asp:BoundField DataField="Distrito" HeaderText="Distrito" />
                    <asp:BoundField DataField="Ingreso Familiar" HeaderText="Ingreso Familiar" />
                    <asp:BoundField DataField="Egreso Familiar" HeaderText="Egreso Familiar" />
                    <asp:BoundField DataField="Cantidad de miembros" HeaderText="Cantidad de miembros" />
                    <asp:BoundField DataField="Ingreso Per Cápita" HeaderText="Ingreso Per Cápita" />
                    <asp:BoundField DataField="Visita Domiciliaria" HeaderText="Visita Domiciliaria" />
                     <asp:BoundField DataField="Muebles e Inmuebles" HeaderText="Muebles e Inmuebles" />
                    <asp:BoundField DataField="Categoria de Beca" HeaderText="Categoria de Beca" />
                    <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" />
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Anexos">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDownload" runat="server" Text="Descargar Anexo" OnClick="DownloadFile"
                                CommandArgument='<%# Eval("N° Formulario") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        </form>
    </div>
    <script type="text/javascript">
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 44) {
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }
    </script>
</body>
</html>

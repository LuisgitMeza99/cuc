﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class Codigo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            password.Visible = false;
            password2.Visible = false;
            Entrar2.Visible = false;
            string id = (string)Session["IDusuario"];
            string Correo = (string)Session["email"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(Correo))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            string id = (string)Session["IDusuario"];
            string token = Codigot.Text;
            Controlador control = new Controlador();
            Boolean paso = control.Validartoken(id, token);

           if(paso == true)
            {
                Codigot.Visible = false;
                Entrar.Visible = false;
                password.Visible = true;
                password2.Visible = true;
                Entrar2.Visible = true;
            }
            Session["token"] = Codigot.Text;
        }
        protected void Entrar2_Click(object sender, EventArgs e)
        {
            string contra = password.Text;
            if (contra.Equals(password2.Text))
            {
                if (contra.Length >= 5)
                {
                    Controlador control = new Controlador();
                   Boolean paso = control.cambiarContra(contra);
                    if (paso == true)
                    {
                        Response.Redirect("IniciarSesion");
                    }
                    else
                    {
                        Response.Write("<script>alert('Hubo un error al cambiar la contraseña')</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Las Contraseña tiene que tener mas de 5 caracteres')</script>");
                }
                }
                else
                {
                    Response.Write("<script>alert('Las Contraseña no coincide')</script>");
                }

            }
    }
}
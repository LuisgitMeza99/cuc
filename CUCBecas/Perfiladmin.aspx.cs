﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class Perfiladmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("4"))
                {
                    Response.Redirect("IniciarSesion");
                }

              
                Nombre.Text = (string)Session["Nombre"] ;
                Correo.Text= (string)Session["Email"];
            }
        }

        protected void Entrar2_Click(object sender, EventArgs e)
        {
            Response.Redirect("CambiarPerfilPersonalContra");
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            string nombre = Nombre.Text;
            string correo = Correo.Text;
            string valido =  control.Updateperfiladmin(nombre,correo);
           
            if (valido.Equals("1"))
            {
                   Response.Redirect("PrincipalAdmin");
   
            }
            else
            {
                Response.Write("<script>alert('Hubo un error por favor informar')</script>");
            }
        
    }

    
    }
}
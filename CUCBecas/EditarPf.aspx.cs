﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class EditarPf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("3"))
                {
                    Response.Redirect("IniciarSesion");
                }
                Controlador control = new Controlador();
                string[] val = control.selectpFin();
                Coste.Text = val[0];
                Horario.Text = val[1];
            }
          
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            control.EditarPfin(Coste.Text, Horario.Text);
            Response.Redirect("PrincipalFinanciero");
        }
    }
}
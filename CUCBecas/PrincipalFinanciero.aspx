﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrincipalFinanciero.aspx.cs" Inherits="CUCBecas.PrincipalFinanciero" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Pantalla Principal</title>
    <meta charset="utf-8">
 
    <script src="js/main.js"></script>
    <link href="css/estilos.css" rel="stylesheet" />
 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<style class="cp-pen-styles">
}</style><style type="text/css">{ display: none !important; }</style>
</head>
    <body >
         <header>
   <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"  class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="Contact.aspx">Contactenos</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
<div class="espacio">
 </div>
        
    
<div class="container" id='iconos'>
  <h2 style="color:#026466;">
    <asp:Label ID="Nom" runat="server" Text=""></asp:Label>
  </h2>
    <a href="Facturacion.aspx" >
  <div class="row">
      <div class="col orange">
      <i class="fa fa-book"></i>
      <span>Ingresar facturas</span>
    </div>
    </div>
</a>
   
     <a href="CambiarPerfilPersonal" >
    <div class="row">
    <div class="col blue">
      <i class="fa fa-user"></i>
      <span>Perfil</span>
    </div>
    </div>
</a>
     <a href="EditarPf" >
    <div class="row">
    <div class="col blue2">
      <i class="fa fa-user"></i>
      <span>Editar Parametros</span>
    </div>
    </div>
</a>

     <a href="Default" >
    <div class="row">
     <div class="col green2">
      <i class="fa fa-sign-out"></i>
      <span>Salir</span>
    </div>
  </div>
    </a>
  
</div>
<div class="espaciob">
 </div>
 
      
     
</body>
</html>
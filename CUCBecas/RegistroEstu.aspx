﻿<%@ Page Title="About" Language="C#"  AutoEventWireup="true" CodeBehind="RegistroEstu.aspx.cs" Inherits="CUCBecas.About" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Registro</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    
   

</head>
    <body class="overlay">
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"  class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="IniciarSesion.aspx">Iniciar Sesión</a>
          <a href="Contact.aspx">Contactenos</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
  
          
   <form id="form1" runat="server"  class="registro">
       <label class="labelre">Registro</label>
      <asp:TextBox ID="Cedula" runat="server" placeholder="Cédula o Dimex"  required="Falta tu cédula o dimex" class="select" onkeypress="return numbersonly(event);" MinLength="9" MaxLength="12" ></asp:TextBox>
       <asp:TextBox ID="Nombre" runat="server" placeholder="Nombre"  required="Falta tu Nombre" class="select"></asp:TextBox>
         <asp:TextBox ID="Apellidos" runat="server" placeholder="Apellidos"  required="Faltan tus Apellidos" class="select"></asp:TextBox>
        <asp:TextBox ID="Correo" runat="server" placeholder="Correo" TextMode="Email" required="Falta tu Correo" class="select"></asp:TextBox>

       <asp:DropDownList ID="Sexo" runat="server" AppendDataBoundItems="true" required="Falta tu sexo" class="select">
       <asp:ListItem  Value="">Seleccione su Sexo</asp:ListItem>
       <asp:ListItem Value="Masculino">Masculino</asp:ListItem>
       <asp:ListItem Value="Femenino">Femenino</asp:ListItem>
       </asp:DropDownList>
   <asp:DropDownList ID="Carrera" runat="server" AppendDataBoundItems="true" required="Falta tu Carrera" class="select">
       <asp:ListItem  Value="" >Carrera</asp:ListItem>
    <asp:ListItem Value="Electrónica">Electrónica</asp:ListItem>
   <asp:ListItem Value="Secretariado Ejecutivo">Secretariado Ejecutivo</asp:ListItem>
    <asp:ListItem Value="Tecnologías de Información">Tecnologías de Información</asp:ListItem>
    <asp:ListItem Value="Mecánica Dental">Mecánica Dental</asp:ListItem>
    <asp:ListItem Value="Investigación Criminal">Investigación Criminal</asp:ListItem>
   <asp:ListItem Value="Turismo">Turismo</asp:ListItem>
    <asp:ListItem Value="Dirección y Administración de Empresas">Dirección y Administración de Empresas</asp:ListItem>
 </asp:DropDownList>

  <label class="label"">Fecha de Nacimiento</label>
  <asp:TextBox ID="Nacimiento" runat="server" placeholder="Fecha Nacimiento" TextMode="Date" required="Falta tu fecha de nacimiento" class="select"></asp:TextBox>
    <asp:TextBox ID="password" runat="server" placeholder="Contraseña" TextMode="Password" required="Falta tu  Contraseña" class="select"></asp:TextBox>
        <asp:TextBox ID="password2" runat="server" placeholder="Repita la Contraseña" TextMode="Password" required="Falta tu Contraseña" class="select"></asp:TextBox> 
     <asp:Button ID="Entrar" runat="server" Text="Registrarse"  OnClick="Entrar_Click" />
</form>
       
      <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>


                        

                    </div>

                </div>
          </footer>
     <script type="text/javascript">
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode
        if (unicode != 8 && unicode != 44) {
            if (unicode < 48 || unicode > 57) //if not a number
            { return false } //disable key press    
        }
    }  
</SCRIPT>      
</body>
</html>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RevisiondelEstudiante.aspx.cs" Inherits="CUCBecas.RevisiondelEstudiante" %>

<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,800,700,900,300,100" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
</head>
<body>
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                    <a href="PrincipalTrabajoSocial.aspx">Salir</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
   <div class="containerEs">
    <form class="formEs" runat="server">
        <div style="overflow-x: auto; width=150%">
             <h1 class="titulos">➤Datos estudiante </h1>
            <asp:GridView ID="Estudiante" runat="server"  DataKeyNames="N° Formulario" UseAccessibleHeader="True" BackColor="White" BorderColor="White" BorderWidth="1px" CellPadding="10" CellSpacing="5" ForeColor="White" GridLines="Horizontal"  AutoGenerateColumns="False" CssClass="gridview">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Anexos">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDownload" runat="server" Text="Descargar Anexo" OnClick="DownloadFile"
                                CommandArgument='<%# Eval("N° Formulario") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Cédula" HeaderText="Cédula" />
                    <asp:BoundField DataField="Nombre Completo" HeaderText="Nombre Completo" />
                    <asp:BoundField DataField="Teléfono Celular" HeaderText="Teléfono Celular" />
                    <asp:BoundField DataField="Teléfono Fijo" HeaderText="Teléfono Fijo" />
                    <asp:BoundField DataField="Correo" HeaderText="Correo" />
                    <asp:BoundField DataField="Condicion Solicitante" HeaderText="Condicion Solicitante" />
                    <asp:BoundField DataField="Provincia" HeaderText="Provincia" />
                    <asp:BoundField DataField="Canton" HeaderText="Canton" />
                  <asp:BoundField DataField="Distrito" HeaderText="Distrito" />
                    <asp:BoundField DataField="Direccion Exacta" HeaderText="Direccion Exacta" />
                </Columns>



            </asp:GridView>

        </div>
         
          <div style="overflow-x: auto; width=150%">
              <h1 class="titulos">➤Datos Grupo Familiar </h1>
            <asp:GridView ID="DatosGF" runat="server" UseAccessibleHeader="True" BackColor="White" BorderColor="White" BorderWidth="1px" CellPadding="10" CellSpacing="5" ForeColor="White" GridLines="Horizontal"   CssClass="gridview">
               



            </asp:GridView>

        </div>
         <div style="overflow-x: auto; width=150%">
              <h1 class="titulos">➤Otros ingresos del grupo familiar </h1>
            <asp:GridView ID="Dineros" runat="server" UseAccessibleHeader="True" BackColor="White" BorderColor="White" BorderWidth="1px" CellPadding="10" CellSpacing="5" ForeColor="White" GridLines="Horizontal"   CssClass="gridview">
               



            </asp:GridView>

        </div>
          <div style="overflow-x: auto; width=150%">
                       <h1 class="titulos">➤Datos Bienes Muebles e Inmuebles </h1>
            <asp:GridView ID="DatosBm" runat="server" UseAccessibleHeader="True" BackColor="White" BorderColor="White" BorderWidth="1px" CellPadding="10" CellSpacing="5" ForeColor="White" GridLines="Horizontal"  CssClass="gridview">
             
            </asp:GridView>
        </div>
         
          <div style="overflow-x: auto; width=150%">
              <h1 class="titulos">➤Datos Valoración Domiciliaria</h1>
            <asp:GridView ID="DatosVisita" runat="server" UseAccessibleHeader="True" BackColor="White" BorderColor="White" BorderWidth="1px" CellPadding="10" CellSpacing="5" ForeColor="White" GridLines="Horizontal"  CssClass="gridview">
            
            </asp:GridView>
        </div>
         <div class="divEs">
        <label class="labelEs">
                     Dar Beca<br>
                    <asp:DropDownList ID="Beca" runat="server" AppendDataBoundItems="true"  class="inputEs" requered ="">
                        <asp:ListItem Value="">Seleccione la beca para el estudiante</asp:ListItem>
                        <asp:ListItem Value="Beca 0">Beca 0</asp:ListItem>
                        <asp:ListItem Value="Beca 1">Beca 1</asp:ListItem>
                         <asp:ListItem Value="Beca 2">Beca 2</asp:ListItem>
                        <asp:ListItem Value="Beca 3">Beca 3</asp:ListItem>
                         <asp:ListItem Value="Beca 4">Beca 4</asp:ListItem>
                         <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </label>

         <label class="labelEs">
                   Observaciones<br>
                    <asp:TextBox ID="Observaciones" runat="server" placeholder="Motivo" class="inputEs" TextMode="MultiLine" Rows="5" cols="40" MaxLength="200"></asp:TextBox>
                </label>
            
            <label class="labelEs">
        <asp:Button ID="Sig" runat="server" Text="Guardar" class="buttonEs" OnClick="Sig_Click1" />
                </label>
           </div>
    </form>
     </div>
</body>
</html>

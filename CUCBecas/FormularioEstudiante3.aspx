﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormularioEstudiante3.aspx.cs" Inherits="CUCBecas.FormularioEstudiante3" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Formulario</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body class="bodyEs">
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
    <div class="containerEs">
        <form runat="server" class="formEs">
            <div class="divEs">
                <h1 class="titulos">➤ Primer dueño del grupo familiar </h1>
               
         
                    <label class="labelEs">
                        <br>Nombre Propietario<br>
                        <asp:TextBox ID="Nombrep" runat="server" placeholder=">Nombre Propietario" class="inputEs" ForeColor="Black" ></asp:TextBox>
                    </label>
               
                    <label class="labelEs">
                       <br>Cédula<br>
                        
                        <asp:TextBox ID="Cedula" runat="server" placeholder="Cédula" class="inputEs"  onkeypress="return numbersonly(event);"></asp:TextBox>
                    </label>
                 <h1 class="titulos">Lotes </h1>
                  <label class="titulos">
                    <label class="labelEs">            
                       <br> Cuántos <br><asp:TextBox ID="CuantosL" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>   
                    </label>
                      </label>
                  <label class="titulos">
                <label class="labelEs">            
                          Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosL" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                    </label>
                        </label>
                <h1 class="titulos"> Fincas / Parcelas</h1>
                <label class="titulos">
                 <label class="labelEs">
                      <br> Cuántos <br> <asp:TextBox ID="CuantosP" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>    
                    </label>
                     </label>
                  <label class="titulos">
                 <label class="labelEs">
                      Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosP" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                     </label>
               </label>

                    <label class="labelEs">
                       Casas de habitación<br> Cuántos  <br>
                        
                         <asp:TextBox ID="CuantosCH" runat="server" placeholder="Casas de habitación" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </label>
                
                     <label class="labelEs">
                       Casas de Alquiler<br> Cuántos <br> 
                         <asp:TextBox ID="CuantosCA" runat="server" placeholder="Casas de Alquiler" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </label>
               
                 <label class="labelEs">
                       Apartamentos<br> Cuántos  <br>
                     <asp:TextBox ID="CuantosAP" runat="server" placeholder="Apartamentos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </label>
                
                    
                
                   <label class="labelEs"><br>Vehículos<br>
                        <asp:DropDownList ID="Vehiculo" runat="server" AppendDataBoundItems="true"  class="inputEs">
                            <asp:ListItem Value="">Seleccione el Tipo de Vehículo</asp:ListItem>
                            <asp:ListItem Value="Carro">Carro</asp:ListItem>
                            <asp:ListItem Value="Moto">Moto</asp:ListItem>
                            <asp:ListItem Value="Trailer">Trailer</asp:ListItem>
                            <asp:ListItem Value="Pickup">Pickup</asp:ListItem>
                            <asp:ListItem Value="Cuadraciclo">Cuadraciclo</asp:ListItem>
                            <asp:ListItem Value="otro">Otro tipo de Vehículo</asp:ListItem>
                        </asp:DropDownList>
                    </Label>
               
                    <label class="labelEs">
                        Descripcion del Vehículo<br>(Cantidad , Marca , Modelo y Año)<br>
                        <asp:TextBox ID="Descripcion" runat="server" placeholder="Ocupación" class="inputEs" TextMode="MultiLine" Rows="5" cols="35" MaxLength="150" MinLength="10"></asp:TextBox>
                    </label>
                     <!--Segunda persona -->
 
                 
      <asp:Label ID="titulo2" runat="server" class="titulos" ><h1 >➤ Segundo dueño</h1></asp:Label>
                      <asp:Label ID="n2" runat="server" class="labelEs" > 
                        <br>Nombre Propietario<br>
                        <asp:TextBox ID="Nombrep2" runat="server" placeholder=">Nombre Propietario" class="inputEs" ForeColor="Black" ></asp:TextBox>
                   </asp:Label>
               
                   <asp:Label ID="c2" runat="server" class="labelEs"> 
                       <br>Cédula<br>
                        
                        <asp:TextBox ID="Cedula2" runat="server" placeholder="Cédula" class="inputEs"  onkeypress="return numbersonly(event);"></asp:TextBox>
                  </asp:Label>
                <asp:Label ID="tl2" runat="server" class="titulos" ><h1 >Lotes </h1></asp:Label>
                   <asp:Label ID="cl2" runat="server" class="titulos"> 
                        <label class="labelEs">
                       <br> Cuántos <br><asp:TextBox ID="CuantosL2" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>   
                            </label>
                   </asp:Label>
               
                   <asp:Label ID="usl2" runat="server" class="titulos"> 
                        <label class="labelEs">         
                       Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosL2" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                            </label>
                    </asp:Label>
                 <asp:Label ID="t2" runat="server" class="titulos" ><h1 >Fincas / Parcelas</h1></asp:Label>
                 <asp:Label ID="cf2" runat="server" class="titulos"> 
                        <label class="labelEs">
                      <br> Cuántos <br> <asp:TextBox ID="CuantosP2" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>    
                   </label>
                            </asp:Label>
                <asp:Label ID="usf2" runat="server" class="titulos"> 
                        <label class="labelEs">
                      Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosP2" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                    </label>
                            </asp:Label>
              
                  <asp:Label ID="ch2" runat="server" class="labelEs">   
                       Casas de habitación<br> Cuántos  <br>
                        
                         <asp:TextBox ID="CuantosCH2" runat="server" placeholder="Casas de habitación" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                   <asp:Label ID="ca2" runat="server" class="labelEs"> 
                       Casas de Alquiler<br> Cuántos <br> 
                         <asp:TextBox ID="CuantosCA2" runat="server" placeholder="Casas de Alquiler" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
               
                 <asp:Label ID="aparta2" runat="server" class="labelEs"> 
                       Apartamentos<br> Cuántos  <br>
                     <asp:TextBox ID="CuantosAP2" runat="server" placeholder="Apartamentos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                    
                
                  <asp:Label ID="v2" runat="server" class="labelEs"> <br>Vehículos<br>
                        <asp:DropDownList ID="Vehiculo2" runat="server" AppendDataBoundItems="true"  class="inputEs">
                            <asp:ListItem Value="">Seleccione el Tipo de Vehículo</asp:ListItem>
                            <asp:ListItem Value="Carro">Carro</asp:ListItem>
                            <asp:ListItem Value="Moto">Moto</asp:ListItem>
                            <asp:ListItem Value="Trailer">Trailer</asp:ListItem>
                            <asp:ListItem Value="Pickup">Pickup</asp:ListItem>
                            <asp:ListItem Value="Cuadraciclo">Cuadraciclo</asp:ListItem>
                            <asp:ListItem Value="otro">Otro tipo de Vehículo</asp:ListItem>
                        </asp:DropDownList>
                     </asp:Label>
               
                   <asp:Label ID="dv2" runat="server" class="labelEs"> 
                        Descripcion del Vehículo<br>(Cantidad , Marca , Modelo y Año)<br>
                        <asp:TextBox ID="Descripcion2" runat="server" placeholder="Ocupación" class="inputEs" TextMode="MultiLine" Rows="5" cols="35" MaxLength="150" MinLength="10"></asp:TextBox>
                    </asp:Label>
                <!-- Tercer Dueno-->
                
                 
      <asp:Label ID="titulo3" runat="server" class="titulos" ><h1 >➤ Tercer dueño</h1></asp:Label>
                      <asp:Label ID="n3" runat="server" class="labelEs" > 
                        <br>Nombre Propietario<br>
                        <asp:TextBox ID="Nombrep3" runat="server" placeholder=">Nombre Propietario" class="inputEs" ForeColor="Black" ></asp:TextBox>
                   </asp:Label>
               
                   <asp:Label ID="c3" runat="server" class="labelEs"> 
                       <br>Cédula<br>
                        
                        <asp:TextBox ID="Cedula3" runat="server" placeholder="Cédula" class="inputEs" onkeypress="return numbersonly(event);" ></asp:TextBox>
                  </asp:Label>
                <asp:Label ID="tl3" runat="server" class="titulos" ><h1 >Lotes </h1></asp:Label>
                   <asp:Label ID="cl3" runat="server" class="titulos"> 
                        <label class="labelEs">          
                       <br> Cuántos <br><asp:TextBox ID="CuantosL3" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>   
                   </label>
                            </asp:Label>
               
                   <asp:Label ID="usl3" runat="server" class="titulos"> 
                        <label class="labelEs">         
                          Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosL3" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                   </label>
                            </asp:Label>
                 <asp:Label ID="t3" runat="server" class="titulos" ><h1 >Fincas / Parcelas</h1></asp:Label>
                 <asp:Label ID="cf3" runat="server" class="titulos"> 
                        <label class="labelEs">
                      <br> Cuántos <br> <asp:TextBox ID="CuantosP3" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>    
                   </label>
                            </asp:Label>
                <asp:Label ID="usf3" runat="server" class="titulos"> 
                        <label class="labelEs">  
                      Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosP3" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                    </label>
                            </asp:Label>
              
                  <asp:Label ID="ch3" runat="server" class="labelEs">   
                       Casas de habitación<br> Cuántos  <br>
                        
                         <asp:TextBox ID="CuantosCH3" runat="server" placeholder="Casas de habitación" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                   <asp:Label ID="ca3" runat="server" class="labelEs"> 
                       Casas de Alquiler<br> Cuántos <br> 
                         <asp:TextBox ID="CuantosCA3" runat="server" placeholder="Casas de Alquiler" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
               
                 <asp:Label ID="aparta3" runat="server" class="labelEs"> 
                       Apartamentos<br> Cuántos  <br>
                     <asp:TextBox ID="CuantosAP3" runat="server" placeholder="Apartamentos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                    
                
                  <asp:Label ID="v3" runat="server" class="labelEs"> <br>Vehículos<br>
                        <asp:DropDownList ID="Vehiculo3" runat="server" AppendDataBoundItems="true"  class="inputEs">
                            <asp:ListItem Value="">Seleccione el Tipo de Vehículo</asp:ListItem>
                            <asp:ListItem Value="Carro">Carro</asp:ListItem>
                            <asp:ListItem Value="Moto">Moto</asp:ListItem>
                            <asp:ListItem Value="Trailer">Trailer</asp:ListItem>
                            <asp:ListItem Value="Pickup">Pickup</asp:ListItem>
                            <asp:ListItem Value="Cuadraciclo">Cuadraciclo</asp:ListItem>
                            <asp:ListItem Value="otro">Otro tipo de Vehículo</asp:ListItem>
                        </asp:DropDownList>
                     </asp:Label>
               
                   <asp:Label ID="dv3" runat="server" class="labelEs"> 
                        Descripcion del Vehículo<br>(Cantidad , Marca , Modelo y Año)<br>
                        <asp:TextBox ID="Descripcion3" runat="server" placeholder="Ocupación" class="inputEs" TextMode="MultiLine" Rows="5" cols="35" MaxLength="150" MinLength="10"></asp:TextBox>
                    </asp:Label>
                     <!--Cuarto dueno-->
 
                 
      <asp:Label ID="titulo4" runat="server" class="titulos" ><h1 >➤ Cuarto dueño</h1></asp:Label>
                      <asp:Label ID="n4" runat="server" class="labelEs" > 
                        <br>Nombre Propietario<br>
                        <asp:TextBox ID="Nombrep4" runat="server" placeholder=">Nombre Propietario" class="inputEs" ForeColor="Black" ></asp:TextBox>
                   </asp:Label>
               
                   <asp:Label ID="c4" runat="server" class="labelEs"> 
                       <br>Cédula<br>
                        
                        <asp:TextBox ID="Cedula4" runat="server" placeholder="Cédula" class="inputEs" onkeypress="return numbersonly(event);" ></asp:TextBox>
                  </asp:Label>
                <asp:Label ID="tl4" runat="server" class="titulos" ><h1 >Lotes </h1></asp:Label>
                   <asp:Label ID="cl4" runat="server" class="titulos"> 
                        <label class="labelEs">           
                       <br> Cuántos <br><asp:TextBox ID="CuantosL4" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>   
                   </label>
                            </asp:Label>
               
                   <asp:Label ID="usl4" runat="server" class="titulos"> 
                        <label class="labelEs">         
                          Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosL4" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                   </label>
                            </asp:Label>
                 <asp:Label ID="t4" runat="server" class="titulos" ><h1 >Fincas / Parcelas</h1></asp:Label>
                 <asp:Label ID="cf4" runat="server" class="titulos"> 
                        <label class="labelEs">
                      <br> Cuántos <br> <asp:TextBox ID="CuantosP4" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>    
                  </label>
                            </asp:Label>
                <asp:Label ID="usf4" runat="server" class="titulos"> 
                        <label class="labelEs">
                      Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosP4" runat="server" placeholder="Usos" class="inputEs" textmode="MultiLine"  Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                    </label>
                            </asp:Label>
              
                  <asp:Label ID="ch4" runat="server" class="labelEs">   
                       Casas de habitación<br> Cuántos  <br>
                        
                         <asp:TextBox ID="CuantosCH4" runat="server" placeholder="Casas de habitación" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                   <asp:Label ID="ca4" runat="server" class="labelEs"> 
                       Casas de Alquiler<br> Cuántos <br> 
                         <asp:TextBox ID="CuantosCA4" runat="server" placeholder="Casas de Alquiler" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
               
                 <asp:Label ID="aparta4" runat="server" class="labelEs"> 
                       Apartamentos<br> Cuántos  <br>
                     <asp:TextBox ID="CuantosAP4" runat="server" placeholder="Apartamentos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                    
                
                  <asp:Label ID="v4" runat="server" class="labelEs"> <br>Vehículos<br>
                        <asp:DropDownList ID="Vehiculo4" runat="server" AppendDataBoundItems="true"  class="inputEs">
                            <asp:ListItem Value="">Seleccione el Tipo de Vehículo</asp:ListItem>
                            <asp:ListItem Value="Carro">Carro</asp:ListItem>
                            <asp:ListItem Value="Moto">Moto</asp:ListItem>
                            <asp:ListItem Value="Trailer">Trailer</asp:ListItem>
                            <asp:ListItem Value="Pickup">Pickup</asp:ListItem>
                            <asp:ListItem Value="Cuadraciclo">Cuadraciclo</asp:ListItem>
                            <asp:ListItem Value="otro">Otro tipo de Vehículo</asp:ListItem>
                        </asp:DropDownList>
                     </asp:Label>
               
                   <asp:Label ID="dv4" runat="server" class="labelEs"> 
                        Descripcion del Vehículo<br>(Cantidad , Marca , Modelo y Año)<br>
                        <asp:TextBox ID="Descripcion4" runat="server" placeholder="Ocupación" class="inputEs" TextMode="MultiLine" Rows="5" cols="35" MaxLength="150" MinLength="10"></asp:TextBox>
                    </asp:Label>
                     <!--Qunto dueno-->
 
                 
      <asp:Label ID="titulo5" runat="server" class="titulos" ><h1 >➤ Quinto dueño</h1></asp:Label>
                      <asp:Label ID="n5" runat="server" class="labelEs" > 
                        <br>Nombre Propietario<br>
                        <asp:TextBox ID="Nombrep5" runat="server" placeholder=">Nombre Propietario" class="inputEs" ForeColor="Black" ></asp:TextBox>
                   </asp:Label>
               
                   <asp:Label ID="c5" runat="server" class="labelEs"> 
                       <br>Cédula<br>
                        
                        <asp:TextBox ID="Cedula5" runat="server" placeholder="Cédula" class="inputEs" onkeypress="return numbersonly(event);" ></asp:TextBox>
                  </asp:Label>
                <asp:Label ID="tl5" runat="server" class="titulos" ><h1 >Lotes </h1></asp:Label>
                   <asp:Label ID="cl5" runat="server" class="titulos"> 
                        <label class="labelEs">            
                       <br> Cuántos <br><asp:TextBox ID="CuantosL5" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>   
                  </label>
                            </asp:Label>
               
                   <asp:Label ID="usl5" runat="server" class="titulos"> 
                        <label class="labelEs">        
                          Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosL5" runat="server" placeholder="Usos" class="inputEs" TextMode="MultiLine" Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                   </label>
                            </asp:Label>
                 <asp:Label ID="t5" runat="server" class="titulos" ><h1 >Fincas / Parcelas</h1></asp:Label>
                 <asp:Label ID="cf5" runat="server" class="titulos"> 
                        <label class="labelEs">
                      <br> Cuántos <br> <asp:TextBox ID="CuantosP5" runat="server" placeholder="Cuantos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>    
                   </label>
                            </asp:Label>
                <asp:Label ID="usf5" runat="server" class="titulos"> 
                        <label class="labelEs"> 
                      Usos , Extensión y Ubicacion <br><asp:TextBox ID="UsosP5" runat="server" placeholder="Usos" class="inputEs" TextMode="MultiLine" Rows="5" cols="40" MaxLength="200" MinLength="25"></asp:TextBox>
                    </label>
                            </asp:Label>
              
                  <asp:Label ID="ch5" runat="server" class="labelEs">   
                       Casas de habitación<br> Cuántos  <br>
                        
                         <asp:TextBox ID="CuantosCH5" runat="server" placeholder="Casas de habitación" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                   <asp:Label ID="ca5" runat="server" class="labelEs"> 
                       Casas de Alquiler<br> Cuántos <br> 
                         <asp:TextBox ID="CuantosCA5" runat="server" placeholder="Casas de Alquiler" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
               
                 <asp:Label ID="aparta5" runat="server" class="labelEs"> 
                       Apartamentos<br> Cuántos  <br>
                     <asp:TextBox ID="CuantosAP5" runat="server" placeholder="Apartamentos" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                    </asp:Label>
                
                    
                
                  <asp:Label ID="v5" runat="server" class="labelEs"> <br>Vehículos<br>
                        <asp:DropDownList ID="Vehiculo5" runat="server" AppendDataBoundItems="true"  class="inputEs">
                            <asp:ListItem Value="">Seleccione el Tipo de Vehículo</asp:ListItem>
                            <asp:ListItem Value="Carro">Carro</asp:ListItem>
                            <asp:ListItem Value="Moto">Moto</asp:ListItem>
                            <asp:ListItem Value="Trailer">Trailer</asp:ListItem>
                            <asp:ListItem Value="Pickup">Pickup</asp:ListItem>
                            <asp:ListItem Value="Cuadraciclo">Cuadraciclo</asp:ListItem>
                            <asp:ListItem Value="otro">Otro tipo de Vehículo</asp:ListItem>
                        </asp:DropDownList>
                     </asp:Label>
               
                   <asp:Label ID="dv5" runat="server" class="labelEs"> 
                        Descripcion del Vehículo<br>(Cantidad , Marca , Modelo y Año)<br>
                        <asp:TextBox ID="Descripcion5" runat="server" placeholder="Ocupación" class="inputEs" TextMode="MultiLine" Rows="5" cols="35" MaxLength="150" MinLength="10"></asp:TextBox>
                    </asp:Label>
                </div>
             <div class="centrado">
            <a href="instruciones3.aspx" class="buttonEs">Ver Instrucciónes</a>
       
                    <asp:Button ID="Sig" runat="server" Text="Guardar y Seguir"  class="buttonEs" OnClick="Sig_Click" />
                </div>
        </form>
    </div>
      <script type="text/javascript">
    function numbersonly(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode
        if (unicode != 8 && unicode != 44) {
            if (unicode < 48 || unicode > 57) //if not a number
            { return false } //disable key press    
        }
    }  
</SCRIPT>
</body>
</html>

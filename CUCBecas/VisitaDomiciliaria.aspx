﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisitaDomiciliaria.aspx.cs" Inherits="CUCBecas.VisitaDomiciliaria" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Visita Domiciliaria</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body class="bodyEs">
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                 <a href="PrincipalTrabajoSocial.aspx">Salir</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio">
    </div>
    <div class="containerEs">
        <form runat="server" class="formEs">
            <div class="divEs">
                <h1 class="titulos">Datos Generales </h1>
                <label class="labelEs">
                    Cédula del estudiante<br>
                    <asp:TextBox ID="Cédula" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula" AutoPostBack="true"  MaxLength="12"></asp:TextBox>
                  
                </label>

                <label class="labelEs">
                    Nombre del estudiante<br>
                    <asp:TextBox ID="Nombre" runat="server" placeholder="Nombre" class="inputEs" ReadOnly="True"></asp:TextBox>
                </label>

                <label class="labelEs">
                    Carrera<br>
                    <asp:TextBox ID="Carrera" runat="server" placeholder="Carrera" class="inputEs" ReadOnly="True"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Fecha<br>
                    <asp:TextBox ID="Fecha" runat="server" placeholder="Fecha" class="inputEs" ReadOnly="True"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Personas entravistadas y parentesco con el estudiante<br>
                    <asp:TextBox ID="PersonasEntravistadas" runat="server" placeholder="Personas entravistadas" class="inputEs" TextMode="MultiLine" Rows="3" cols="60" MaxLength="200" required=""></asp:TextBox>
                </label>
                <label class="labelEs">
                    Motivo de la realización<br>
                    <asp:TextBox ID="Motivo" runat="server" placeholder="Motivo" class="inputEs" TextMode="MultiLine" Rows="5" cols="40" MaxLength="200" required=""></asp:TextBox>
                </label>
                <label class="labelEs">
                    Condicion de acceso al lugar<br>
                    <asp:DropDownList ID="CondicionA" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Seleccione codicion de acceso</asp:ListItem>
                        <asp:ListItem Value="Buena">Buena</asp:ListItem>
                        <asp:ListItem Value="Regular">Regular</asp:ListItem>
                        <asp:ListItem Value="Limitada">Limitada</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
                    Medios de acceso<br>
                    <asp:TextBox ID="MediosAceso" runat="server" placeholder="Motivo" class="inputEs" required=""></asp:TextBox>
                </label>
                <label class="labelEs">
                    Infraestructura<br>
                    <asp:TextBox ID="Infraestructura" runat="server" placeholder="Motivo" class="inputEs" required=""></asp:TextBox>
                </label>
                <label class="labelEs">
                    Vulnerabilidad social<br>
                    <asp:TextBox ID="VulnerabilidadSocial" runat="server" placeholder="Motivo" class="inputEs"></asp:TextBox>
                </label>
                <label class="labelEs">
          <br>
                    <asp:CheckBox ID="AccesoCentro" runat="server" class="inputEs" Text="Acceso a centros comerciales de abastecimientos" Width="320px"/>
                </label>
                <h1 class="titulos">Acesso a servicios públicos </h1>
                <br>
                <label class="labelEs">
                  <asp:CheckBox ID="Escuela" runat="server" class="inputEs" Text="Escuela"/>
                </label>
                <label class="labelEs">
                     <asp:CheckBox ID="Electricidad" runat="server" class="inputEs" Text="Electricidad"/>
                </label>
                
                <label class="labelEs">
                     <asp:CheckBox ID="Internet" runat="server" class="inputEs" Text="Internet"/>
                </label>
                <label class="labelEs">
                    <asp:CheckBox ID="Cable" runat="server" class="inputEs" Text="Tv por Cable" />
                </label>
                <label class="labelEs">
                     <asp:CheckBox ID="Transporte" runat="server" class="inputEs" Text="Transporte"/>
                </label>
                <label class="labelEs">
                     <asp:CheckBox ID="RecoleccionBasura" runat="server" class="inputEs" Text="Recolección de basura" />
                </label>
                <label class="labelEs">
                     <asp:CheckBox ID="CentroSalud" runat="server" class="inputEs" Text="Centro de salud"/>
                </label>
                <label class="labelEs">
                    <asp:CheckBox ID="AguaPotable" runat="server" class="inputEs" Text="Agua potable"/>
                </label>
                <label class="labelEs">
                     <asp:CheckBox ID="Teléfono" runat="server" class="inputEs" Text="Teléfono celular"/>
                </label>
                <label class="labelEs">
                    <asp:CheckBox ID="Alumbrado" runat="server" class="inputEs"  Text="Alumbrado público"/>
                </label>
                <label class="labelEs">
                     <asp:CheckBox ID="Otros" runat="server" class="inputEs" Text="Otros"/>
                </label>
                <label class="labelEs">
                    Fuentes de empleo<br>
                    <asp:DropDownList ID="FuentesEmpleo" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Seleccione Fuente de empleo</asp:ListItem>
                        <asp:ListItem Value="Formal">Formal</asp:ListItem>
                        <asp:ListItem Value="Informal">Informal</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
                    <asp:CheckBox ID="Recreacion" runat="server" class="inputEs" Text="Acceso a espacios de recreación" Width="320px"/>
                </label>
                <h1 class="titulos">Características de la conformación familiar </h1>

                <label class="labelEs">
                    Número de integrantes<br>
                    <asp:TextBox ID="NumeroIntegrantes" runat="server" placeholder="Motivo" class="inputEs" onkeypress="return numbersonly(event);" required=""></asp:TextBox>
                </label>
                <label class="labelEs">
                    Número de personas que trabajan<br>
                    <asp:TextBox ID="NumeroTrabajadores" runat="server" placeholder="Motivo" class="inputEs" onkeypress="return numbersonly(event);" required=""></asp:TextBox>
                </label>

                <label class="labelEs">
                    Hogar jefeado por mujeres<br>
                    <asp:DropDownList ID="Jmujeres" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Jefeado por mujeres</asp:ListItem>
                        <asp:ListItem Value="1">Si</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:DropDownList>
                </label>

                <label class="labelEs">
                    Número de personas Adultas Mayores<br>
                    <asp:TextBox ID="NumeroAdultasMayores" runat="server" placeholder="Motivo" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Número de personas con discapacidad<br>
                    <asp:TextBox ID="NumeroDiscapacidados" runat="server" placeholder="Motivo" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Número de personas que estudian<br>
                    <asp:TextBox ID="NumeroEstudiantes" runat="server" placeholder="Motivo" class="inputEs" onkeypress="return numbersonly(event);"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Situaciones de salud<br>
                    <asp:TextBox ID="SituacioneSalud" runat="server" placeholder="Motivo" class="inputEs"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Situación de desempleo familiar<br>
                    <asp:TextBox ID="SituacionDesempleo" runat="server" placeholder="Motivo" class="inputEs"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Problemas de abuso o adicción a sustacias psicoactivas<br>
                     <asp:DropDownList ID="Adicciones" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Problemas de abuso o adicción</asp:ListItem>
                        <asp:ListItem Value="Si">Si</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
               Observaciones de problemas de abuso o adicción a sustacias psicoactivas<br>
                    <asp:TextBox ID="observaciondeProblemas" runat="server" placeholder="Motivo" class="inputEs" TextMode="MultiLine" Rows="5" cols="40" MaxLength="200"></asp:TextBox>
                </label>
                <label class="labelEs">
                    Antecedentes de violencia intrafamiliar<br>
                      <asp:DropDownList ID="Violencia" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Antecedentes de violencia</asp:ListItem>
                        <asp:ListItem Value="Si">Si</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
                    Falta de apoyo económico o familiar<br>
                    <asp:TextBox ID="FaltaApoyo" runat="server" placeholder="Motivo" class="inputEs"></asp:TextBox>
                </label>
                <h1 class="titulos">Características de vivienda</h1>
                <label class="labelEs">
                    Tenecia<br>
                    <asp:DropDownList ID="Tenecia" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Selecione Tenecias</asp:ListItem>
                        <asp:ListItem Value="Prestada">Prestada</asp:ListItem>
                        <asp:ListItem Value="Prestada ubicada en precario">Prestada ubicada en precario</asp:ListItem>
                        <asp:ListItem Value="Alquilada">Alquilada</asp:ListItem>
                        <asp:ListItem Value="Alquilada ubicada en precario">Alquilada ubicada en precario</asp:ListItem>
                        <asp:ListItem Value="Propia hipotecada">Propia hipotecada</asp:ListItem>
                        <asp:ListItem Value="Propia hipotecada ubicada en precario">Propia hipotecada ubicada en precario</asp:ListItem>
                        <asp:ListItem Value="Propia sin hipoteca">Propia sin hipoteca</asp:ListItem>
                        <asp:ListItem Value="Propia sin hipoteca">Propia sin hipoteca  ubicada en precario</asp:ListItem>
                        <asp:ListItem Value="Propia Donada sin hipoteca">Propia Donada sin hipoteca</asp:ListItem>
                        <asp:ListItem Value="Propia Donada sin hipoteca">Propia Donada sin hipoteca ubicada en precario</asp:ListItem>
                        <asp:ListItem Value="Propia Donada hipotecada">Propia Donada hipotecada</asp:ListItem>
                        <asp:ListItem Value="Propia Donada hipotecada ubicada en precario">Propia Donada hipotecada ubicada en precario</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
                    Adquisión<br>
                    <asp:DropDownList ID="Adquision" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Selecione Adquision</asp:ListItem>
                        <asp:ListItem Value="Financiamiento Bancario">Financiamiento Bancario</asp:ListItem>
                        <asp:ListItem Value="Otorgada por el IMAS o INVU">Otorgada por el IMAS o INVU</asp:ListItem>
                        <asp:ListItem Value="Bono de la vivienda">Bono de la vivienda</asp:ListItem>
                        <asp:ListItem Value="Recursos propios">Recursos propios</asp:ListItem>
                        <asp:ListItem Value="Donación">Donación</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <label class="labelEs">
                    Estado<br>
                    <asp:DropDownList ID="Estado" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Selecione Estado</asp:ListItem>
                        <asp:ListItem Value="Bueno">Bueno</asp:ListItem>
                        <asp:ListItem Value="Regular"></asp:ListItem>
                        <asp:ListItem Value="Malo">Malo</asp:ListItem>
                        <asp:ListItem Value="Precario">Precario</asp:ListItem>
                        <asp:ListItem Value="Hacinamiento">Hacinamiento</asp:ListItem>
                    </asp:DropDownList>
                </label>
                 <label class="labelEs">
                    Aposentos<br>
                    <asp:DropDownList ID="Aposentos" runat="server" AppendDataBoundItems="true"  class="inputEs" required="">
                        <asp:ListItem Value="">Selecione Aposentos</asp:ListItem>
                        <asp:ListItem Value="Sala , Comedor">Sala , Comedor</asp:ListItem>
                        <asp:ListItem Value="Sala , Baño / Sevicio Sanitario">Sala , Baño / Sevicio Sanitario</asp:ListItem>
                        <asp:ListItem Value="Sala , Dormitorio">Sala , Dormitorio</asp:ListItem>
                        <asp:ListItem Value="Sala, Comedor , Baño / Sevicio Sanitario">Sala , Comedor , Baño / Sevicio Sanitario</asp:ListItem>
                        <asp:ListItem Value="Sala, Comedor , Baño / Sevicio Sanitario, Dormitorio">Sala , Comedor , Baño / Sevicio Sanitario , Dormitorio</asp:ListItem>
                        <asp:ListItem Value="Comedor , Baño / Sevicio Sanitario">Comedor , Baño / Sevicio Sanitario</asp:ListItem>
                        <asp:ListItem Value="Comedor , Baño / Sevicio Sanitario">Comedor , Dormitorio"</asp:ListItem>
                        <asp:ListItem Value="Comedor , Baño / Sevicio Sanitario, Dormitorio">Comedor , Baño / Sevicio Sanitario, Dormitorio</asp:ListItem>
                         <asp:ListItem Value="Dormitorio , Baño / Sevicio Sanitario">Dormitorio , Baño / Sevicio Sanitario</asp:ListItem>
                    </asp:DropDownList>
                </label>
                <h1 class="titulos">Menaje</h1>
                 <label class="labelEs">
                    <asp:CheckBox ID="ducha" runat="server" class="inputEs" Text="Tanque de agua caliente o ducha" Width="320px" />
                </label>
                  <label class="labelEs">
                    <asp:CheckBox ID="microondas" runat="server" class="inputEs" Text="Horno microondas" />
                </label>
                <label class="labelEs">
                    <asp:CheckBox ID="Computadora" runat="server" class="inputEs" Text="Computadora" />
                </label>
                 <label class="labelEs">
                    <asp:CheckBox ID="Consola" runat="server" class="inputEs" Text="Consola de video juegos" />
                </label>
                 <label class="labelEs">
                    <asp:CheckBox ID="Radio" runat="server" class="inputEs" Text="Radio o equipo de sonido" />
                </label>
                 <label class="labelEs">
                    <asp:CheckBox ID="Televisor" runat="server" class="inputEs" Text="Televisor / pantallas" />
                </label>
                 <label class="labelEs">
                    <asp:CheckBox ID="CocinaE" runat="server" class="inputEs" Text="Cocina eléctrica o de gas" />
                </label>
                  <label class="labelEs">
                    <asp:CheckBox ID="Refrigeradora" runat="server" class="inputEs" Text="Refrigeradora" />
                </label>
                 <label class="labelEs">
                    <asp:CheckBox ID="Lavadora" runat="server" class="inputEs" Text="Lavadora" />
                </label>
                 <label class="labelEs">
                    <asp:CheckBox ID="CocinaL" runat="server" class="inputEs" Text="Cocina de leña" />
                </label>
                 <h1 class="titulos">Justificación</h1>
                <label class="labelEs">
                   Observaciones, recomendaciones y consideraciones de Trabajo Social (en referencia al entorno, caracterización familiar, particularidades del estudiante, otros).<br>
                    <asp:TextBox ID="Observaciones" runat="server" placeholder="Motivo" class="inputEs" TextMode="MultiLine" Rows="5" cols="40" MaxLength="200"></asp:TextBox>
                </label>
                 <label class="labelEs">
                    Categoría sugerida de beca según valoración domiciliaria<br>
                    <asp:DropDownList ID="Beca" runat="server" AppendDataBoundItems="true"  class="inputEs" requered ="">
                        <asp:ListItem Value="">Seleccione la sugerencia de beca para el estudiante</asp:ListItem>
                        <asp:ListItem Value="Beca 0">Beca 0</asp:ListItem>
                        <asp:ListItem Value="Beca 1">Beca 1</asp:ListItem>
                         <asp:ListItem Value="Beca 2">Beca 2</asp:ListItem>
                        <asp:ListItem Value="Beca 3">Beca 3</asp:ListItem>
                         <asp:ListItem Value="Beca 4">Beca 4</asp:ListItem>
                         <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </label>
            </div>
            
              <div class="rowb">
      <div class="colb un">
      <span><h2>Consentimiento</h2></span><br>
         <span>  Por este medio yo <asp:TextBox ID="NomEntre" runat="server" placeholder="Nombre" class="inputEs" ></asp:TextBox> , cédula <asp:TextBox ID="CedEntre" runat="server" placeholder="Nombre" class="inputEs" onkeypress="return numbersonly(event);" minLength="9" MaxLength="9"></asp:TextBox>
autorizo a la Licda. Jennifer Araya Pérez a tomar fotografías de mi propiedad para ser usada con fines 
de análisis por parte de la Unidad de Trabajo Social del Colegio Universitario de Cartago.
             <br>
              <br>
             
             </span>  
             <span>
              <asp:CheckBox ID="Acepto" runat="server" class="inputEs" Text="Acepto los Terminos" required="" />
         </span>  

           <br>
           <br>
          </div>
                  </div>
                      <div class="centrado">
                             <asp:Button ID="Button1" runat="server" Text="Guardar datos" class="buttonEs" Enabled="False" Visible="False" />
            <asp:Button ID="Sig" runat="server" Text="Guardar datos" class="buttonEs" OnClick="Sig_Click" />
                          </div>
        </form>
    </div>
    <script type="text/javascript">
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 44) {
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }

    </script>
</body>
</html>

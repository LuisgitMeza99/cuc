﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class instruciones1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["IDForm"] = "";
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            string cod = (string)Session["cod"];

            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1") || !cod.Equals("true"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
           
            Controlador control = new Controlador();
            control.selectform1();
            string idform = (string)Session["IDForm"];
            if (!string.IsNullOrEmpty(idform))
            {
                Response.Redirect("instruciones2");
            }
        }

        protected void Sig_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormularioEstudiante1");
        }
    }
}
﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["IDusuario"] = "";
            Session["Nombre"] = "";
            Session["Estado"] = "";
            Session["Fecha"] = "";
            Session["sesion"] = "";
            Session["email"] = "";
            Session["mj"] = "";
            Controlador control = new Controlador();
            control.CorreoMasivo();
            string[] val = control.FechasIncio();
            Label1.Text = val[0];
            Label2.Text = val[1];
            Label3.Text = val[2];
            Label4.Text = val[3];
            Label5.Text = val[4];
            Label6.Text = val[5];
        }
    }
}
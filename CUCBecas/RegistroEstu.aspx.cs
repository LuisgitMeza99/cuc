﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            string contra = password.Text; 
            if (contra.Equals(password2.Text)) {
                if (contra.Length >= 5)
                {
                    Controlador control = new Controlador();
                    string NombreFull = Nombre.Text + " " + Apellidos.Text;
                    string cedu = Cedula.Text;
                    string nom = Nombre.Text;
                    string apellidos = Apellidos.Text;
                    string correo = Correo.Text;
                    string sexo = Sexo.Text;
                    string carrera = Carrera.SelectedValue;
                    string nacimiento = Nacimiento.Text;

                    float Registro = control.RegistroEstu(cedu, nom, apellidos, correo, sexo, carrera, nacimiento, contra);
                    if (Registro == 1)
                    {
                        control.EnviarCorreoRegistro(NombreFull, correo);
                        Response.Redirect("IniciarSesion");
                    }
                    else
                    {
                        Response.Write("<script>alert('Cedula o Dimex ya registrados en el sistema')</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Las Contraseña tiene que tener mas de 5 caracteres')</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Las Contraseña no coincide')</script>");
            }
           
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Facturacion.aspx.cs" Inherits="CUCBecas.Facturacion" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Facturacion</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
<body class="bodyEs">
    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div class="espacio"> 
    </div>
    <div class="containerEs">
        <form runat="server" class="formEs">
            <div class="divEs">
                <h1 class="titulos">Factura </h1>
                <label class="labelEs">
                     <br>
                    Cédula del estudiante<br>
                    <asp:TextBox ID="Cédula" runat="server" placeholder="Cédula" class="inputEs" ForeColor="Black" onkeypress="return numbersonly(event);" required="cedula" AutoPostBack="True" MaxLength="12"></asp:TextBox>
                  
          </label>
                  <label class="labelEs">
                    <br>
                    <br>
                      <asp:Button ID="Button1" runat="server" Text="Cargar datos" class="buttonEs" OnClick="Button1_Click"  />
                </label>
                <label class="labelEs">
                    <br>
                    Nombre<br>

                    <asp:TextBox ID="Nombre" runat="server" placeholder="Nombre" class="inputEs" ></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                    Apellidos<br>

                    <asp:TextBox ID="Apellidos" runat="server" placeholder="Apellidos" class="inputEs" ></asp:TextBox>
                </label>

                <label class="labelEs">
                    <br>
                   Correo<br>
                    <asp:TextBox ID="Correo" runat="server" placeholder="Apellidos" class="inputEs" ></asp:TextBox>
                 
                </label>

                <label class="labelEs">
                     <br>
                    Total
                    <br>
                     <asp:TextBox ID="costo" runat="server" placeholder="Cédula" class="inputEs"  required="cedula" ReadOnly="true" ></asp:TextBox>
                  </label>
               <label class="labelEs">
                     <br>
                     <br>
            <asp:Button ID="Sig" runat="server" Text="Realizar Factura" class="buttonEs" OnClick="Sig_Click"  />
                           </label>
                   
         </form>
        
    </div>
    <script type="text/javascript">
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 44) {
                if (unicode < 48 || unicode > 57) //if not a number
                { return false } //disable key press    
            }
        }

    </script>
</body>
</html>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class PrincipalFinanciero : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("3"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
        }
    }
}
﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class instruciones3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            string cod = (string)Session["cod"];

            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("1") || !cod.Equals("true"))
                {
                    Response.Redirect("IniciarSesion");
                }
            }
            Controlador control = new Controlador();
            string val = control.selectform3();
            if (val.Equals("1"))
            {
                Response.Redirect("FormularioEstudiante4");
            }
            ced.Text = id;
            DateTime fech = DateTime.Now;
            fecha.Text = fech.ToShortDateString();
        }

        protected void Sigu_Click(object sender, EventArgs e)
        {
            if (Acepto.Checked) {
                Controlador control = new Controlador();
                control.AceptoDB(fecha.Text);
                Session["miembros"] = num.Text;
                Response.Redirect("FormularioEstudiante3");
            }
        }
    }
}
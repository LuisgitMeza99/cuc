﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class CambiarFecha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("2"))
                {
                    Response.Redirect("IniciarSesion");
                }
                Controlador control = new Controlador();
              string [] v1 = control.selectFechas("1");
                FechaPago.Text = v1[0];
                FechaEntrega.Text = v1[1];
                FechaResultado.Text = v1[2];
                FechaApelacion.Text = v1[3];
                string[] v2 = control.selectFechas("2");
                FechaPago2.Text = v2[0];
                FechaEntrega2.Text = v2[1];
                FechaResultado2.Text = v2[2];
                FechaApelacion2.Text = v2[3];
                string[] v3 = control.selectFechas("3");
                FechaPago3.Text = v3[0];
                FechaEntrega3.Text = v3[1];
                FechaResultado3.Text = v3[2];
                FechaApelacion3.Text = v3[3];
            }
        }

        protected void Entra_Click(object sender, EventArgs e)
        {
            string C1 = "1";
            string C2 = "2";
            string C3 = "3";
            Controlador control = new Controlador();
            control.actualizarFechas(C1, FechaPago.Text, FechaEntrega.Text, FechaResultado.Text, FechaApelacion.Text);
            control.actualizarFechas(C2, FechaPago2.Text, FechaEntrega2.Text, FechaResultado2.Text, FechaApelacion2.Text);
            control.actualizarFechas(C3, FechaPago3.Text, FechaEntrega3.Text, FechaResultado3.Text, FechaApelacion3.Text);
            Response.Redirect("PrincipalTrabajoSocial");
        }

       
    }
}
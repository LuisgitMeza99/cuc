﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OlvidoContra.aspx.cs" Inherits="CUCBecas.OlvidoContra" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio Sesión</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,800,700,900,300,100" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
</head>

<body class="overlay">

    <header>
        <div class="contenedor">

            <h1>
                <img src="img/logo_cuc.png" class="logo"></h1>
            <input type="checkbox" id="menu-bar">
            <label class="fontawesome-align-justify" for="menu-bar"></label>
            <nav class="menu">
                <a href="default.aspx">Inicio</a>
                <a href="IniciarSesion.aspx">Iniciar Sesión</a>
                <a href="Contact.aspx">Contactenos</a>
            </nav>
        </div>
    </header>
    <!-- TERMINA EL MENU -->
    <div>
        <h1 class="inih1">Cambio de Contraseña</h1>
        <div class="espaciob"></div>

        <div class="wrapper">
            <div class="container">
                <form class="form" runat="server">
                    <asp:TextBox ID="Cedula" runat="server" placeholder="Cédula o Dimex" required="Falta tu cédula o dimex" class="log"></asp:TextBox>
                    <asp:TextBox ID="Email" runat="server" placeholder="Correo" required="Falta tu cédula o dimex" class="log"></asp:TextBox>
                    <asp:Button ID="Entrar" runat="server" Text="Enviar" OnClick="Entrar_Click" />


                </form>

            </div>
        </div>
    </div>




</body>
</html>

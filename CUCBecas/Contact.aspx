﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="CUCBecas.Contacto" %>

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<!DOCTYPE html>
<html class="wide wow-animation" lang="es">

<head>

    <title>Inicio</title>
    <meta charset="utf-8">
    <link href="css/estilos.css" rel="stylesheet" />
    <script src="js/main.js"></script>


</head>
    <body>
  <header>
  <div class="contenedor">
    
    <h1 > <img src="img/logo_cuc.png"   class="logo"></h1>
    <input type="checkbox" id="menu-bar">
    <label class="fontawesome-align-justify" for="menu-bar"></label>
        <nav class="menu">
         <a href="default.aspx">Inicio</a>
          <a href="IniciarSesion.aspx">Iniciar Sesión</a>
          <a href="Contact.aspx">Contactenos</a>
        </nav>
      </div>
</header>
      <!-- TERMINA EL MENU -->  
        <div class="espacio">
 </div>

        <div class="rowb">
    <div class="colb tre">
      <span><h2>Información</h2><br>
	 <span>Teléfono<br><br>
	2550-6100<br> 
         <br>
         <br>
	Correo de Jennifer Araya Pérez <br><br>
	jarayap@cuc.ac.cr<br></span>

</span>
    </div>
    </div>
						  
	
<div class="bloque" id="left">
	<div class="d-contact">
		&nbsp;</div>
	<p>
		<iframe frameborder="0" height="300" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7861.684565258417!2d-83.92606947671166!3d9.863596246245635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd32be30333c86a2c!2sColegio+Universitario+de+Cartago!5e0!3m2!1ses!2s!4v1407261952189" style="border:0" width="100%"></iframe></p>
</div>



	


				
<div class="espacio">
 </div>
				

 <div class="espacio">
 </div>

    

        <!-- COMIENZA EL FOOTER -->  
        
        <div class="clear"></div>
      <footer>
          <div id="content_footer" class="container">

                    <ul id="nav_footer"><a href="" class="logo_footer">
                        <img src="img/logo_cuc.png" /></a></ul>

                    <div class="clear"></div>

                    <div class="content_info_footer ">
                    
                        <span>Colegio Universitario de Cartago</span>
                    <p>Cartago,Barrio el Molino, de la Funeraria La Última Joya, 300 metros Sur.</p>
                    <a href="">Teléfono:2550-6100</a>


                        

                    </div>

                </div>
          </footer>
</body>
</html>

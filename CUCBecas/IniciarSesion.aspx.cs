﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class IniciarSesion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void acceder_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            string cedu = Cedula.Text; 
            string contra = password.Text; 
            float tipo;
            control.CorreoMasivo();
            tipo = control.inicio(cedu, contra);
            if (tipo == 0)
            {
                Response.Write("<script>alert('La Contraseña no es valida')</script>");
            }
            if(tipo == 1)
            {
                Session["IDusuario"] = cedu;
                string[] datos = control.SeccionE(cedu);
                Session["Nombre"] = datos[0];
                Session["Estado"] = datos[1];
                Session["Fecha"] = datos[2];
                Session["email"] = datos[3];
                Session["sesion"] = "1";
                Response.Redirect("Pagina_Estudiante");
               
            }
            if(tipo == 2)
            {
                Session["IDusuario"] = cedu;
                Session["Nombre"] = control.varseccion(cedu);
                Session["sesion"] = "2";
                Response.Redirect("PrincipalTrabajoSocial");
               
            }
            if(tipo == 3)
            {
               
                string[] val = control.FechasIncio();
                Session["Coste"] = val[0];
                Session["IDusuario"] = cedu;
                Session["Nombre"] = control.varseccion(cedu);
                Session["sesion"] = "3";
                Response.Redirect("PrincipalFinanciero");
              
            }
            if (tipo == 4) 
            {
                Session["IDusuario"] = cedu;
                Session["sesion"] = "4";
                Response.Redirect("PrincipalAdmin");
               
            }
            if (tipo == 5) 
            {
                Response.Write("<script>alert('Cedula o Dimex no esta registrado en el sistema')</script>");
            }

        }
    }
}
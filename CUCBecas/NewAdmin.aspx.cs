﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class NewAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string seccion = (string)Session["sesion"];
            string id = (string)Session["IDusuario"];
            if (!IsPostBack)
            {

                if (string.IsNullOrEmpty(id) || !seccion.Equals("4"))
                {
                    Response.Redirect("IniciarSesion");
                }
              
            }
        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            string contra = password.Text;
            if (contra.Equals(password2.Text))
            {
                if (contra.Length >= 5)
                {
                    Controlador control = new Controlador();
                    string val = control.Nadmin(ua.Text,Nombre.Text,Correo.Text,password.Text);
                    if (val.Equals("1")){
                        Response.Redirect("IniciarSesion");
                    }
                    else
                    {
                        Response.Redirect("NewAdmin");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Las Contraseña tiene que tener mas de 5 caracteres')</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Las Contraseña no coincide')</script>");
            }
        }
    }
}
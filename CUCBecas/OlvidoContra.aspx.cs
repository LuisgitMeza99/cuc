﻿using CUCBecas.Capa_Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CUCBecas
{
    public partial class OlvidoContra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Entrar_Click(object sender, EventArgs e)
        {
            Controlador control = new Controlador();
            string cedu = Cedula.Text;
            string email = Email.Text;
            string Valido = control.ValidarCambioContra(cedu, email);
            if (Valido.Equals("1"))
            {
                Session["IDusuario"] = cedu;
                Session["email"] = email;
                Response.Write("<script>alert('Se le envio el Codigo para el cambio de contraseña a su correo')</script>");
                Response.Redirect("Codigo");
            }
            if (Valido.Equals("2"))
            {
                Response.Write("<script>alert('El correo no concuerda con la cedula o dimex')</script>");
            }
            else
            {
                Response.Write("<script>alert('La cedula o dimex no esta registrada')</script>");
            }
        }

        
    }
}